package io.lkulisz.accounts;

import lombok.Data;

@Data
public class Config {
    private int httpPort = 8080;
    private String jdbcDriverClass = "org.hsqldb.jdbcDriver";
    private String jdbcUrl = "jdbc:hsqldb:mem:accounts_db;shutdown=true";
    private int jdbcMaxPoolSize = 4;
}
