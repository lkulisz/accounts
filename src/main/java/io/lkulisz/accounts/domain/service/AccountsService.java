package io.lkulisz.accounts.domain.service;

import com.google.inject.Inject;
import com.google.inject.Provider;
import io.lkulisz.accounts.domain.model.*;
import io.lkulisz.accounts.domain.params.*;
import io.lkulisz.accounts.domain.repository.AccountsRepository;
import rx.Observable;
import rx.Single;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Set;
import java.util.UUID;

public class AccountsService {
    private final Provider<AccountsRepository> repositoryProvider;
    private final Validator validator;

    @Inject
    public AccountsService(Provider<AccountsRepository> repositoryProvider, Validator validator) {
        this.repositoryProvider = repositoryProvider;
        this.validator = validator;
    }

    public Single<Account> openAccount(AccountOpenParams params) {
        AccountsRepository repository = repositoryProvider.get();
        return Single.just(params)
                .map(this::validate)
                .map(Account::new)
                .flatMap(repository::save);
    }

    public Single<Account> getAccount(UUID id) {
        AccountsRepository repository = repositoryProvider.get();
        return Single.just(id)
                .map(this::validate)
                .flatMap(repository::getAccountById);
    }

    public Single<Deposit> deposit(DepositParams params) {
        AccountsRepository repository = repositoryProvider.get();
        return Single.just(params)
                .map(this::validate)
                .flatMap(validParams -> repository.getAccountById(validParams.getDestinationAccountId())
                        .map(account -> new Deposit(params, account))
                )
                .map(transaction -> {
                    transaction.execute();
                    return transaction;
                })
                .flatMap(repository::save);
    }

    public Single<Withdrawal> withdraw(WithdrawalParams params) {
        AccountsRepository repository = repositoryProvider.get();
        return Single.just(params)
                .map(this::validate)
                .flatMap(validParams -> repository.getAccountById(validParams.getSourceAccountId())
                        .map(account -> new Withdrawal(params, account))
                )
                .map(transaction -> {
                    transaction.execute();
                    return transaction;
                })
                .flatMap(repository::save);
    }

    public Single<Transfer> transfer(TransferParams params) {
        AccountsRepository repository = repositoryProvider.get();
        return Single.just(params)
                .map(this::validate)
                .flatMap(validParams -> repository.getAccountById(validParams.getSourceAccountId())
                        .flatMap(sourceAccount -> repository.getAccountById(validParams.getDestinationAccountId())
                                .map(destinationAccount -> new Transfer(params, sourceAccount, destinationAccount))
                        )
                )
                .map(transaction -> {
                    transaction.execute();
                    return transaction;
                })
                .flatMap(repository::save);
    }

    public Single<Account> closeAccount(UUID id) {
        AccountsRepository repository = repositoryProvider.get();
        return Single.just(id)
                .map(this::validate)
                .flatMap(repository::getAccountById)
                .doOnSuccess(Account::close)
                .flatMap(repository::save);
    }

    public Single<Transaction> getTransaction(SingleTransactionQueryCriteria queryCriteria) {
        AccountsRepository repository = repositoryProvider.get();
        return Single.just(queryCriteria)
                .map(this::validate)
                .flatMap(repository::getTransaction);
    }

    public Observable<Transaction> getTransactions(TransactionsQueryCriteria queryCriteria) {
        AccountsRepository repository = repositoryProvider.get();
        return Single.just(queryCriteria)
                .map(this::validate)
                .flatMapObservable(repository::getTransactions);
    }

    private AccountOpenParams validate(AccountOpenParams params) {
        return validate(params, AccountOpenParams.class);
    }

    private UUID validate(UUID id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }

        return id;
    }

    private DepositParams validate(DepositParams params) {
        return validate(params, DepositParams.class);
    }

    private WithdrawalParams validate(WithdrawalParams params) {
        return validate(params, WithdrawalParams.class);
    }

    private TransferParams validate(TransferParams params) {
        return validate(params, TransferParams.class);
    }

    private SingleTransactionQueryCriteria validate(SingleTransactionQueryCriteria queryCriteria) {
        return validate(queryCriteria, SingleTransactionQueryCriteria.class);
    }

    private TransactionsQueryCriteria validate(TransactionsQueryCriteria queryCriteria) {
        return validate(queryCriteria, TransactionsQueryCriteria.class);
    }

    private <T> T validate(T item, Class<T> klass) {
        if (item == null) {
            throw new IllegalArgumentException("item must not be null");
        }

        Set<ConstraintViolation<T>> violations = validator.validate(item, klass);

        if (!violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }

        return item;
    }
}
