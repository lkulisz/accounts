package io.lkulisz.accounts.domain.exception;

import lombok.Getter;

import java.util.UUID;

public class DuplicateAccountException extends RuntimeException {
    @Getter
    private final UUID id;

    public DuplicateAccountException(UUID id) {
        super(String.format("Account already exists (ID: %s)", id));
        this.id = id;
    }
}
