package io.lkulisz.accounts.domain.exception;

import io.lkulisz.accounts.domain.params.SingleTransactionQueryCriteria;
import lombok.Getter;

import java.util.UUID;

public class TransactionNotFoundException extends RuntimeException {
    @Getter
    private final String id;

    public TransactionNotFoundException(String id) {
        super(String.format("Transaction with given ID not found (ID: %s)", id));
        this.id = id;
    }

    public TransactionNotFoundException(SingleTransactionQueryCriteria queryCriteria) {
        super("Transaction with given criteria was not found");
        this.id = queryCriteria.getTransactionId().toString();
    }
}
