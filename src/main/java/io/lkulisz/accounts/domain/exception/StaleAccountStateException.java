package io.lkulisz.accounts.domain.exception;

import lombok.Getter;

import java.util.UUID;

public class StaleAccountStateException extends RuntimeException {
    @Getter
    private final UUID id;

    public StaleAccountStateException(UUID id) {
        this(id, "Account has been concurrently modified while executing request");
    }

    public StaleAccountStateException(UUID id, String details) {
        super(String.format("%s (ID: %s)", details, id));
        this.id = id;
    }
}
