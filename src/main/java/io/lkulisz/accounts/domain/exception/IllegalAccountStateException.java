package io.lkulisz.accounts.domain.exception;

import lombok.Getter;

import java.util.UUID;

public class IllegalAccountStateException extends RuntimeException {
    @Getter
    private final UUID id;

    public IllegalAccountStateException(UUID id) {
        this(id, "Account state is illegal for this operation");
    }

    public IllegalAccountStateException(UUID id, String details) {
        super(String.format("%s (ID: %s)", details, id));
        this.id = id;
    }
}
