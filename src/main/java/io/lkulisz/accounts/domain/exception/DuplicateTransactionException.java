package io.lkulisz.accounts.domain.exception;

import lombok.Getter;

import java.util.UUID;

public class DuplicateTransactionException extends RuntimeException {
    @Getter
    private final UUID id;

    public DuplicateTransactionException(UUID id) {
        super(String.format("Transaction already exists (ID: %s)", id));
        this.id = id;
    }
}
