package io.lkulisz.accounts.domain.exception;

import lombok.Getter;

import java.util.UUID;

public class AccountNotFoundException extends RuntimeException {
    @Getter
    private final String id;

    public AccountNotFoundException(String id) {
        super(String.format("Account with given ID not found (ID: %s)", id));
        this.id = id;
    }

    public AccountNotFoundException(UUID id) {
        this(id.toString());
    }
}
