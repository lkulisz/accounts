package io.lkulisz.accounts.domain.validation;

import io.lkulisz.accounts.domain.params.TransferParams;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TransferParamsValidator implements ConstraintValidator<ValidTransferParams, TransferParams> {
    @Override
    public boolean isValid(TransferParams value, ConstraintValidatorContext context) {
        if (value.getSourceAccountId() == null || value.getDestinationAccountId() == null) {
            // Other validation rules will take care of null values
            return true;
        }

        return !value.getSourceAccountId().equals(value.getDestinationAccountId());
    }
}
