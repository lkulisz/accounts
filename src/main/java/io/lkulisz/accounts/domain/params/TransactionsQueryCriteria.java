package io.lkulisz.accounts.domain.params;

import io.lkulisz.accounts.domain.model.TransactionType;

import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

public interface TransactionsQueryCriteria {
    @NotNull
    UUID getAccountId();
    Optional<TransactionType> getTransactionType();
}
