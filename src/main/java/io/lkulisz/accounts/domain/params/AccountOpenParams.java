package io.lkulisz.accounts.domain.params;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

public interface AccountOpenParams {
    @NotNull
    UUID getId();
    @NotNull
    UUID getOwnerId();
    @NotNull
    @Size(min = 1, max = 1000)
    String getName();
}
