package io.lkulisz.accounts.domain.params;

import io.lkulisz.accounts.domain.validation.ValidTransferParams;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.UUID;

@ValidTransferParams
public interface TransferParams {
    @NotNull
    UUID getId();
    @NotNull
    UUID getSourceAccountId();
    @NotNull
    UUID getDestinationAccountId();
    @NotNull
    @DecimalMin(value = "0", inclusive = false)
    BigDecimal getAmount();
    @NotNull
    @Size(min = 1, max = 1000)
    String getTitle();
}
