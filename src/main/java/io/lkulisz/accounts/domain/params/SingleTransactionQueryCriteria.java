package io.lkulisz.accounts.domain.params;

import io.lkulisz.accounts.domain.model.TransactionType;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public interface SingleTransactionQueryCriteria {
    @NotNull
    UUID getAccountId();
    @NotNull
    UUID getTransactionId();
    @NotNull
    TransactionType getTransactionType();
}
