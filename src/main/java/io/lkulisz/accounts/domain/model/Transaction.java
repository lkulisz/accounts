package io.lkulisz.accounts.domain.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

public interface Transaction {
    UUID getId();
    String getTitle();
    BigDecimal getAmount();
    Instant getInstant();
    TransactionType getType();

    Optional<Account> getSourceAccount();
    Optional<Account> getDestinationAccount();
}
