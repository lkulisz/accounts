package io.lkulisz.accounts.domain.model;

import io.lkulisz.accounts.domain.params.DepositParams;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Getter
@AllArgsConstructor
@Builder
public class Deposit implements Transaction {
    @NonNull
    private UUID id;
    @NonNull
    private BigDecimal amount;
    @NonNull
    private String title;
    @NonNull
    private Instant instant;
    @NonNull
    private Account destinationAccount;

    public Deposit(DepositParams params, Account destinationAccount) {
        this.id = params.getId();
        this.title = params.getTitle();
        this.amount = params.getAmount();
        this.instant = Instant.now();
        this.destinationAccount = destinationAccount;
    }

    @Override
    public TransactionType getType() {
        return TransactionType.DEPOSIT;
    }

    @Override
    public Optional<Account> getSourceAccount() {
        return Optional.empty();
    }

    @Override
    public Optional<Account> getDestinationAccount() {
        return Optional.of(destinationAccount);
    }

    public UUID getDestinationAccountId() {
        return destinationAccount.getId();
    }

    public void execute() {
        destinationAccount.increaseBalance(amount);
    }
}
