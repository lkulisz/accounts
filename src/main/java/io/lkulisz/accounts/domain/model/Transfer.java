package io.lkulisz.accounts.domain.model;

import io.lkulisz.accounts.domain.params.TransferParams;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Getter
@Builder
@AllArgsConstructor
public class Transfer implements Transaction {
    @NonNull
    private UUID id;
    @NonNull
    private BigDecimal amount;
    @NonNull
    private String title;
    @NonNull
    private Instant instant;
    @NonNull
    private Account sourceAccount;
    @NonNull
    private Account destinationAccount;

    public Transfer(TransferParams params, Account sourceAccount, Account destinationAccount) {
        this.id = params.getId();
        this.title = params.getTitle();
        this.amount = params.getAmount();
        this.instant = Instant.now();
        this.sourceAccount = sourceAccount;
        this.destinationAccount = destinationAccount;
    }

    @Override
    public TransactionType getType() {
        return TransactionType.TRANSFER;
    }

    @Override
    public Optional<Account> getSourceAccount() {
        return Optional.of(sourceAccount);
    }

    public UUID getSourceAccountId() {
        return sourceAccount.getId();
    }

    @Override
    public Optional<Account> getDestinationAccount() {
        return Optional.of(destinationAccount);
    }

    public UUID getDestinationAccountId() {
        return destinationAccount.getId();
    }

    public void execute() {
        sourceAccount.decreaseBalance(amount);
        destinationAccount.increaseBalance(amount);
    }
}
