package io.lkulisz.accounts.domain.model;

import io.lkulisz.accounts.domain.params.WithdrawalParams;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Getter
@AllArgsConstructor
@Builder
public class Withdrawal implements Transaction {
    @NonNull
    private UUID id;
    @NonNull
    private BigDecimal amount;
    @NonNull
    private String title;
    @NonNull
    private Instant instant;
    @NonNull
    private Account sourceAccount;

    public Withdrawal(WithdrawalParams params, Account sourceAccount) {
        this.id = params.getId();
        this.title = params.getTitle();
        this.amount = params.getAmount();
        this.instant = Instant.now();
        this.sourceAccount = sourceAccount;
    }

    @Override
    public TransactionType getType() {
        return TransactionType.WITHDRAWAL;
    }

    @Override
    public Optional<Account> getSourceAccount() {
        return Optional.of(sourceAccount);
    }

    @Override
    public Optional<Account> getDestinationAccount() {
        return Optional.empty();
    }

    public UUID getSourceAccountId() {
        return sourceAccount.getId();
    }

    public void execute() {
        sourceAccount.decreaseBalance(amount);
    }
}
