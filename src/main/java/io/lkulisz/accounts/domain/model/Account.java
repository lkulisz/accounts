package io.lkulisz.accounts.domain.model;

import io.lkulisz.accounts.domain.exception.IllegalAccountStateException;
import io.lkulisz.accounts.domain.params.AccountOpenParams;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;
import java.util.UUID;

@Getter
@AllArgsConstructor
@Builder
public class Account {
    @NonNull
    private UUID id;
    @NonNull
    private String name;
    @NonNull
    private UUID ownerId;
    @NonNull
    private BigDecimal balance;
    private boolean closed;

    public Account(AccountOpenParams accountOpenParams) {
        this.id = accountOpenParams.getId();
        this.name = accountOpenParams.getName();
        this.ownerId = accountOpenParams.getOwnerId();
        this.balance = BigDecimal.ZERO;
        this.closed = false;
    }

    public boolean canCloseAccount() {
        return !closed && balance.signum() == 0;
    }

    void increaseBalance(BigDecimal amount) {
        if (closed) {
            throw new IllegalAccountStateException(id, "Account is closed");
        }
        balance = balance.add(amount);
    }

    void decreaseBalance(BigDecimal amount) {
        if (closed) {
            throw new IllegalAccountStateException(id, "Account is closed");
        }
        if (balance.compareTo(amount) < 0) {
            throw new IllegalAccountStateException(id , "Account has too low balance");
        }

        balance = balance.subtract(amount);
    }

    public void close() {
        if (!canCloseAccount()) {
            throw new IllegalAccountStateException(id, "Account cannot be closed");
        }
        closed = true;
    }
}
