package io.lkulisz.accounts.domain.repository;

import io.lkulisz.accounts.domain.model.Account;
import io.lkulisz.accounts.domain.model.Transaction;
import io.lkulisz.accounts.domain.params.SingleTransactionQueryCriteria;
import io.lkulisz.accounts.domain.params.TransactionsQueryCriteria;
import rx.Observable;
import rx.Single;

import java.util.UUID;

public interface AccountsRepository {
    Single<Account> save(Account account);
    <T extends Transaction> Single<T> save(T transaction);
    Single<Account> getAccountById(UUID uuid);
    Single<Transaction> getTransaction(SingleTransactionQueryCriteria queryCriteria);
    Observable<Transaction> getTransactions(TransactionsQueryCriteria queryCriteria);
}
