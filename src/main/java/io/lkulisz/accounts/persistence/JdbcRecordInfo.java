package io.lkulisz.accounts.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
class JdbcRecordInfo {
    private long id;
    private int version;
}
