package io.lkulisz.accounts.persistence;

import io.lkulisz.accounts.domain.exception.*;
import io.lkulisz.accounts.domain.model.Account;
import io.lkulisz.accounts.domain.model.Transaction;
import io.lkulisz.accounts.domain.params.SingleTransactionQueryCriteria;
import io.lkulisz.accounts.domain.params.TransactionsQueryCriteria;
import io.lkulisz.accounts.domain.repository.AccountsRepository;
import io.vertx.ext.sql.UpdateResult;
import io.vertx.rxjava.ext.jdbc.JDBCClient;
import io.vertx.rxjava.ext.sql.SQLConnection;
import lombok.extern.slf4j.Slf4j;
import rx.Completable;
import rx.Observable;
import rx.Single;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;

import static java.util.stream.Collectors.toList;

@Slf4j
public class JdbcAccountsRepository implements AccountsRepository {
    private static final String SQL_STATE_DUPLICATE_ID = "23505";
    private static final Integer DEFAULT_VERSION = 1;

    private final JDBCClient dbClient;
    private final JdbcRepositoryContext context;
    private final JdbcMapper mapper;

    JdbcAccountsRepository(JDBCClient dbClient, JdbcRepositoryContext context, JdbcMapper mapper) {
        this.dbClient = dbClient;
        this.context = context;
        this.mapper = mapper;
    }

    @Override
    public Single<Account> save(Account account) {
        return context.has(account) ? update(account) : insert(account);
    }

    private Single<Account> insert(Account account) {
        return withConnection(connection -> Single.just(mapper.insert(account))
                .flatMap(command -> connection
                    .rxUpdateWithParams(command.getSql(), command.getParams())
                    .onErrorResumeNext(error -> handleSaveError(account, error))
                    .map(updateResult -> {
                        Long id = updateResult.getKeys().getLong(0);
                        context.putId(account, id);
                        context.putVersion(account, DEFAULT_VERSION);
                        return account;
                    })
                ));
    }

    private Single<Account> update(Account account) {
        return withConnection(connection -> doUpdateAccount(connection, account));
    }

    private Single<Account> doUpdateAccount(SQLConnection connection, Account account) {
        return Single.just(context.getInfo(account))
                .map(optionalAccount -> optionalAccount.orElseThrow(() -> new AccountNotFoundException(account.getId())))
                .map(recordInfo -> mapper.update(account, recordInfo))
                .flatMap(command -> connection
                    .rxUpdateWithParams(command.getSql(), command.getParams())
                    .map(updateResult -> {
                        if (updateResult.getUpdated() <= 0) {
                            throw new StaleAccountStateException(account.getId());
                        }
                        context.bumpVersion(account);
                        return updateResult;
                    })
                    .onErrorResumeNext(error -> handleSaveError(account, error))
                    .map(updateResult -> account)
                );
    }

    private Single<? extends UpdateResult> handleSaveError(Account account, Throwable error) {
        if (uuidNotUnique(error)) {
            return Single.error(new DuplicateAccountException(account.getId()));
        } else if (error instanceof StaleAccountStateException) {
            return Single.error(error);
        }

        log.error("Could not insert Account into DB", error);
        return Single.error(error);
    }

    private boolean uuidNotUnique(Throwable error) {
        if (!(error instanceof SQLIntegrityConstraintViolationException)) {
            return false;
        }
        SQLIntegrityConstraintViolationException sqlException = (SQLIntegrityConstraintViolationException) error;
        return SQL_STATE_DUPLICATE_ID.equals(sqlException.getSQLState());
    }

    @Override
    public Single<Account> getAccountById(UUID uuid) {
        return withConnection(connection -> Single.just(mapper.selectAccount(uuid))
                .flatMap(query -> connection
                    .rxQueryWithParams(query.getSql(), query.getParams())
                    .doOnError(error -> log.error("Could not get Account by UUID", error))
                    .map(resultSet -> {
                        if (resultSet.getNumRows() <= 0) {
                            throw new AccountNotFoundException(uuid);
                        }
                        return query.mqp(resultSet.getRows().get(0));
                    })
                ));
    }

    @Override
    public <T extends Transaction> Single<T> save(T transaction) {
        return withConnection(connection -> Single.just(transaction)
                .map(this::validate)
                .flatMap(validTransaction -> connection
                    .rxSetAutoCommit(false)
                    .toCompletable()
                    .andThen(transaction.getSourceAccount()
                            .map(account -> doUpdateAccount(connection, account)).map(Single::toCompletable)
                            .orElse(Completable.complete())
                    )
                    .andThen(transaction.getDestinationAccount()
                            .map(account -> doUpdateAccount(connection, account)).map(Single::toCompletable)
                            .orElse(Completable.complete())
                    )
                    .andThen(Single.just(mapper.insert(transaction))
                            .flatMap(command -> connection
                                    .rxUpdateWithParams(command.getSql(), command.getParams())
                                    .onErrorResumeNext(error -> handleSaveError(transaction, error))
                            ).toCompletable()
                    )
                    .andThen(connection.rxCommit())
                    .onErrorResumeNext(error -> connection.rxRollback().flatMap(x -> Single.error(error)))
                    .map(x -> transaction))
                );
    }

    private Transaction validate(Transaction transaction) {
        transaction.getSourceAccount().ifPresent(account -> {
            if (!context.has(account)) {
                throw new AccountNotFoundException(account.getId());
            }
        });
        transaction.getDestinationAccount().ifPresent(account -> {
            if (!context.has(account)) {
                throw new AccountNotFoundException(account.getId());
            }
        });

        return transaction;
    }

    private Single<? extends UpdateResult> handleSaveError(Transaction transaction, Throwable error) {
        if (uuidNotUnique(error)) {
            return Single.error(new DuplicateTransactionException(transaction.getId()));
        }

        log.error("Could not insert Transaction into DB", error);
        return Single.error(error);
    }

    @Override
    public Single<Transaction> getTransaction(SingleTransactionQueryCriteria queryCriteria) {
        return withConnection(connection -> Single.just(mapper.selectTransaction(queryCriteria))
                .flatMap(query -> connection
                        .rxQueryWithParams(query.getSql(), query.getParams())
                        .doOnError(error -> log.error("Could not get Transaction", error))
                        .map(resultSet -> {
                            if (resultSet.getNumRows() <= 0) {
                                throw new TransactionNotFoundException(queryCriteria);
                            }
                            return query.mqp(resultSet.getRows().get(0));
                        })
                ));
    }

    @Override
    public Observable<Transaction> getTransactions(TransactionsQueryCriteria queryCriteria) {
        return withConnectionReturnObservable(connection -> Single.just(mapper.selectTransactions(queryCriteria))
                .flatMapObservable(query -> connection
                        .rxQueryWithParams(query.getSql(), query.getParams())
                        .doOnError(error -> log.error("Could not get Transaction", error))
                        .flatMapObservable(resultSet -> {
                            if (resultSet.getNumRows() <= 0) {
                                return Observable.empty();
                            }

                            List<Transaction> transactions = resultSet.getRows().stream()
                                    .map(query::mqp)
                                    .collect(toList());

                            return Observable.from(transactions);
                        })
                ));
    }

    private <T> Single<T> withConnection(Function<SQLConnection, ? extends Single<T>> dbFunction) {
        return dbClient.rxGetConnection()
                .doOnError(error -> log.error("HttpError when getting DB connection", error))
                .flatMap(connection -> dbFunction.apply(connection)
                        .doAfterTerminate(connection::close));
    }

    private <T> Observable<T> withConnectionReturnObservable(
            Function<SQLConnection, ? extends Observable<T>> dbFunction) {
        return dbClient.rxGetConnection()
                .doOnError(error -> log.error("HttpError when getting DB connection", error))
                .flatMapObservable(connection -> dbFunction.apply(connection)
                        .doAfterTerminate(connection::close));
    }
}
