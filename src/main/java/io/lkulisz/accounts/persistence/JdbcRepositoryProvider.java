package io.lkulisz.accounts.persistence;

import com.google.inject.Inject;
import com.google.inject.Provider;
import io.lkulisz.accounts.domain.repository.AccountsRepository;
import io.vertx.rxjava.ext.jdbc.JDBCClient;

public class JdbcRepositoryProvider implements Provider<AccountsRepository> {
    private final JDBCClient jdbcClient;

    @Inject
    public JdbcRepositoryProvider(JDBCClient jdbcClient) {
        this.jdbcClient = jdbcClient;
    }

    @Override
    public AccountsRepository get() {
        JdbcRepositoryContext context = new JdbcRepositoryContext();
        return new JdbcAccountsRepository(jdbcClient, context, new JdbcMapper(context));
    }
}
