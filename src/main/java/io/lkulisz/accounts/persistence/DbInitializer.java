package io.lkulisz.accounts.persistence;

import com.google.inject.Inject;
import io.vertx.rxjava.ext.jdbc.JDBCClient;
import io.vertx.rxjava.ext.sql.SQLConnection;
import lombok.extern.slf4j.Slf4j;
import rx.Single;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

@Slf4j
public class DbInitializer {
    private final JDBCClient dbClient;

    @Inject
    public DbInitializer(JDBCClient dbClient) {
        this.dbClient = dbClient;
    }

    public Single<Void> createTables() {
        return dbClient.rxGetConnection()
                .flatMap(this::createAccountsTable)
                .doOnError(error -> log.error("Error when getting DB connection", error));
    }

    private Single<Void> createAccountsTable(SQLConnection connection) {
        log.info("Loading DDL from resources");
        InputStream in = getClass().getResourceAsStream("/db.sql");
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String sql = reader.lines().collect(Collectors.joining());

        log.info("Creating DB tables");
        return connection
                .rxExecute(sql)
                .doOnError(error -> log.error("Error when creating DB tables", error))
                .doAfterTerminate(connection::close);
    }
}
