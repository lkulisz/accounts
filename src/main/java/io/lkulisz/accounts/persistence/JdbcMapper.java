package io.lkulisz.accounts.persistence;

import io.lkulisz.accounts.domain.model.*;
import io.lkulisz.accounts.domain.params.SingleTransactionQueryCriteria;
import io.lkulisz.accounts.domain.params.TransactionsQueryCriteria;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
class JdbcMapper {
    private static final String GENERIC = "GENERIC";

    private final JdbcRepositoryContext context;

    JdbcMapper(JdbcRepositoryContext context) {
        this.context = context;
    }

    SqlQuery<Account> selectAccount(UUID uuid) {
        // It's necessary to convert balance to varchar, because otherwise it will be represented as a Double in ResultSet
        // Using Double will cause lost precision.
        String sql =
                "SELECT id, uuid, owner_id, name, CONVERT(balance, SQL_VARCHAR) AS balance, is_closed, version " +
                "FROM accounts " +
                "WHERE uuid = ?";
        JsonArray params = new JsonArray().add(uuid.toString());

        return SqlQuery.create(sql, params, this::createAccount);
    }

    private Account createAccount(JsonObject row) {
        return createAccount(row, "");
    }

    private Account createAccount(JsonObject row, String prefix) {
        Account account = Account.builder()
                .id(UUID.fromString(row.getString(prefix + "UUID")))
                .name(row.getString(prefix + "NAME"))
                .ownerId(UUID.fromString(row.getString(prefix + "OWNER_ID")))
                .balance(new BigDecimal(row.getString(prefix + "BALANCE")))
                .closed(row.getBoolean(prefix + "IS_CLOSED"))
                .build();

        context.putId(account, row.getLong(prefix + "ID"));
        context.putVersion(account, row.getInteger(prefix + "VERSION"));

        return account;
    }

    SqlQuery<? extends Transaction> selectTransaction(SingleTransactionQueryCriteria criteria) {
        String whereClause =
                "t.uuid = ? AND " +
                "t.type = ? AND (" +
                        "t.source_account_uuid = ? OR " +
                        "t.destination_account_uuid = ?)";

        String transactionType = criteria.getTransactionType() != null
                ? criteria.getTransactionType().toString()
                : GENERIC;
        JsonArray params = new JsonArray()
                .add(criteria.getTransactionId().toString())
                .add(transactionType)
                .add(criteria.getAccountId().toString())
                .add(criteria.getAccountId().toString());

        return selectTransactions(whereClause, params);
    }

    SqlQuery<? extends Transaction> selectTransactions(TransactionsQueryCriteria criteria) {
        StringBuilder whereClauseBuilder = new StringBuilder();

        whereClauseBuilder.append("(t.source_account_uuid = ? OR t.destination_account_uuid = ?)");
        JsonArray params = new JsonArray()
                .add(criteria.getAccountId().toString())
                .add(criteria.getAccountId().toString());

        criteria.getTransactionType().ifPresent(transactionType -> {
            whereClauseBuilder.append("AND t.type = ?");
            params.add(transactionType);
        });

        return selectTransactions(whereClauseBuilder.toString(), params);
    }

    private SqlQuery<? extends Transaction> selectTransactions(String whereClause, JsonArray params) {
        String sql =
                "SELECT " +
                        "t.uuid, " +
                        "t.type, " +
                        "t.title, " +
                        "CONVERT(t.amount, SQL_VARCHAR) AS amount, " +
                        "t.instant, " +
                        "sa.id AS sa_id, " +
                        "sa.uuid AS sa_uuid, " +
                        "sa.owner_id AS sa_owner_id, " +
                        "sa.name AS sa_name, " +
                        "CONVERT(sa.balance, SQL_VARCHAR) AS sa_balance, " +
                        "sa.is_closed AS sa_is_closed, " +
                        "sa.version AS sa_version, " +
                        "da.id AS da_id, " +
                        "da.uuid AS da_uuid, " +
                        "da.owner_id AS da_owner_id, " +
                        "da.name AS da_name, " +
                        "CONVERT(da.balance, SQL_VARCHAR) AS da_balance, " +
                        "da.is_closed AS da_is_closed, " +
                        "da.version AS da_version " +
                "FROM " +
                        "transactions t " +
                        "LEFT OUTER JOIN accounts sa ON t.source_account_id = sa.id " +
                        "LEFT OUTER JOIN accounts da ON t.destination_account_id = da.id " +
                "WHERE " + whereClause +
                "ORDER BY t.id";
        return SqlQuery.create(sql, params, this::createTransaction);
    }

    private Transaction createTransaction(JsonObject row) {
        String type = row.getString("TYPE");
        if (GENERIC.equals(type)) {
            log.warn("GENERIC transaction should be used only for testing purposes");
            return createGenericTransaction(row);
        }

        try {
            switch (TransactionType.valueOf(type)) {
                case DEPOSIT:
                    return createDeposit(row);
                case WITHDRAWAL:
                    return createWithdrawal(row);
                case TRANSFER:
                    return createTransfer(row);
                default:
                    throw new IllegalArgumentException(String.format("Unknown type of transaction: %s", type));
            }
        } catch (IllegalArgumentException ex) {
            throw new IllegalArgumentException(String.format("Unknown type of transaction: %s", type));
        }
    }

    private Deposit createDeposit(JsonObject row) {
        return Deposit.builder()
                .id(UUID.fromString(row.getString("UUID")))
                .title(row.getString("TITLE"))
                .amount(new BigDecimal(row.getString("AMOUNT")))
                .instant(row.getInstant("INSTANT"))
                .destinationAccount(createAccount(row, "DA_"))
                .build();
    }

    private Withdrawal createWithdrawal(JsonObject row) {
        return Withdrawal.builder()
                .id(UUID.fromString(row.getString("UUID")))
                .title(row.getString("TITLE"))
                .amount(new BigDecimal(row.getString("AMOUNT")))
                .instant(row.getInstant("INSTANT"))
                .sourceAccount(createAccount(row, "SA_"))
                .build();
    }

    private Transfer createTransfer(JsonObject row) {
        return Transfer.builder()
                .id(UUID.fromString(row.getString("UUID")))
                .title(row.getString("TITLE"))
                .amount(new BigDecimal(row.getString("AMOUNT")))
                .instant(row.getInstant("INSTANT"))
                .sourceAccount(createAccount(row, "SA_"))
                .destinationAccount(createAccount(row, "DA_"))
                .build();
    }

    private GenericTransaction createGenericTransaction(JsonObject row) {
        boolean hasSourceAccount = row.getLong("SA_ID") != null;
        boolean hasDestinationAccount = row.getLong("DA_ID") != null;

        return GenericTransaction.builder()
                .id(UUID.fromString(row.getString("UUID")))
                .title(row.getString("TITLE"))
                .amount(new BigDecimal(row.getString("AMOUNT")))
                .instant(row.getInstant("INSTANT"))
                .sourceAccount(hasSourceAccount ? Optional.of(createAccount(row, "SA_")) : Optional.empty())
                .destinationAccount(hasDestinationAccount ? Optional.of(createAccount(row, "DA_")) : Optional.empty())
                .build();
    }

    SqlCommand insert(Account account) {
        String sql =  "INSERT INTO accounts(uuid, owner_id, name, balance, is_closed) VALUES(?, ?, ?, ?, ?)";
        JsonArray params = insertParams(account);
        return new SqlCommand(sql, params);
    }

    private JsonArray insertParams(Account account) {
        return new JsonArray()
                    .add(account.getId().toString())
                    .add(account.getOwnerId().toString())
                    .add(account.getName())
                    .add(account.getBalance().toString())
                    .add(account.isClosed());
    }

    SqlCommand update(Account account, JdbcRecordInfo info) {
        String sql =
                "UPDATE accounts " +
                "SET uuid = ?, owner_id = ?, name = ?, balance = ?, is_closed = ?, version = ? " +
                "WHERE id = ? and version = ?";

        Integer newVersion = info.getVersion() + 1;
        JsonArray params =  insertParams(account)
                .add(newVersion)
                .add(info.getId())
                .add(info.getVersion());

        return new SqlCommand(sql, params);
    }

    SqlCommand insert(Transaction transaction) {
        String sql =
                "INSERT INTO transactions(" +
                    "type, uuid, title, amount, instant, " +
                    "source_account_id, source_account_uuid, destination_account_id, destination_account_uuid) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";


        // Using Stream, and List here, because JsonArray.add does not accept null.
        // It would be necessary to call addNull, which is super cumbersome.
        JsonArray params = new JsonArray(Stream.of(
                getType(transaction),
                transaction.getId().toString(),
                transaction.getTitle(),
                transaction.getAmount().toString(),
                transaction.getInstant().toString(),
                transaction.getSourceAccount().map(context::getId).orElse(null),
                transaction.getSourceAccount().map(Account::getId).orElse(null),
                transaction.getDestinationAccount().map(context::getId).orElse(null),
                transaction.getDestinationAccount().map(Account::getId).orElse(null)
        ).collect(Collectors.toList()));

        return new SqlCommand(sql, params);
    }

    private String getType(Transaction transaction) {
        TransactionType type = transaction.getType();
        if (type == null) {
            log.warn("GENERIC transaction should be used only for testing purposes");
            return GENERIC;
        }

        return type.toString();
    }
}
