package io.lkulisz.accounts.persistence;

import io.vertx.core.json.JsonArray;
import lombok.Value;

@Value
class SqlCommand {
    private final String sql;
    private final JsonArray params;
}
