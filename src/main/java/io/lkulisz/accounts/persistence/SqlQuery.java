package io.lkulisz.accounts.persistence;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;

import java.util.function.Function;

@AllArgsConstructor
class SqlQuery<T> {
    @Getter
    private final String sql;
    @Getter
    private final JsonArray params;
    private final Function<JsonObject, T> mapper;

    static <T> SqlQuery<T> create(String sql, JsonArray params, Function<JsonObject, T> mapper) {
        return new SqlQuery<>(sql, params, mapper);
    }

    T mqp(JsonObject row) {
        return mapper.apply(row);
    }
}
