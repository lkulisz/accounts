package io.lkulisz.accounts.persistence;

import io.lkulisz.accounts.domain.model.Account;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Optional;

class JdbcRepositoryContext {
    private final Map<Account, JdbcRecordInfo> recordInfoByAccount = new IdentityHashMap<>();

    boolean has(Account account) {
        return recordInfoByAccount.containsKey(account);
    }

    Optional<JdbcRecordInfo> getInfo(Account account) {
        return Optional.ofNullable(recordInfoByAccount.getOrDefault(account, null));
    }

    Long getId(Account account) {
        return recordInfoByAccount.get(account).getId();
    }

    private Integer getVersion(Account account) {
        return recordInfoByAccount.get(account).getVersion();
    }

    void putId(Account account, Long id) {
        getOrCreateFor(account).setId(id);
    }

    void putVersion(Account account, Integer version) {
        getOrCreateFor(account).setVersion(version);
    }

    void bumpVersion(Account account) {
        putVersion(account, getVersion(account) + 1);
    }

    private JdbcRecordInfo getOrCreateFor(Account account) {
        return recordInfoByAccount.computeIfAbsent(account, x -> new JdbcRecordInfo());
    }
}
