package io.lkulisz.accounts.persistence;

import io.lkulisz.accounts.domain.model.Account;
import io.lkulisz.accounts.domain.model.Transaction;
import io.lkulisz.accounts.domain.model.TransactionType;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Data
@Builder
public class GenericTransaction implements Transaction {
    private UUID id;
    private String title;
    private BigDecimal amount;
    private Instant instant;
    private Optional<Account> sourceAccount;
    private Optional<Account> destinationAccount;

    @Override
    public TransactionType getType() {
        return null;
    }
}
