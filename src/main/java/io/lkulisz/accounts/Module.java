package io.lkulisz.accounts;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import io.lkulisz.accounts.domain.repository.AccountsRepository;
import io.lkulisz.accounts.http.error.ErrorHandler;
import io.lkulisz.accounts.http.v1.accounts.error.AccountsErrorHandlerProvider;
import io.lkulisz.accounts.persistence.JdbcRepositoryProvider;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.eventbus.EventBus;
import io.vertx.rxjava.ext.jdbc.JDBCClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public class Module extends AbstractModule {
    private static final Path CONFIG_PATH = Paths.get("config.json");

    private final Vertx vertx;

    public Module(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    protected void configure() {
        bind(Vertx.class).toInstance(vertx);
        bind(EventBus.class).toInstance(vertx.eventBus());
        bind(AccountsRepository.class).toProvider(JdbcRepositoryProvider.class);
        bind(ErrorHandler.class).toProvider(AccountsErrorHandlerProvider.class);
    }

    @Provides
    @Singleton
    @SneakyThrows
    private Config config() {
        if (!Files.exists(CONFIG_PATH)) {
            log.info("Using default config");
            return new Config();
        }
        log.info("Applying configuration from {}", CONFIG_PATH.toString());
        String configJson = new String(Files.readAllBytes(CONFIG_PATH), Charset.forName("UTF-8"));
        return new JsonObject(configJson).mapTo(Config.class);
    }

    @Provides
    private JDBCClient jdbcClientProvider(Vertx vertx, Config config) {
        return JDBCClient.createShared(vertx, new JsonObject()
                .put("url", config.getJdbcUrl())
                .put("driver_class", config.getJdbcDriverClass())
                .put("max_pool_size", config.getJdbcMaxPoolSize()));
    }

    @Singleton
    @Provides
    private Validator hibernateValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        return factory.getValidator();
    }
}
