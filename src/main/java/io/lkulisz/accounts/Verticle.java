package io.lkulisz.accounts;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.google.inject.Guice;
import com.google.inject.Inject;
import io.lkulisz.accounts.http.ServerInitializer;
import io.lkulisz.accounts.persistence.DbInitializer;
import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.Vertx;
import lombok.extern.slf4j.Slf4j;
import rx.Single;

import java.util.function.Function;

@Slf4j
public class Verticle extends AbstractVerticle {
    private final Function<Vertx, com.google.inject.Module> getModule;

    @Inject
    private DbInitializer dbInitializer;
    @Inject
    private ServerInitializer serverInitializer;

    public Verticle(Function<Vertx, com.google.inject.Module> getModule) {
        this.getModule = getModule;
    }

    public Verticle() {
        this(Module::new);
    }

    @Override
    public void start(Future<Void> startFuture) {
        Json.mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);

        Guice.createInjector(getModule.apply(vertx)).injectMembers(this);

        Single.concat(dbInitializer.createTables(), serverInitializer.startServer())
                .subscribe(
                        unused -> {},
                        startFuture::fail,
                        startFuture::complete);
    }
}
