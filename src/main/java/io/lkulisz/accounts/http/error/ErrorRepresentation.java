package io.lkulisz.accounts.http.error;

import io.lkulisz.accounts.http.util.Envelope;

public class ErrorRepresentation extends Envelope<Void, Envelope.Meta<Envelope.Links>> {
    public ErrorRepresentation(HttpError httpError, String resourceType, String path) {
        setError(httpError);
        setMeta(new Envelope.Meta<>(resourceType, new Links(path)));
    }
}
