package io.lkulisz.accounts.http.error;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import java.util.Optional;

public class JacksonInvalidFormatMapper implements ErrorMapper {
    private static final int STATUS_CODE = 400;
    private static final String CODE = "invalid_payload";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!canHandle(error)) {
            return Optional.empty();
        }

        InvalidFormatException formatException;
        if (error instanceof InvalidFormatException) {
            formatException = (InvalidFormatException) error;
        } else {
            formatException = (InvalidFormatException) error.getCause();
        }

        return Optional.of(new HttpError(STATUS_CODE, CODE, formatException.getMessage()));
    }

    private boolean canHandle(Throwable error) {
        return error instanceof InvalidFormatException ||
                (error.getCause() != null && error.getCause() instanceof InvalidFormatException);
    }
}
