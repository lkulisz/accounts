package io.lkulisz.accounts.http.error;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;

import java.util.Optional;
import java.util.stream.Collectors;

public class JacksonUnrecognizedPropertyMapper implements ErrorMapper {
    private static final int STATUS_CODE = 400;
    private static final String CODE = "invalid_payload";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!canHandle(error)) {
            return Optional.empty();
        }

        UnrecognizedPropertyException unrecognizedPropertyException;
        if (error instanceof UnrecognizedPropertyException) {
            unrecognizedPropertyException = (UnrecognizedPropertyException) error;
        } else {
            unrecognizedPropertyException = (UnrecognizedPropertyException) error.getCause();
        }

        String path = unrecognizedPropertyException.getPath().stream()
                .map(JsonMappingException.Reference::getFieldName)
                .collect(Collectors.joining("."));
        String details = String.format("Unrecognized field `%s`", path);

        return Optional.of(new HttpError(STATUS_CODE, CODE, details));
    }

    private boolean canHandle(Throwable error) {
        return error instanceof UnrecognizedPropertyException ||
                (error.getCause() != null && error.getCause() instanceof UnrecognizedPropertyException);
    }
}
