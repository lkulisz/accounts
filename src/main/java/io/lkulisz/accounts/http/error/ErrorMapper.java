package io.lkulisz.accounts.http.error;

import java.util.Optional;

public interface ErrorMapper {
    Optional<HttpError> map(Throwable error);
}
