package io.lkulisz.accounts.http.error;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CompositeErrorMapper implements ErrorMapper {
    private final List<ErrorMapper> handlers;

    private CompositeErrorMapper(List<ErrorMapper> handlers) {
        this.handlers = handlers;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public Optional<HttpError> map(Throwable error) {
        for (ErrorMapper handler : handlers) {
            Optional<HttpError> httpError = handler.map(error);
            if (httpError.isPresent()) {
                return httpError;
            }
        }
        return Optional.empty();
    }

    public static class Builder {
        private static List<ErrorMapper> handlers = new ArrayList<>();

        public Builder add(ErrorMapper handler) {
            handlers.add(handler);
            return this;
        }

        public CompositeErrorMapper build() {
            return new CompositeErrorMapper(handlers);
        }
    }
}
