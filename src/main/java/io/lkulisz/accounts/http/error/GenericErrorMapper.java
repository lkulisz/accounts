package io.lkulisz.accounts.http.error;

import io.vertx.rxjava.ext.web.RoutingContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GenericErrorMapper{
    private static final int STATUS_CODE = 500;
    private static final String CODE = "unknown";

    public static HttpError map(RoutingContext rc, Throwable error) {
        String logMessage = String.format(
                "Unhandled exception when processing %s %s",
                rc.request().rawMethod(),
                rc.normalisedPath());
        log.error(logMessage, error);
        return new HttpError(STATUS_CODE, CODE, null);
    }
}
