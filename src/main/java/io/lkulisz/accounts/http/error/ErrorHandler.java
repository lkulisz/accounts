package io.lkulisz.accounts.http.error;

import io.vertx.rxjava.ext.web.RoutingContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ErrorHandler {
    private final ErrorMapper errorMapper;

    public ErrorHandler(ErrorMapper errorMapper) {
        this.errorMapper = errorMapper;
    }

    public void handle(
            RoutingContext rc,
            Throwable error,
            String resourceType) {
        try {
            HttpError httpError = errorMapper.map(error).orElseGet(() -> GenericErrorMapper.map(rc, error));
            rc.response()
                    .setStatusCode(httpError.getStatusCode())
                    .end(new ErrorRepresentation(httpError, resourceType, rc.normalisedPath()).toJsonString());
        } catch (RuntimeException ex) {
            log.error("Unhandled exception when mapping error", error);
            rc.fail(ex);
        }
    }
}
