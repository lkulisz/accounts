package io.lkulisz.accounts.http.error;

import com.fasterxml.jackson.databind.JsonMappingException;

import java.util.Optional;

public class JacksonJsonMappingMapper implements ErrorMapper {
    private static final int STATUS_CODE = 400;
    private static final String CODE = "invalid_payload";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!canHandle(error)) {
            return Optional.empty();
        }

        JsonMappingException mappingException;
        if (error instanceof JsonMappingException) {
            mappingException = (JsonMappingException) error;
        } else {
            mappingException = (JsonMappingException) error.getCause();
        }

        return Optional.of(new HttpError(STATUS_CODE, CODE, mappingException.getMessage()));
    }

    private boolean canHandle(Throwable error) {
        return error instanceof JsonMappingException ||
                (error.getCause() != null && error.getCause() instanceof JsonMappingException);
    }
}
