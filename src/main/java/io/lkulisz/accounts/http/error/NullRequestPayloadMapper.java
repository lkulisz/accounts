package io.lkulisz.accounts.http.error;

import java.util.Optional;

public class NullRequestPayloadMapper implements ErrorMapper {
    private static final int STATUS_CODE = 400;
    private static final String CODE = "invalid_payload";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!(error instanceof NullRequestPayloadException)) {
            return Optional.empty();
        }
        return Optional.of(new HttpError(STATUS_CODE, CODE, error.getMessage()));
    }
}
