package io.lkulisz.accounts.http.error;

public class NullRequestPayloadException extends RuntimeException {
    public NullRequestPayloadException() {
        super("`data`: must not be null");
    }

    public static void throwWhenNull(Object data) {
        if (data == null) {
            throw new NullRequestPayloadException();
        }
    }
}
