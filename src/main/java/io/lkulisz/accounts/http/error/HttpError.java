package io.lkulisz.accounts.http.error;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
public class HttpError {
    @JsonIgnore
    private int statusCode;
    private String code;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String details;
}
