package io.lkulisz.accounts.http.error;

import com.google.common.base.CaseFormat;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import java.util.Optional;

public class ConstraintViolationMapper implements ErrorMapper {
    private static final int STATUS_CODE = 422;
    private static final String CODE = "invalid_payload";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!(error instanceof ConstraintViolationException)) {
            return Optional.empty();
        }

        ConstraintViolationException exception = (ConstraintViolationException) error;
        ConstraintViolation<?> violation = exception.getConstraintViolations().iterator().next();
        String message = buildPath(violation)
                .map(path -> String.format("`%s`: %s", path, violation.getMessage()))
                .orElse(violation.getMessage());

        return Optional.of(new HttpError(STATUS_CODE, CODE, message));
    }

    private Optional<String> buildPath(ConstraintViolation<?> violation) {
        StringBuilder pathBuilder = new StringBuilder();
        for (Path.Node pathNode : violation.getPropertyPath()) {
            String pathNodeString = pathNode.toString();
            if (pathNodeString != null && !pathNodeString.equals("")) {
                pathBuilder.append('.');
                pathBuilder.append(toSnakeCase(pathNodeString));
            }
        }

        String result = pathBuilder.toString();
        return !result.equals("") ? Optional.of("data" + result) : Optional.empty();
    }

    private String toSnakeCase(String pathNode) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, pathNode);
    }
}