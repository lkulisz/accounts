package io.lkulisz.accounts.http.v1.accounts.representation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.lkulisz.accounts.domain.model.Transaction;
import io.lkulisz.accounts.domain.model.Withdrawal;
import io.lkulisz.accounts.domain.params.WithdrawalParams;
import io.lkulisz.accounts.http.util.Envelope;
import io.lkulisz.accounts.http.v1.accounts.AccountsPaths;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
public class WithdrawalRepresentation extends Envelope<WithdrawalRepresentation.Data, WithdrawalRepresentation.Meta> {
    public static final String TYPE = "Withdrawal";

    private WithdrawalRepresentation(Withdrawal withdrawal, AccountsPaths paths) {
        setData(new WithdrawalRepresentation.Data(withdrawal));
        setMeta(new WithdrawalRepresentation.Meta(withdrawal, paths));
    }

    public static WithdrawalRepresentation create(Transaction transaction, AccountsPaths paths) {
        if (!(transaction instanceof Withdrawal)) {
            throw new IllegalArgumentException("transaction must be an instance of Withdrawal");
        }
        return new WithdrawalRepresentation((Withdrawal) transaction, paths);
    }

    @NoArgsConstructor
    @lombok.Data
    public static class Data implements WithdrawalParams {
        @JsonSerialize(using = ToStringSerializer.class)
        private UUID id;
        @JsonSerialize(using = ToStringSerializer.class)
        private BigDecimal amount;
        private String title;
        @JsonSerialize(using = ToStringSerializer.class)
        private Instant instant;
        private UUID sourceAccountId;

        Data(Withdrawal withdrawal) {
            this.id = withdrawal.getId();
            this.amount = withdrawal.getAmount();
            this.title = withdrawal.getTitle();
            this.instant = withdrawal.getInstant();
            this.sourceAccountId = withdrawal.getSourceAccountId();
        }
    }

    public static class Meta extends Envelope.Meta<Envelope.Links> {
        @SuppressWarnings("unused")
        public Meta() {
            super(TYPE);
        }

        Meta(Withdrawal withdrawal, AccountsPaths paths) {
            super(TYPE, new Links(withdrawal, paths));
        }
    }

    @Getter
    public static class Links extends Envelope.Links {
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String account;

        Links(Withdrawal withdrawal, AccountsPaths paths) {
            super(paths.withdrawal(withdrawal.getSourceAccountId(), withdrawal.getId()));
            this.account = paths.account(withdrawal.getSourceAccountId());
        }
    }
}
