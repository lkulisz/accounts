package io.lkulisz.accounts.http.v1.accounts.error;

import com.google.inject.Provider;
import io.lkulisz.accounts.http.error.*;

public class AccountsErrorHandlerProvider implements Provider<ErrorHandler> {
    private static final ErrorMapper ERROR_MAPPER = CompositeErrorMapper.builder()
            .add(new DuplicateResourceMapper())
            .add(new AccountNotFoundMapper())
            .add(new JsonDecodeMapper())
            .add(new JacksonInvalidFormatMapper())
            .add(new JacksonUnrecognizedPropertyMapper())
            .add(new JacksonJsonMappingMapper())
            .add(new ConstraintViolationMapper())
            .add(new StaleAccountStateMapper())
            .add(new IllegalAccountStateMapper())
            .add(new NullRequestPayloadMapper())
            .add(new TransactionNotFoundMapper())
            .build();


    @Override
    public ErrorHandler get() {
        return new ErrorHandler(ERROR_MAPPER);
    }
}
