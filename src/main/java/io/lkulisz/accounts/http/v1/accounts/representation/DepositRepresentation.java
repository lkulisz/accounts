package io.lkulisz.accounts.http.v1.accounts.representation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.lkulisz.accounts.domain.model.Deposit;
import io.lkulisz.accounts.domain.model.Transaction;
import io.lkulisz.accounts.domain.params.DepositParams;
import io.lkulisz.accounts.http.util.Envelope;
import io.lkulisz.accounts.http.v1.accounts.AccountsPaths;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
public class DepositRepresentation extends Envelope<DepositRepresentation.Data, DepositRepresentation.Meta> {
    public static final String TYPE = "Deposit";

    private DepositRepresentation(Deposit deposit, AccountsPaths paths) {
        setData(new DepositRepresentation.Data(deposit));
        setMeta(new DepositRepresentation.Meta(deposit, paths));
    }

    public static DepositRepresentation create(Transaction transaction, AccountsPaths paths) {
        if (!(transaction instanceof Deposit)) {
            throw new IllegalArgumentException("transaction must an be instance of Deposit");
        }
        return new DepositRepresentation((Deposit) transaction, paths);
    }

    @NoArgsConstructor
    @lombok.Data
    public static class Data implements DepositParams {
        @JsonSerialize(using = ToStringSerializer.class)
        private UUID id;
        @JsonSerialize(using = ToStringSerializer.class)
        private BigDecimal amount;
        private String title;
        @JsonSerialize(using = ToStringSerializer.class)
        private Instant instant;
        private UUID destinationAccountId;

        Data(Deposit deposit) {
            this.id = deposit.getId();
            this.amount = deposit.getAmount();
            this.title = deposit.getTitle();
            this.instant = deposit.getInstant();
            this.destinationAccountId = deposit.getDestinationAccountId();
        }
    }

    public static class Meta extends Envelope.Meta<Envelope.Links> {
        @SuppressWarnings("unused")
        public Meta() {
            super(TYPE);
        }

        Meta(Deposit deposit, AccountsPaths paths) {
            super(TYPE, new Links(deposit, paths));
        }
    }

    @Getter
    public static class Links extends Envelope.Links {
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String account;

        Links(Deposit deposit, AccountsPaths paths) {
            super(paths.deposit(deposit.getDestinationAccountId(), deposit.getId()));
            this.account = paths.account(deposit.getDestinationAccountId());
        }
    }
}
