package io.lkulisz.accounts.http.v1.accounts.error;

import io.lkulisz.accounts.domain.exception.DuplicateAccountException;
import io.lkulisz.accounts.domain.exception.DuplicateTransactionException;
import io.lkulisz.accounts.http.error.ErrorMapper;
import io.lkulisz.accounts.http.error.HttpError;

import java.util.Optional;

public class DuplicateResourceMapper implements ErrorMapper {
    private static final int STATUS_CODE = 409;
    private static final String CODE = "duplicate";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!(error instanceof DuplicateAccountException || error instanceof DuplicateTransactionException)) {
            return Optional.empty();
        }
        return Optional.of(new HttpError(STATUS_CODE, CODE, error.getMessage()));
    }
}
