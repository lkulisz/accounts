package io.lkulisz.accounts.http.v1.accounts.representation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.lkulisz.accounts.domain.model.Transaction;
import io.lkulisz.accounts.domain.model.Transfer;
import io.lkulisz.accounts.domain.params.TransferParams;
import io.lkulisz.accounts.http.util.Envelope;
import io.lkulisz.accounts.http.v1.accounts.AccountsPaths;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

@NoArgsConstructor
public class TransferRepresentation extends Envelope<TransferRepresentation.Data, TransferRepresentation.Meta> {
    public static final String TYPE = "Transfer";

    private TransferRepresentation(Transfer transfer, AccountsPaths paths) {
        setData(new TransferRepresentation.Data(transfer));
        setMeta(new TransferRepresentation.Meta(transfer, paths));
    }

    public static TransferRepresentation create(Transaction transaction, AccountsPaths paths) {
        if (!(transaction instanceof Transfer)) {
            throw new IllegalArgumentException("transaction must be an instance of Transfer");
        }
        return new TransferRepresentation((Transfer) transaction, paths);
    }

    @NoArgsConstructor
    @lombok.Data
    public static class Data implements TransferParams {
        @JsonSerialize(using = ToStringSerializer.class)
        private UUID id;
        @JsonSerialize(using = ToStringSerializer.class)
        private BigDecimal amount;
        private String title;
        @JsonSerialize(using = ToStringSerializer.class)
        private Instant instant;
        private UUID sourceAccountId;
        private UUID destinationAccountId;

        Data(Transfer transfer) {
            this.id = transfer.getId();
            this.amount = transfer.getAmount();
            this.title = transfer.getTitle();
            this.instant = transfer.getInstant();
            this.sourceAccountId = transfer.getSourceAccountId();
            this.destinationAccountId = transfer.getDestinationAccountId();
        }
    }

    public static class Meta extends Envelope.Meta<Envelope.Links> {
        @SuppressWarnings("unused")
        public Meta() {
            super(TYPE);
        }

        Meta(Transfer transfer, AccountsPaths paths) {
            super(TYPE, new Links(transfer, paths));
        }
    }

    @Getter
    public static class Links extends Envelope.Links {
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String sourceAccount;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String destinationAccount;

        Links(Transfer transfer, AccountsPaths paths) {
            super(paths.transfer(transfer.getSourceAccountId(), transfer.getId()));
            this.sourceAccount = paths.account(transfer.getSourceAccountId());
            this.destinationAccount = paths.account(transfer.getDestinationAccountId());
        }
    }
}
