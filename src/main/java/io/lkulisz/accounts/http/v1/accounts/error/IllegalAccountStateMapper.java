package io.lkulisz.accounts.http.v1.accounts.error;

import io.lkulisz.accounts.domain.exception.IllegalAccountStateException;
import io.lkulisz.accounts.http.error.ErrorMapper;
import io.lkulisz.accounts.http.error.HttpError;

import java.util.Optional;

public class IllegalAccountStateMapper implements ErrorMapper {
    private static final int STATUS_CODE = 422;
    private static final String CODE = "illegal_state";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!(error instanceof IllegalAccountStateException)) {
            return Optional.empty();
        }
        return Optional.of(new HttpError(STATUS_CODE, CODE, error.getMessage()));
    }
}
