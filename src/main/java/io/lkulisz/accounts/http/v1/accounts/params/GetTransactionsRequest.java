package io.lkulisz.accounts.http.v1.accounts.params;

import io.lkulisz.accounts.domain.model.TransactionType;
import io.lkulisz.accounts.domain.params.TransactionsQueryCriteria;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.UUID;

@AllArgsConstructor
public class GetTransactionsRequest implements TransactionsQueryCriteria {
    @Getter
    private final UUID accountId;
    private final TransactionType transactionType;

    @Override
    public Optional<TransactionType> getTransactionType() {
        return Optional.ofNullable(transactionType);
    }
}
