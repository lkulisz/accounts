package io.lkulisz.accounts.http.v1.accounts.representation;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.lkulisz.accounts.http.util.Envelope;
import io.lkulisz.accounts.http.v1.accounts.AccountsPaths;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@NoArgsConstructor
public class AccountCloseRepresentation extends Envelope<Void, AccountCloseRepresentation.Meta>{
    public static final String TYPE = "AccountClose";

    public AccountCloseRepresentation(UUID accountId, AccountsPaths paths) {
        setMeta(new Meta(accountId, paths));
    }

    public static class Meta extends Envelope.Meta<Envelope.Links> {
        @SuppressWarnings("unused")
        public Meta() {
            super(TYPE);
        }

        Meta(UUID accountId, AccountsPaths paths) {
            super(TYPE, new Links(accountId, paths));
        }
    }

    @Getter
    public static class Links extends Envelope.Links {
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String account;

        Links(UUID accountId, AccountsPaths paths) {
            super(paths.accountClose(accountId));
            account = paths.account(accountId);
        }
    }
}
