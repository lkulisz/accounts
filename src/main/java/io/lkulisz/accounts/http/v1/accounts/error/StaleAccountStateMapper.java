package io.lkulisz.accounts.http.v1.accounts.error;

import io.lkulisz.accounts.domain.exception.StaleAccountStateException;
import io.lkulisz.accounts.http.error.ErrorMapper;
import io.lkulisz.accounts.http.error.HttpError;

import java.util.Optional;

public class StaleAccountStateMapper implements ErrorMapper {
    private static final int STATUS_CODE = 409;
    private static final String CODE = "conflict";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!(error instanceof StaleAccountStateException)) {
            return Optional.empty();
        }
        return Optional.of(new HttpError(STATUS_CODE, CODE, error.getMessage()));
    }
}
