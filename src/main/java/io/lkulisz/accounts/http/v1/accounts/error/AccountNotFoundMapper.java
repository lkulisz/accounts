package io.lkulisz.accounts.http.v1.accounts.error;

import io.lkulisz.accounts.domain.exception.AccountNotFoundException;
import io.lkulisz.accounts.http.error.ErrorMapper;
import io.lkulisz.accounts.http.error.HttpError;

import java.util.Optional;

public class AccountNotFoundMapper implements ErrorMapper {
    private static final int STATUS_CODE = 404;
    private static final String CODE = "not_found";

    @Override
    public Optional<HttpError> map(Throwable error) {
        if (!(error instanceof AccountNotFoundException)) {
            return Optional.empty();
        }
        return Optional.of(new HttpError(STATUS_CODE, CODE, error.getMessage()));
    }
}
