package io.lkulisz.accounts.http.v1.accounts.representation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.lkulisz.accounts.domain.model.Account;
import io.lkulisz.accounts.domain.params.AccountOpenParams;
import io.lkulisz.accounts.http.util.Envelope;
import io.lkulisz.accounts.http.v1.accounts.AccountsPaths;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.UUID;

@NoArgsConstructor
public class AccountRepresentation extends Envelope<AccountRepresentation.Data, AccountRepresentation.Meta>{
    public static final String TYPE = "Account";

    public AccountRepresentation(Account account, AccountsPaths paths) {
        setData(new AccountRepresentation.Data(account));
        setMeta(new AccountRepresentation.Meta(account, paths));
    }

    @NoArgsConstructor
    @lombok.Data
    public static class Data implements AccountOpenParams {
        @JsonSerialize(using = ToStringSerializer.class)
        private BigDecimal balance;
        private UUID id;
        private UUID ownerId;
        private String name;
        private boolean closed;

        Data(Account account) {
            this.id = account.getId();
            this.ownerId = account.getOwnerId();
            this.name = account.getName();
            this.balance = account.getBalance();
            this.closed = account.isClosed();
        }
    }

    public static class Meta extends Envelope.Meta<Envelope.Links> {
        @SuppressWarnings("unused")
        public Meta() {
            super(TYPE);
        }

        Meta(Account account, AccountsPaths paths) {
            super(TYPE, new Links(account, paths));
        }
    }

    @Getter
    public static class Links extends Envelope.Links {
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String close;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String deposits;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String withdrawals;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String transfers;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private String transactions;

        Links(Account account, AccountsPaths paths) {
            super(paths.account(account.getId()));
            if (account.canCloseAccount()) {
                close = paths.accountClose(account.getId());
            }
            deposits = paths.deposits(account.getId());
            withdrawals = paths.withdrawals(account.getId());
            transfers = paths.transfers(account.getId());
            transactions = paths.transactions(account.getId());
        }
    }
}
