package io.lkulisz.accounts.http.v1.accounts;

import com.google.inject.Singleton;

import java.util.UUID;

@Singleton
public class AccountsPaths {
    static final String ACCOUNTS = "/";
    static final String ACCOUNT = "/:account_id";
    static final String ACCOUNT_CLOSE = "/:account_id/close";
    static final String DEPOSITS = "/:account_id/deposits";
    static final String DEPOSIT = "/:account_id/deposits/:transaction_id";
    static final String WITHDRAWALS = "/:account_id/withdrawals";
    static final String WITHDRAWAL = "/:account_id/withdrawals/:transaction_id";
    static final String TRANSFERS = "/:account_id/transfers";
    static final String TRANSFER = "/:account_id/transfers/:transaction_id";
    static final String TRANSACTIONS = "/:account_id/transactions";

    static final String ACCOUNT_ID = "account_id";
    static final String TRANSACTION_ID = "transaction_id";

    private static final String ACCOUNT_ID_PARAM = ":account_id";
    private static final String TRANSACTION_ID_PARAM = ":transaction_id";

    private String basePath;

    void configure(String basePath) {
        if (basePath == null) {
            throw new IllegalArgumentException("baseBath must not be null");
        }
        this.basePath = basePath;
    }

    private String basePath() {
        if (basePath == null) {
            throw new IllegalStateException("Not configured");
        }

        return basePath;
    }

    public String account(UUID accountId) {
        return basePath() + ACCOUNT.replace(ACCOUNT_ID_PARAM, accountId.toString());
    }

    public String accountClose(UUID accountId) {
        return basePath() + ACCOUNT_CLOSE.replace(ACCOUNT_ID_PARAM, accountId.toString());
    }

    public String deposits(UUID accountId) {
        return basePath() + DEPOSITS.replace(ACCOUNT_ID_PARAM, accountId.toString());
    }

    public String deposit(UUID accountId, UUID depositId) {
        return basePath() + DEPOSIT
                .replace(ACCOUNT_ID_PARAM, accountId.toString())
                .replace(TRANSACTION_ID_PARAM, depositId.toString());
    }

    public String withdrawals(UUID accountId) {
        return basePath() + WITHDRAWALS.replace(ACCOUNT_ID_PARAM, accountId.toString());
    }

    public String withdrawal(UUID accountId, UUID withdrawalId) {
        return basePath() + WITHDRAWAL
                .replace(ACCOUNT_ID_PARAM, accountId.toString())
                .replace(TRANSACTION_ID_PARAM, withdrawalId.toString());
    }

    public String transfers(UUID accountId) {
        return basePath() + TRANSFERS.replace(ACCOUNT_ID_PARAM, accountId.toString());
    }

    public String transfer(UUID accountId, UUID transferId) {
        return basePath() + TRANSFER
                .replace(ACCOUNT_ID_PARAM, accountId.toString())
                .replace(TRANSACTION_ID_PARAM, transferId.toString());
    }
    public String transactions(UUID accountId) {
        return basePath() + TRANSACTIONS.replace(ACCOUNT_ID_PARAM, accountId.toString());
    }
}
