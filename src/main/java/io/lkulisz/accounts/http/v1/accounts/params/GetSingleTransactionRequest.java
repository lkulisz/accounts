package io.lkulisz.accounts.http.v1.accounts.params;

import io.lkulisz.accounts.domain.model.TransactionType;
import io.lkulisz.accounts.domain.params.SingleTransactionQueryCriteria;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class GetSingleTransactionRequest implements SingleTransactionQueryCriteria {
    private final UUID accountId;
    private final UUID transactionId;
    private final TransactionType transactionType;
}
