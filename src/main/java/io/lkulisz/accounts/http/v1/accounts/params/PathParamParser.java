package io.lkulisz.accounts.http.v1.accounts.params;

import io.lkulisz.accounts.domain.exception.AccountNotFoundException;
import io.lkulisz.accounts.domain.exception.TransactionNotFoundException;

import java.util.UUID;

public class PathParamParser {
    public static UUID parseAccountId(String value) {
        try {
            return UUID.fromString(value);
        } catch (IllegalArgumentException ex) {
            throw new AccountNotFoundException(value);
        }
    }

    public static UUID parseTransactionId(String value) {
        try {
            return UUID.fromString(value);
        } catch (IllegalArgumentException ex) {
            throw new TransactionNotFoundException(value);
        }
    }
}
