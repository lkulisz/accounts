package io.lkulisz.accounts.http.v1.accounts;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import io.lkulisz.accounts.domain.model.Account;
import io.lkulisz.accounts.domain.model.Transaction;
import io.lkulisz.accounts.domain.model.TransactionType;
import io.lkulisz.accounts.domain.service.AccountsService;
import io.lkulisz.accounts.http.error.ErrorHandler;
import io.lkulisz.accounts.http.error.NullRequestPayloadException;
import io.lkulisz.accounts.http.util.CollectionEnvelope;
import io.lkulisz.accounts.http.util.Representation;
import io.lkulisz.accounts.http.v1.accounts.params.GetSingleTransactionRequest;
import io.lkulisz.accounts.http.v1.accounts.params.GetTransactionsRequest;
import io.lkulisz.accounts.http.v1.accounts.params.PathParamParser;
import io.lkulisz.accounts.http.v1.accounts.representation.*;
import io.vertx.core.json.JsonObject;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.RoutingContext;
import lombok.extern.slf4j.Slf4j;
import rx.Single;

import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;

import static io.lkulisz.accounts.domain.model.TransactionType.*;
import static io.lkulisz.accounts.http.util.HttpConstants.APPLICATION_JSON;
import static io.lkulisz.accounts.http.v1.accounts.params.PathParamParser.parseAccountId;
import static io.lkulisz.accounts.http.v1.accounts.params.PathParamParser.parseTransactionId;

@Slf4j
public class AccountsController {
    private static final Map<TransactionType, BiFunction<Transaction, AccountsPaths, Representation>>
            TRANSACTION_REPRESENTATIONS = ImmutableMap.of(
            DEPOSIT, DepositRepresentation::create,
            WITHDRAWAL, WithdrawalRepresentation::create,
            TRANSFER, TransferRepresentation::create
    );

    private final Vertx vertx;
    private final AccountsService service;
    private final AccountsPaths paths;
    private final ErrorHandler errorHandler;

    @Inject
    public AccountsController(Vertx vertx, AccountsService service, AccountsPaths paths, ErrorHandler errorHandler) {
        this.vertx = vertx;
        this.service = service;
        this.paths = paths;
        this.errorHandler = errorHandler;
    }

    public Router configureRoutes(String basePath) {
        paths.configure(basePath);

        Router router = Router.router(vertx);
        router.post(AccountsPaths.ACCOUNTS).consumes(APPLICATION_JSON).handler(this::create);
        router.get(AccountsPaths.ACCOUNT).produces(APPLICATION_JSON).handler(this::get);
        router.post(AccountsPaths.ACCOUNT_CLOSE).produces(APPLICATION_JSON).handler(this::close);

        router.post(AccountsPaths.DEPOSITS).produces(APPLICATION_JSON).handler(this::createDeposit);
        router.get(AccountsPaths.DEPOSIT).produces(APPLICATION_JSON).handler(this::getDeposit);
        router.get(AccountsPaths.DEPOSITS).produces(APPLICATION_JSON).handler(this::getDeposits);

        router.post(AccountsPaths.WITHDRAWALS).produces(APPLICATION_JSON).handler(this::createWithdrawal);
        router.get(AccountsPaths.WITHDRAWAL).produces(APPLICATION_JSON).handler(this::getWithdrawal);
        router.get(AccountsPaths.WITHDRAWALS).produces(APPLICATION_JSON).handler(this::getWithdrawals);

        router.post(AccountsPaths.TRANSFERS).produces(APPLICATION_JSON).handler(this::createTransfer);
        router.get(AccountsPaths.TRANSFER).produces(APPLICATION_JSON).handler(this::getTransfer);
        router.get(AccountsPaths.TRANSFERS).produces(APPLICATION_JSON).handler(this::getTransfers);

        router.get(AccountsPaths.TRANSACTIONS).produces(APPLICATION_JSON).handler(this::getTransactions);

        return router;
    }

    private void create(RoutingContext rc) {
        Single.just(rc)
                .map(context -> context.getBodyAsJson().mapTo(AccountRepresentation.class).getData())
                .doOnSuccess(NullRequestPayloadException::throwWhenNull)
                .flatMap(service::openAccount)
                .map(this::representAccount)
                .subscribe(
                        representation -> rc.response().setStatusCode(201).end(representation.toJsonString()),
                        error -> errorHandler.handle(rc, error, AccountRepresentation.TYPE)
                );
    }

    private void get(RoutingContext rc) {
        Single.just(rc.pathParam(AccountsPaths.ACCOUNT_ID))
                .map(PathParamParser::parseAccountId)
                .flatMap(service::getAccount)
                .map(this::representAccount)
                .subscribe(
                        representation -> rc.response().setStatusCode(200).end(representation.toJsonString()),
                        error -> errorHandler.handle(rc, error, AccountRepresentation.TYPE)
                );
    }

    private void close(RoutingContext rc) {
        Single.just(rc.pathParam(AccountsPaths.ACCOUNT_ID))
                .map(PathParamParser::parseAccountId)
                .flatMap(service::closeAccount)
                .map(this::representAccountClose)
                .subscribe(
                        representation -> rc.response()
                                .setStatusCode(201)
                                .end(representation.toJsonString()),
                        error -> errorHandler.handle(rc, error, AccountCloseRepresentation.TYPE)
                );
    }
    private void createDeposit(RoutingContext rc) {
        createTransaction(rc, DepositRepresentation.TYPE, (accountId, payload) -> {
            DepositRepresentation request = payload.mapTo(DepositRepresentation.class);
            request.getData().setDestinationAccountId(accountId);
            return service.deposit(request.getData());
        });
    }

    private void getDeposit(RoutingContext rc) {
        getTransaction(rc, TransactionType.DEPOSIT, DepositRepresentation.TYPE);
    }

    private void getDeposits(RoutingContext rc) {
        getTransactions(rc, TransactionType.DEPOSIT);
    }

    private void createWithdrawal(RoutingContext rc) {
        createTransaction(rc, WithdrawalRepresentation.TYPE, (accountId, payload) -> {
            WithdrawalRepresentation request = payload.mapTo(WithdrawalRepresentation.class);
            request.getData().setSourceAccountId(accountId);
            return service.withdraw(request.getData());
        });
    }

    private void getWithdrawal(RoutingContext rc) {
        getTransaction(rc, WITHDRAWAL, WithdrawalRepresentation.TYPE);
    }

    private void getWithdrawals(RoutingContext rc) {
        getTransactions(rc, WITHDRAWAL);
    }

    private void createTransfer(RoutingContext rc) {
        createTransaction(rc, TransferRepresentation.TYPE, (accountId, payload) -> {
            TransferRepresentation request = payload.mapTo(TransferRepresentation.class);
            request.getData().setSourceAccountId(accountId);
            return service.transfer(request.getData());
        });
    }

    private void getTransfer(RoutingContext rc) {
        getTransaction(rc, TRANSFER, TransferRepresentation.TYPE);
    }

    private void getTransfers(RoutingContext rc) {
        getTransactions(rc, TRANSFER);
    }

    private void getTransactions(RoutingContext rc) {
        getTransactions(rc, null);
    }

    private void createTransaction(
            RoutingContext rc,
            String representationType,
            BiFunction<UUID, JsonObject, Single<? extends Transaction>> handler) {
        Single.just(rc.pathParam(AccountsPaths.ACCOUNT_ID))
                .map(PathParamParser::parseAccountId)
                .flatMap(accountId -> {
                    JsonObject body = rc.getBodyAsJson();
                    if (!body.containsKey("data")) {
                        throw new NullRequestPayloadException();
                    }
                    return handler.apply(accountId, body);
                })
                .map(this::representTransaction)
                .subscribe(
                        representation -> rc.response()
                                .setStatusCode(201)
                                .end(representation.toJsonString()),
                        error -> errorHandler.handle(rc, error, representationType)
                );
    }

    private void getTransaction(
            RoutingContext rc,
            TransactionType transactionType,
            String resourceType) {
        parseGetSingleTransactionRequest(rc, transactionType)
                .flatMap(service::getTransaction)
                .map(this::representTransaction)
                .subscribe(
                        representation -> rc.response().setStatusCode(200).end(representation.toJsonString()),
                        error -> errorHandler.handle(rc, error, resourceType)
                );
    }

    private Single<GetSingleTransactionRequest> parseGetSingleTransactionRequest(
            RoutingContext rc,
            TransactionType transactionType) {
        try {
            return Single.just(new GetSingleTransactionRequest(
                    parseAccountId(rc.pathParam(AccountsPaths.ACCOUNT_ID)),
                    parseTransactionId(rc.pathParam(AccountsPaths.TRANSACTION_ID)),
                    transactionType));
        } catch (Exception ex) {
            return Single.error(ex);
        }
    }

    private void getTransactions(
            RoutingContext rc,
            TransactionType transactionType) {
        Single.just(rc.pathParam(AccountsPaths.ACCOUNT_ID))
                .map(PathParamParser::parseAccountId)
                .map(accountId -> new GetTransactionsRequest(accountId, transactionType))
                .flatMapObservable(service::getTransactions)
                .map(this::representTransaction)
                .toList()
                .toSingle()
                .map(CollectionEnvelope::new)
                .subscribe(
                        representation -> rc.response().setStatusCode(200).end(representation.toJsonString()),
                        error -> errorHandler.handle(rc, error, CollectionEnvelope.TYPE)
                );
    }

    private AccountRepresentation representAccount(Account account) {
        return new AccountRepresentation(account, paths);
    }

    private AccountCloseRepresentation representAccountClose(Account account) {
        return new AccountCloseRepresentation(account.getId(), paths);
    }

    private Representation representTransaction(Transaction transaction) {
        BiFunction<Transaction, AccountsPaths, Representation> representationFactory =
                TRANSACTION_REPRESENTATIONS.get(transaction.getType());

        if (representationFactory == null) {
            throw new IllegalStateException(String.format("No representation found for transaction of type %s", transaction.getClass()));
        }

        return representationFactory.apply(transaction, paths);
    }
}
