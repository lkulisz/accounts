package io.lkulisz.accounts.http;

import com.google.inject.Inject;
import io.lkulisz.accounts.Config;
import io.lkulisz.accounts.http.util.HttpConstants;
import io.lkulisz.accounts.http.v1.accounts.AccountsController;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.http.HttpServer;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.handler.BodyHandler;
import io.vertx.rxjava.ext.web.handler.LoggerHandler;
import io.vertx.rxjava.ext.web.handler.ResponseContentTypeHandler;
import lombok.extern.slf4j.Slf4j;
import rx.Single;

@Slf4j
public class ServerInitializer {
    private final Vertx vertx;
    private final Config config;
    private final AccountsController accountsController;

    @Inject
    public ServerInitializer(
            Vertx vertx,
            Config config,
            AccountsController accountsController) {
        this.vertx = vertx;
        this.config = config;
        this.accountsController = accountsController;
    }

    public Single<HttpServer> startServer() {
        Router mainRouter = buildRouter();
        return vertx.createHttpServer()
                .requestHandler(mainRouter::accept)
                .rxListen(config.getHttpPort())
                .doOnSuccess(httpServer -> log.info("HTTP server running on port {}", config.getHttpPort()))
                .doOnError(failure -> log.error("Could not start HTTP server", failure));
    }

    private Router buildRouter() {
        Router mainRouter = Router.router(vertx);
        mainRouter.route().handler(LoggerHandler.create());
        mainRouter.route().handler(BodyHandler.create());
        mainRouter.route().produces(HttpConstants.APPLICATION_JSON).handler(ResponseContentTypeHandler.create());
        mainRouter.mountSubRouter("/v1", buildV1Router("/v1"));
        return mainRouter;
    }

    private Router buildV1Router(String basePath) {
        Router router = Router.router(vertx);
        router.mountSubRouter("/accounts", accountsController.configureRoutes(basePath + "/accounts"));
        return router;
    }
}
