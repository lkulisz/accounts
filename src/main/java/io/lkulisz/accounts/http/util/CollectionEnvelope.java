package io.lkulisz.accounts.http.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class CollectionEnvelope implements Representation {
    public static final String TYPE = "Collection";

    @Getter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Representation> items;
    @Getter
    private Meta meta = new Meta();

    public CollectionEnvelope(List<Representation> items) {
        this.setItems(items);
    }

    private void setItems(List<Representation> items) {
        this.items = items;
        this.meta.setSize(items.size());
    }

    @Getter
    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Meta {
        private final String type = TYPE;
        @Setter
        private int size = 0;
    }
}
