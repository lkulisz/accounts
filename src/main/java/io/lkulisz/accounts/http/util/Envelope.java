package io.lkulisz.accounts.http.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.lkulisz.accounts.http.error.HttpError;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
public class Envelope<TData, TMeta extends Envelope.Meta<?>> implements Representation {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private TData data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private HttpError error;
    private TMeta meta;

    @Getter
    @AllArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Meta<TLinks extends Links> {
        private final String type;
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private final TLinks links;

        public Meta(String type) {
            this.type = type;
            this.links = null;
        }
    }

    @Getter
    @AllArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Links {
        @JsonInclude(JsonInclude.Include.NON_NULL)
        private final String self;

        public Links() {
            this.self = null;
        }
    }
}
