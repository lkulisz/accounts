package io.lkulisz.accounts.http.util;

import io.vertx.core.json.JsonObject;

public interface Representation {
    default String toJsonString() {
        return JsonObject.mapFrom(this).encode();
    }
}
