CREATE TABLE IF NOT EXISTS accounts (
  id INTEGER IDENTITY PRIMARY KEY,
  uuid UUID UNIQUE NOT NULL,
  owner_id UUID NOT NULL,
  name VARCHAR(1024) NOT NULL,
  balance DECIMAL(20,4) NOT NULL,
  is_closed BOOLEAN DEFAULT FALSE NOT NULL,
  version INTEGER DEFAULT 1 NOT NULL
);

CREATE TABLE IF NOT EXISTS transactions (
  id INTEGER IDENTITY PRIMARY KEY,
  uuid UUID UNIQUE NOT NULL,
  type VARCHAR(100) NOT NULL,
  title VARCHAR(1024) NOT NULL,
  amount DECIMAL(20,4) NOT NULL,
  instant TIMESTAMP NOT NULL,
  source_account_id INTEGER DEFAULT NULL,
  source_account_uuid UUID DEFAULT NULL,
  destination_account_id INTEGER DEFAULT NULL,
  destination_account_uuid UUID DEFAULT NULL,

  FOREIGN KEY (source_account_id) REFERENCES accounts(id),
  FOREIGN KEY (destination_account_id) REFERENCES accounts(id)
);
