package io.lkulisz.accounts.domain.service;

import com.google.inject.Inject;
import com.google.inject.Provider;
import io.lkulisz.accounts.domain.exception.AccountNotFoundException;
import io.lkulisz.accounts.domain.exception.DuplicateAccountException;
import io.lkulisz.accounts.domain.exception.IllegalAccountStateException;
import io.lkulisz.accounts.domain.model.Deposit;
import io.lkulisz.accounts.domain.model.TransactionType;
import io.lkulisz.accounts.domain.model.Transfer;
import io.lkulisz.accounts.domain.model.Withdrawal;
import io.lkulisz.accounts.domain.params.*;
import io.lkulisz.accounts.domain.repository.AccountsRepository;
import io.lkulisz.accounts.testing.Matchers;
import io.lkulisz.accounts.testing.ServiceTest;
import io.lkulisz.accounts.testing.TestSubscriber;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import rx.Observable;
import rx.Single;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import static io.lkulisz.accounts.testing.ModelFactory.*;

@RunWith(VertxUnitRunner.class)
public class AccountsServiceTest extends ServiceTest {
    @Inject
    private AccountsService service;
    @Inject
    private Provider<AccountsRepository> repositoryProvider;

    @Test
    public void openAccountShouldStoreAccountInDb(TestContext context) {
        AccountsRepository repository = repositoryProvider.get();
        AccountOpenParams accountParams = buildTestAccountOpenParams();
        Async async = context.async();
        service.openAccount(accountParams)
                .flatMap(account -> repository.getAccountById(accountParams.getId()))
                .doOnSuccess(storedAccount -> {
                        context.assertEquals(accountParams.getId(), storedAccount.getId());
                        context.assertEquals(accountParams.getOwnerId(), storedAccount.getOwnerId());
                        context.assertEquals(accountParams.getName(), storedAccount.getName());
                        context.assertEquals(new BigDecimal("0.0000"), storedAccount.getBalance());
                        context.assertFalse(storedAccount.isClosed());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void openAccountShouldThrowWhenTryingToOpenAccountWithDuplicateId(TestContext context) {
        AccountOpenParams accountParams = buildTestAccountOpenParams();

        Async async = context.async();
        Single.concat(service.openAccount(accountParams), service.openAccount(accountParams))
                .onErrorResumeNext(error -> {
                    context.assertEquals(DuplicateAccountException.class, error.getClass());
                    context.assertEquals(accountParams.getId(), ((DuplicateAccountException) error).getId());
                    return Observable.empty();
                })
                .count()
                .doOnNext(successCount -> context.assertEquals(1, successCount, "Only first call should succeed"))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getAccountShouldThrowWhenAccountDoesNotExist(TestContext context) {
        UUID id = UUID.randomUUID();
        Async async = context.async();
        service.getAccount(id)
                .doOnSuccess(account -> context.fail("Expected AccountNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(AccountNotFoundException.class, error.getClass());
                    context.assertEquals(id.toString(), ((AccountNotFoundException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void openAccountShouldThrowWhenIdIsNotPresent(TestContext context) {
        AccountOpenParams accountParams = buildTestAccountOpenParams(builder -> builder.id(null));
        expectConstraintViolation(context, service.openAccount(accountParams), "id");
    }

    @Test
    public void closeAccountShouldUpdateStateOfAccount(TestContext context) {
        AccountsRepository repository = repositoryProvider.get();
        Async async = context.async();
        service.openAccount(buildTestAccountOpenParams())
                .flatMap(account -> service.closeAccount(account.getId()))
                .flatMap(account -> repository.getAccountById(account.getId()))
                .doOnSuccess(account -> context.assertTrue(account.isClosed()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void closeAccountShouldThrowWhenAccountDoesNotExist(TestContext context) {
        UUID id = UUID.randomUUID();
        Async async = context.async();
        service.closeAccount(id)
                .doOnSuccess(account -> context.fail("Expected AccountNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(AccountNotFoundException.class, error.getClass());
                    context.assertEquals(id.toString(), ((AccountNotFoundException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void closeAccountShouldThrowWhenAccountHasPositiveBalance(TestContext context) {
        AccountOpenParams accountOpen = buildTestAccountOpenParams();

        Async async = context.async();
        service.openAccount(accountOpen)
                .flatMap(account -> service.deposit(buildTestDepositParams(account.getId())))
                .flatMap(transaction -> service.closeAccount(accountOpen.getId()))
                .doOnSuccess(account -> context.fail("Expected IllegalAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(IllegalAccountStateException.class, error.getClass());
                    context.assertEquals(accountOpen.getId(), ((IllegalAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void depositShouldUpdateAccountBalance(TestContext context) {
        UUID accountId = UUID.randomUUID();
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams(a -> a.id(accountId));
        DepositParams deposit = buildTestDepositParams(d -> d
                .destinationAccountId(accountOpenParams.getId())
                .amount(BigDecimal.valueOf(123.45)));
        DepositParams secondDeposit = buildTestDepositParams(d -> d
                .amount(BigDecimal.valueOf(100.00))
                .destinationAccountId(accountId));

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        service.openAccount(accountOpenParams)
                .flatMap(x -> service.deposit(deposit))
                .flatMap(x -> service.deposit(secondDeposit))
                .flatMap(transaction -> repository.getAccountById(transaction.getDestinationAccountId()))
                .doOnSuccess(account -> context.assertEquals("223.4500", account.getBalance().toString()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void depositShouldSaveTransactionDetails(TestContext context) {
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams();
        DepositParams deposit = buildTestDepositParams(d -> d
                .destinationAccountId(accountOpenParams.getId())
                .amount(new BigDecimal("123.4500")));

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        service.openAccount(accountOpenParams)
                .flatMap(x -> service.deposit(deposit))
                .flatMap(account -> repository.getTransaction(buildQuerySingleCriteria(deposit)))
                .doOnSuccess(transaction -> {
                    context.assertTrue(Matchers.eq((Deposit)transaction).matches(deposit));
                    context.assertFalse(transaction.getInstant().isAfter(Instant.now()));
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void depositShouldThrowWhenAccountDoesNotExist(TestContext context) {
        DepositParams depositParams = buildTestDepositParams();

        Async async = context.async();
        service.deposit(depositParams)
                .doOnSuccess(account -> context.fail("Expected AccountNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(AccountNotFoundException.class, error.getClass());
                    context.assertEquals(depositParams.getDestinationAccountId().toString(), ((AccountNotFoundException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void depositShouldThrowWhenAccountIsClosed(TestContext context) {
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams();
        DepositParams depositParams = buildTestDepositParams(accountOpenParams.getId());

        Async async = context.async();
        service.openAccount(accountOpenParams)
                .flatMap(savedAccount -> service.closeAccount(savedAccount.getId()))
                .flatMap(x -> service.deposit(depositParams))
                .doOnSuccess(account -> context.fail("Expected IllegalAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(IllegalAccountStateException.class, error.getClass());
                    context.assertEquals(depositParams.getDestinationAccountId(), ((IllegalAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void depositShouldThrowWhenAmountIsNegative(TestContext context) {
        DepositParams depositParams = buildTestDepositParams(deposit -> deposit.amount(BigDecimal.valueOf(-10.123)));
        expectConstraintViolation(context, service.deposit(depositParams), "amount");
    }

    @Test
    public void withdrawShouldUpdateAccountBalance(TestContext context) {
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams();
        DepositParams depositParams = buildTestDepositParams(d -> d
                .destinationAccountId(accountOpenParams.getId())
                .amount(BigDecimal.valueOf(100.00))
        );
        WithdrawalParams firstWithdrawal = buildTestWithdrawalParams(w -> w
                .sourceAccountId(accountOpenParams.getId())
                .amount(BigDecimal.valueOf(20.00))
        );
        WithdrawalParams secondWithdrawal = buildTestWithdrawalParams(w -> w
                .sourceAccountId(accountOpenParams.getId())
                .amount(BigDecimal.valueOf(15.00))
        );

        AccountsRepository repository = repositoryProvider.get();
        Async async = context.async();
        service.openAccount(accountOpenParams)
                .flatMap(x -> service.deposit(depositParams))
                .flatMap(x -> service.withdraw(firstWithdrawal))
                .flatMap(x -> service.withdraw(secondWithdrawal))
                .flatMap(transaction -> repository.getAccountById(transaction.getSourceAccountId()))
                .doOnSuccess(account -> context.assertEquals("65.0000", account.getBalance().toString()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void withdrawShouldSaveTransactionDetails(TestContext context) {
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams();
        DepositParams depositParams = buildTestDepositParams(d -> d
                .destinationAccountId(accountOpenParams.getId())
                .amount(BigDecimal.valueOf(100.00))
        );
        WithdrawalParams withdrawalParams = buildTestWithdrawalParams(w -> w
                .sourceAccountId(accountOpenParams.getId())
                .amount(new BigDecimal("20.0000"))
        );

        AccountsRepository repository = repositoryProvider.get();
        Async async = context.async();
        service.openAccount(accountOpenParams)
                .flatMap(x -> service.deposit(depositParams))
                .flatMap(x -> service.withdraw(withdrawalParams))
                .flatMap(account -> repository.getTransaction(buildQuerySingleCriteria(withdrawalParams)))
                .doOnSuccess(transaction -> {
                    context.assertTrue(Matchers.eq((Withdrawal)transaction).matches(withdrawalParams));
                    context.assertFalse(transaction.getInstant().isAfter(Instant.now()));
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void withdrawShouldThrowWhenAccountDoesNotExist(TestContext context) {
        WithdrawalParams withdrawalParams = buildTestWithdrawalParams();

        Async async = context.async();
        service.withdraw(withdrawalParams)
                .doOnSuccess(account -> context.fail("Expected AccountNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(AccountNotFoundException.class, error.getClass());
                    context.assertEquals(withdrawalParams.getSourceAccountId().toString(), ((AccountNotFoundException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void withdrawShouldThrowWhenAccountIsClosed(TestContext context) {
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams();
        WithdrawalParams withdrawalParams = buildTestWithdrawalParams(accountOpenParams.getId());

        Async async = context.async();
        service.openAccount(accountOpenParams)
                .flatMap(savedAccount -> service.closeAccount(savedAccount.getId()))
                .flatMap(x -> service.withdraw(withdrawalParams))
                .doOnSuccess(account -> context.fail("Expected IllegalAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(IllegalAccountStateException.class, error.getClass());
                    context.assertEquals(withdrawalParams.getSourceAccountId(), ((IllegalAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void withdrawShouldThrowWhenTransactionResultsInNegativeBalance(TestContext context) {
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams();
        DepositParams depositParams = buildTestDepositParams(d -> d
                .destinationAccountId(accountOpenParams.getId())
                .amount(BigDecimal.valueOf(100.00))
        );
        WithdrawalParams withdrawalParams = buildTestWithdrawalParams(w -> w
                .sourceAccountId(accountOpenParams.getId())
                .amount(BigDecimal.valueOf(120.00))
        );

        Async async = context.async();
        service.openAccount(accountOpenParams)
                .flatMap(x -> service.deposit(depositParams))
                .flatMap(x -> service.withdraw(withdrawalParams))
                .doOnSuccess(account -> context.fail("Expected IllegalAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(IllegalAccountStateException.class, error.getClass());
                    context.assertEquals(withdrawalParams.getSourceAccountId(), ((IllegalAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }


    @Test
    public void withdrawShouldThrowWhenSourceAccountIsNotPresent(TestContext context) {
        WithdrawalParams withdrawalParams = buildTestWithdrawalParams(w -> w.sourceAccountId(null));
        expectConstraintViolation(context, service.withdraw(withdrawalParams), "sourceAccountId");
    }

    @Test
    public void transferShouldUpdateAccountBalances(TestContext context) {
        AccountOpenParams sourceAccountOpenParams = buildTestAccountOpenParams();
        AccountOpenParams destinationAccountOpenParams = buildTestAccountOpenParams();
        DepositParams deposit = buildTestDepositParams(d -> d
                .destinationAccountId(sourceAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(100.00))
        );
        TransferParams firstTransfer = buildTestTransferParams(w -> w
                .sourceAccountId(sourceAccountOpenParams.getId())
                .destinationAccountId(destinationAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(20.00))
        );
        TransferParams secondTransfer = buildTestTransferParams(w -> w
                .sourceAccountId(sourceAccountOpenParams.getId())
                .destinationAccountId(destinationAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(0.01))
        );

        AccountsRepository repository = repositoryProvider.get();
        Async async = context.async();
        service.openAccount(sourceAccountOpenParams)
                .flatMap(x -> service.openAccount(destinationAccountOpenParams))
                .flatMap(x -> service.deposit(deposit))
                .flatMap(x -> service.transfer(firstTransfer))
                .flatMap(x -> service.transfer(secondTransfer))
                .doOnSuccess(transaction -> repository.getAccountById(transaction.getSourceAccountId())
                        .doOnSuccess(account -> context.assertEquals("79.9900", account.getBalance().toString())))
                .doOnSuccess(transaction -> repository.getAccountById(transaction.getDestinationAccountId())
                        .doOnSuccess(account -> context.assertEquals("20.0100", account.getBalance().toString())))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void transferShouldSaveTransactionDetails(TestContext context) {
        AccountOpenParams sourceAccountOpenParams = buildTestAccountOpenParams();
        AccountOpenParams destinationAccountOpenParams = buildTestAccountOpenParams();
        DepositParams deposit = buildTestDepositParams(d -> d
                .destinationAccountId(sourceAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(100.00))
        );
        TransferParams transfer = buildTestTransferParams(w -> w
                .sourceAccountId(sourceAccountOpenParams.getId())
                .destinationAccountId(destinationAccountOpenParams.getId())
                .amount(new BigDecimal("20.0000"))
        );

        AccountsRepository repository = repositoryProvider.get();
        Async async = context.async();
        service.openAccount(sourceAccountOpenParams)
                .flatMap(x -> service.openAccount(destinationAccountOpenParams))
                .flatMap(x -> service.deposit(deposit))
                .flatMap(x -> service.transfer(transfer))
                .flatMap(x -> repository.getTransaction(buildQuerySingleCriteria(transfer)))
                .doOnSuccess(transaction -> {
                        context.assertTrue(Matchers.eq((Transfer)transaction).matches(transfer));
                        context.assertFalse(transaction.getInstant().isAfter(Instant.now()));
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void transferShouldThrowWhenSourceAccountDoesNotExist(TestContext context) {
        AccountOpenParams destinationAccountOpenParams = buildTestAccountOpenParams();
        TransferParams transfer = buildTestTransferParams(w -> w.destinationAccountId(destinationAccountOpenParams.getId()));

        Async async = context.async();
        service.openAccount(destinationAccountOpenParams)
                .flatMap(x -> service.transfer(transfer))
                .doOnSuccess(account -> context.fail("Expected AccountNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(AccountNotFoundException.class, error.getClass());
                    context.assertEquals(transfer.getSourceAccountId().toString(), ((AccountNotFoundException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void transferShouldThrowWhenDestinationAccountDoesNotExist(TestContext context) {
        AccountOpenParams sourceAccountOpenParams = buildTestAccountOpenParams();
        DepositParams deposit = buildTestDepositParams(d -> d
                .destinationAccountId(sourceAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(100.00))
        );
        TransferParams transfer = buildTestTransferParams(w -> w
                .sourceAccountId(sourceAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(20.00))
        );

        Async async = context.async();
        service.openAccount(sourceAccountOpenParams)
                .flatMap(x -> service.deposit(deposit))
                .flatMap(x -> service.transfer(transfer))
                .doOnSuccess(account -> context.fail("Expected AccountNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(AccountNotFoundException.class, error.getClass());
                    context.assertEquals(transfer.getDestinationAccountId().toString(), ((AccountNotFoundException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void transferShouldThrowWhenSourceAccountIsClosed(TestContext context) {
        AccountOpenParams sourceAccountOpenParams = buildTestAccountOpenParams();
        AccountOpenParams destinationAccountOpenParams = buildTestAccountOpenParams();
        TransferParams transfer = buildTestTransferParams(w -> w
                .sourceAccountId(sourceAccountOpenParams.getId())
                .destinationAccountId(destinationAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(20.00))
        );

        Async async = context.async();
        service.openAccount(sourceAccountOpenParams)
                .flatMap(x -> service.openAccount(destinationAccountOpenParams))
                .flatMap(x -> service.closeAccount(sourceAccountOpenParams.getId()))
                .flatMap(x -> service.transfer(transfer))
                .doOnSuccess(account -> context.fail("Expected IllegalAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(IllegalAccountStateException.class, error.getClass());
                    context.assertEquals(transfer.getSourceAccountId(), ((IllegalAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void transferShouldThrowWhenDestinationAccountIsClosed(TestContext context) {
        AccountOpenParams sourceAccountOpenParams = buildTestAccountOpenParams();
        AccountOpenParams destinationAccountOpenParams = buildTestAccountOpenParams();
        DepositParams deposit = buildTestDepositParams(d -> d
                .destinationAccountId(sourceAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(100.00))
        );
        TransferParams transfer = buildTestTransferParams(w -> w
                .sourceAccountId(sourceAccountOpenParams.getId())
                .destinationAccountId(destinationAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(20.00))
        );

        Async async = context.async();
        service.openAccount(sourceAccountOpenParams)
                .flatMap(x -> service.deposit(deposit))
                .flatMap(x -> service.openAccount(destinationAccountOpenParams))
                .flatMap(x -> service.closeAccount(destinationAccountOpenParams.getId()))
                .flatMap(x -> service.transfer(transfer))
                .doOnSuccess(account -> context.fail("Expected IllegalAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(IllegalAccountStateException.class, error.getClass());
                    context.assertEquals(transfer.getDestinationAccountId(), ((IllegalAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void transferShouldThrowWhenTransactionResultsInNegativeBalance(TestContext context) {
        AccountOpenParams sourceAccountOpenParams = buildTestAccountOpenParams();
        AccountOpenParams destinationAccountOpenParams = buildTestAccountOpenParams();
        DepositParams deposit = buildTestDepositParams(d -> d
                .destinationAccountId(sourceAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(100.00))
        );
        TransferParams transfer = buildTestTransferParams(w -> w
                .sourceAccountId(sourceAccountOpenParams.getId())
                .destinationAccountId(destinationAccountOpenParams.getId())
                .amount(BigDecimal.valueOf(210.00))
        );

        Async async = context.async();
        service.openAccount(sourceAccountOpenParams)
                .flatMap(x -> service.openAccount(destinationAccountOpenParams))
                .flatMap(x -> service.deposit(deposit))
                .flatMap(x -> service.transfer(transfer))
                .doOnSuccess(account -> context.fail("Expected IllegalAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(IllegalAccountStateException.class, error.getClass());
                    context.assertEquals(transfer.getSourceAccountId(), ((IllegalAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void transferShouldThrowWhenSourceAndDestinationAreTheSameAccount(TestContext context) {
        UUID accountId = UUID.randomUUID();
        TransferParams transferParams = buildTestTransferParams(t -> t
                .sourceAccountId(accountId)
                .destinationAccountId(accountId));

        expectException(context, service.transfer(transferParams), ConstraintViolationException.class);
    }

    @Test
    public void transferShouldThrowWhenTitleIsTooLong(TestContext context) {
        TransferParams transferParams = buildTestTransferParams(t -> t.title(buildLongString()));
        expectConstraintViolation(context, service.transfer(transferParams), "title");
    }

    @Test
    public void getTransactionShouldReturnSingleTransactionByGivenQueryCriteria(TestContext context) {
        UUID accountId = UUID.randomUUID();
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams(a -> a.id(accountId));
        DepositParams deposit = buildTestDepositParams(accountId);

        Async async = context.async();
        service.openAccount(accountOpenParams)
                .flatMap(x -> service.deposit(deposit))
                .flatMap(transaction -> service.getTransaction(buildQuerySingleCriteria(deposit)))
                .doOnSuccess(retrievedTransaction -> context.assertEquals(deposit.getId(), retrievedTransaction.getId()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getTransactionShouldThrowWhenTransactionIdIsNotPresent(TestContext context) {
        SingleTransactionQueryCriteria query = buildQuerySingleCriteria(UUID.randomUUID(), TransactionType.DEPOSIT, null);
        expectConstraintViolation(context, service.getTransaction(query), "transactionId");
    }

    @Test
    public void getTransactionsShouldReturnTransactionsByGivenQuery(TestContext context) {
        UUID accountId = UUID.randomUUID();
        AccountOpenParams accountOpenParams = buildTestAccountOpenParams(a -> a.id(accountId));
        UUID secondAccountId = UUID.randomUUID();
        AccountOpenParams secondAccountOpenParams = buildTestAccountOpenParams(a -> a.id(secondAccountId));

        DepositParams firstDeposit = buildTestDepositParams(d -> d
                .destinationAccountId(accountId)
                .amount(new BigDecimal("10000.0000")));
        DepositParams secondDeposit = buildTestDepositParams(accountId);
        WithdrawalParams withdrawalParams = buildTestWithdrawalParams(accountId);
        TransferParams transferParams = buildTestTransferParams(t -> t
                .sourceAccountId(accountId)
                .destinationAccountId(secondAccountId));

        Async async = context.async();
        service.openAccount(accountOpenParams)
                .flatMap(x -> service.openAccount(secondAccountOpenParams))
                .flatMap(x -> service.deposit(firstDeposit))
                .flatMap(x -> service.deposit(secondDeposit))
                .flatMap(x -> service.withdraw(withdrawalParams))
                .flatMap(x -> service.transfer(transferParams))
                .flatMapObservable(x -> service.getTransactions(buildQueryCriteria(accountId)))
                .toList()
                .toSingle()
                .doOnSuccess(retrievedTransactions -> {
                    context.assertEquals(4, retrievedTransactions.size());
                    context.assertTrue(Matchers.eq((Deposit)retrievedTransactions.get(0)).matches(firstDeposit));
                    context.assertTrue(Matchers.eq((Deposit)retrievedTransactions.get(1)).matches(secondDeposit));
                    context.assertTrue(Matchers.eq((Withdrawal) retrievedTransactions.get(2)).matches(withdrawalParams));
                    context.assertTrue(Matchers.eq((Transfer) retrievedTransactions.get(3)).matches(transferParams));
                })
                .flatMapObservable(x -> service.getTransactions(buildQueryCriteria(accountId, TransactionType.WITHDRAWAL)))
                .toList()
                .toSingle()
                .doOnSuccess(retrievedTransactions -> {
                    context.assertEquals(1, retrievedTransactions.size());
                    context.assertTrue(Matchers.eq((Withdrawal) retrievedTransactions.get(0)).matches(withdrawalParams));
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getTransactionsShouldThrowWhenAccountIdIsNotPresent(TestContext context) {
        TransactionsQueryCriteria query = buildQueryCriteria(null, TransactionType.DEPOSIT);
        expectConstraintViolation(context, service.getTransactions(query).toSingle(), "accountId");
    }

    private void expectConstraintViolation(TestContext context, Single<?> result, String propertyPath) {
        expectException(
                context,
                result,
                ConstraintViolationException.class,
                exception -> exception
                        .getConstraintViolations()
                        .forEach(violation -> context
                                .assertEquals(propertyPath, violation.getPropertyPath().toString())
                        )
        );
    }

    private <T> void expectException(
            TestContext context,
            Single<?> result,
            Class<T> exceptionClass) {
        expectException(context, result, exceptionClass, null);
    }

    @SuppressWarnings("unchecked")
    private <T> void expectException(
            TestContext context,
            Single<?> result,
            Class<T> exceptionClass,
            Consumer<T> exceptionAssertions) {
        Async async = context.async();
        result
                .doOnSuccess(account -> context.fail(String.format("Expected %s, but got success", exceptionClass)))
                .onErrorReturn(error -> {
                    context.assertEquals(exceptionClass, error.getClass());
                    if (exceptionAssertions != null) {
                        exceptionAssertions.accept((T)error);
                    }
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    private String buildLongString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 2000; i++) {
            sb.append("a");
        }
        return sb.toString();
    }
}
