package io.lkulisz.accounts;

import io.lkulisz.accounts.domain.model.*;
import io.lkulisz.accounts.testing.ApiTest;
import io.lkulisz.accounts.testing.TestSubscriber;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.ext.web.client.HttpResponse;
import org.junit.Test;
import rx.Completable;
import rx.Single;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;

import static io.lkulisz.accounts.testing.ModelFactory.*;
import static io.lkulisz.accounts.testing.RepresentationMappers.*;
import static io.lkulisz.accounts.testing.RepresentationVerifiers.verifyRepresentation;
import static java.util.stream.Collectors.toMap;

public class AcceptanceTest extends ApiTest {

    @Test
    public void allowsToOpenNewAccount(TestContext context) {
        String requestBody =
                "{" +
                    "\"data\":" + "{" +
                        "\"id\":\"3cba449a-31b1-4a1c-9845-78b9b7497255\"," +
                        "\"owner_id\":\"09289c53-745e-4de2-8497-c47c3f4c53da\"," +
                        "\"name\":\"New Account\"" +
                    "}" +
                "}";
        String expectedResponse =
                "{" +
                    "\"data\":{" +
                        "\"balance\":\"0.0000\"," +
                        "\"id\":\"3cba449a-31b1-4a1c-9845-78b9b7497255\"," +
                        "\"owner_id\":\"09289c53-745e-4de2-8497-c47c3f4c53da\"," +
                        "\"name\":\"New Account\"," +
                        "\"closed\":false" +
                    "}," +
                    "\"meta\":{" +
                        "\"type\":\"Account\"," +
                        "\"links\":{" +
                            "\"self\":\"/v1/accounts/3cba449a-31b1-4a1c-9845-78b9b7497255\"," +
                            "\"close\":\"/v1/accounts/3cba449a-31b1-4a1c-9845-78b9b7497255/close\"," +
                            "\"deposits\":\"/v1/accounts/3cba449a-31b1-4a1c-9845-78b9b7497255/deposits\"," +
                            "\"withdrawals\":\"/v1/accounts/3cba449a-31b1-4a1c-9845-78b9b7497255/withdrawals\"," +
                            "\"transfers\":\"/v1/accounts/3cba449a-31b1-4a1c-9845-78b9b7497255/transfers\"," +
                            "\"transactions\":\"/v1/accounts/3cba449a-31b1-4a1c-9845-78b9b7497255/transactions\"" +
                        "}" +
                    "}" +
                "}";

        Async async = context.async();
        post("/v1/accounts", new JsonObject(requestBody))
                .map(response -> getLink(response.bodyAsJsonObject(), "self"))
                .flatMap(this::get)
                .doOnSuccess(response -> context.assertEquals(new JsonObject(expectedResponse), response.bodyAsJsonObject()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void forbidsOpeningAccountWithDuplicateId(TestContext context) {
        String requestBody =
                "{" +
                    "\"data\":" + "{" +
                        "\"id\":\"3cba449a-31b1-4a1c-9845-78b9b7497254\"," +
                        "\"owner_id\":\"09289c53-745e-4de2-8497-c47c3f4c53da\"," +
                        "\"name\":\"New Account\"" +
                    "}" +
                "}";
        String expectedResponse =
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"duplicate\",\n" +
                "    \"details\": \"Account already exists (ID: 3cba449a-31b1-4a1c-9845-78b9b7497254)\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Account\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        Async async = context.async();
        post("/v1/accounts", new JsonObject(requestBody))
                .flatMap(firstResponse -> post("/v1/accounts", new JsonObject(requestBody))
                        .doOnSuccess(secondResponse -> {
                            context.assertEquals(201, firstResponse.statusCode());
                            context.assertEquals(409, secondResponse.statusCode());
                            context.assertEquals(new JsonObject(expectedResponse), secondResponse.bodyAsJsonObject());
                        })
                )
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void allowsToCloseAccount(TestContext context) {
        Account account = buildTestAccount();

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .map(response -> getLink(response.bodyAsJsonObject(), "close"))
                .flatMap(this::post)
                .map(response -> {
                    context.assertEquals(201, response.statusCode());
                    JsonObject body = response.bodyAsJsonObject();
                    return getLink(body, "account");
                })
                .flatMap(this::get)
                .doOnSuccess(response -> {
                    JsonObject body = response.bodyAsJsonObject();
                    context.assertEquals(true, body.getJsonObject("data").getBoolean("closed"));
                    context.assertNull(tryGetLink(body, "close"));
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void allowsToDepositMoney(TestContext context) {
        Account account = buildTestAccount(a -> a.id(UUID.fromString("acc1c9b1-ba65-44cf-96c4-00d30b20e651")));
        String requestBody =
                "{\n" +
                "  \"data\": {\n" +
                "    \"id\": \"40fcb95d-7c33-4897-ae61-669d28ef9ee8\",\n" +
                "    \"destination_account_id\": \"acc1c9b1-ba65-44cf-96c4-00d30b20e651\",\n" +
                "    \"amount\": \"120.50\",\n" +
                "    \"title\": \"Some extra savings\"\n" +
                "  }\n" +
                "}";
        String expectedResponse =
                "{\n" +
                "  \"data\": {\n" +
                "    \"id\": \"40fcb95d-7c33-4897-ae61-669d28ef9ee8\",\n" +
                "    \"amount\": \"120.50\",\n" +
                "    \"title\": \"Some extra savings\",\n" +
                //"  \"instant\": \"2018-06-29T06:02:50.108Z\",\n" +
                "    \"destination_account_id\": \"acc1c9b1-ba65-44cf-96c4-00d30b20e651\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Deposit\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/acc1c9b1-ba65-44cf-96c4-00d30b20e651/deposits/40fcb95d-7c33-4897-ae61-669d28ef9ee8\",\n" +
                "      \"account\": \"/v1/accounts/acc1c9b1-ba65-44cf-96c4-00d30b20e651\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .map(response -> getLink(response.bodyAsJsonObject(), "deposits"))
                .flatMap(depositLink -> post(depositLink, new JsonObject(requestBody)))
                .map(response -> {
                        verifyTransactionResponse(context, expectedResponse, response);
                        return getLink(response.bodyAsJsonObject(), "account");
                })
                .flatMap(this::get)
                .doOnSuccess(response -> {
                    JsonObject data = response.bodyAsJsonObject().getJsonObject("data");
                    context.assertEquals("120.5000", data.getString("balance"));
                    context.assertNull(tryGetLink(response.bodyAsJsonObject(), "close"));
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void allowsToGetDepositById(TestContext context) {
        Account account = buildTestAccount();
        Deposit deposit = buildTestDeposit(account.getId());

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .map(response -> getLink(response.bodyAsJsonObject(), "deposits"))
                .flatMap(depositLink -> post(depositLink, depositParams(deposit)))
                .flatMap(depositResponse -> get(getLink(depositResponse.bodyAsJsonObject(), "self")))
                .doOnSuccess(response -> {
                    context.assertEquals(200, response.statusCode());
                    verifyRepresentation(context, deposit, response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void forbidsToDepositMoneyToClosedAccount(TestContext context) {
        UUID accountId = UUID.fromString("5a093f66-86a8-4903-aa18-f0aa4f594af0");
        Account account = buildTestAccount(a -> a.id(accountId));
        Deposit deposit = buildTestDeposit(accountId);

        String expectedResponse =
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"illegal_state\",\n" +
                "    \"details\": \"Account is closed (ID: 5a093f66-86a8-4903-aa18-f0aa4f594af0)\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Deposit\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/5a093f66-86a8-4903-aa18-f0aa4f594af0/deposits\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "close")))
                .flatMap(depositLink -> post("/v1/accounts/5a093f66-86a8-4903-aa18-f0aa4f594af0/deposits", depositParams(deposit)))
                .map(response -> {
                    context.assertEquals(422, response.statusCode());
                    context.assertEquals(new JsonObject(expectedResponse), response.bodyAsJsonObject());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void allowsToWithdrawMoneyFromAccountWithPositiveBalance(TestContext context) {
        UUID accountId = UUID.fromString("ccc1c9b1-ba65-44cf-96c4-00d30b20e651");
        Account account = buildTestAccount(a -> a.id(accountId));
        Deposit deposit = buildTestDeposit(d -> d.destinationAccount(account).amount(BigDecimal.valueOf(1000.00)));

        String requestBody =
                "{\n" +
                        "  \"data\": {\n" +
                        "    \"id\": \"40fcb95d-7c33-4897-ae61-569d28ef9ee9\",\n" +
                        "    \"source_account_id\": \"ccc1c9b1-ba65-44cf-96c4-00d30b20e651\",\n" +
                        "    \"amount\": \"123.45\",\n" +
                        "    \"title\": \"Party hard\"\n" +
                        "  }\n" +
                        "}";
        String expectedResponse =
                "{\n" +
                        "  \"data\": {\n" +
                        "    \"id\": \"40fcb95d-7c33-4897-ae61-569d28ef9ee9\",\n" +
                        "    \"amount\": \"123.45\",\n" +
                        "    \"title\": \"Party hard\",\n" +
                        //"  \"instant\": \"2018-06-29T06:02:50.108Z\",\n" +
                        "    \"source_account_id\": \"ccc1c9b1-ba65-44cf-96c4-00d30b20e651\"\n" +
                        "  },\n" +
                        "  \"meta\": {\n" +
                        "    \"type\": \"Withdrawal\",\n" +
                        "    \"links\": {\n" +
                        "      \"self\": \"/v1/accounts/ccc1c9b1-ba65-44cf-96c4-00d30b20e651/withdrawals/40fcb95d-7c33-4897-ae61-569d28ef9ee9\",\n" +
                        "      \"account\": \"/v1/accounts/ccc1c9b1-ba65-44cf-96c4-00d30b20e651\"\n" +
                        "    }\n" +
                        "  }\n" +
                        "}";

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .map(response -> getLink(response.bodyAsJsonObject(), "deposits"))
                .flatMap(depositLink -> post(depositLink, depositParams(deposit)))
                .map(response -> getLink(response.bodyAsJsonObject(), "account"))
                .flatMap(this::get)
                .map(response -> getLink(response.bodyAsJsonObject(), "withdrawals"))
                .flatMap(withdrawalsLink -> post(withdrawalsLink, new JsonObject(requestBody)))
                .map(response -> {
                    verifyTransactionResponse(context, expectedResponse, response);
                    return getLink(response.bodyAsJsonObject(), "account");
                })
                .flatMap(this::get)
                .doOnSuccess(response -> {
                    JsonObject data = response.bodyAsJsonObject().getJsonObject("data");
                    context.assertEquals("876.5500", data.getString("balance"));
                    context.assertNull(tryGetLink(response.bodyAsJsonObject(), "close"));
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void allowsToGetWithdrawalById(TestContext context) {
        Account account = buildTestAccount();
        Deposit deposit = buildTestDeposit(d -> d.destinationAccount(account).amount(BigDecimal.valueOf(1000.00)));
        Withdrawal withdrawal = buildTestWithdrawal(w -> w.sourceAccount(account).amount(BigDecimal.valueOf(500.00)));

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "deposits"), depositParams(deposit)))
                .flatMap(response -> get(getLink(response.bodyAsJsonObject(), "account")))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "withdrawals"), withdrawalParams(withdrawal)))
                .flatMap(response -> get(getLink(response.bodyAsJsonObject(), "self")))
                .doOnSuccess(response -> {
                    context.assertEquals(200, response.statusCode());
                    verifyRepresentation(context, withdrawal, response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void forbidsToWithdrawMoreMoneyThanItIsCurrentlyStoredOnAccount(TestContext context) {
        Account account = buildTestAccount(a -> a.id(UUID.fromString("5a093f66-86a8-4903-aa18-f0aa4f594af2")));
        Deposit deposit = buildTestDeposit(d -> d.destinationAccount(account).amount(BigDecimal.valueOf(100.00)));
        Withdrawal withdrawal = buildTestWithdrawal(w -> w.sourceAccount(account).amount(BigDecimal.valueOf(100.0001)));

        String expectedResponse =
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"illegal_state\",\n" +
                "    \"details\": \"Account has too low balance (ID: 5a093f66-86a8-4903-aa18-f0aa4f594af2)\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Withdrawal\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/5a093f66-86a8-4903-aa18-f0aa4f594af2/withdrawals\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "deposits"), depositParams(deposit)))
                .flatMap(response -> get(getLink(response.bodyAsJsonObject(), "account")))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "withdrawals"), withdrawalParams(withdrawal)))
                .doOnSuccess(response -> {
                    context.assertEquals(422, response.statusCode());
                    context.assertEquals(new JsonObject(expectedResponse), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void forbidsToWithdrawMoneyFromClosedAccount(TestContext context) {
        Account account = buildTestAccount(a -> a.id(UUID.fromString("5a093f66-86a8-4903-aa18-f0aa4f594af3")));
        Withdrawal withdrawal = buildTestWithdrawal(account.getId());

        String expectedResponse =
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"illegal_state\",\n" +
                "    \"details\": \"Account is closed (ID: 5a093f66-86a8-4903-aa18-f0aa4f594af3)\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Withdrawal\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/5a093f66-86a8-4903-aa18-f0aa4f594af3/withdrawals\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "close")))
                .flatMap(response -> get(getLink(response.bodyAsJsonObject(), "account")))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "withdrawals"), withdrawalParams(withdrawal)))
                .doOnSuccess(response -> {
                    context.assertEquals(422, response.statusCode());
                    context.assertEquals(new JsonObject(expectedResponse), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void allowsToTransferMoneyBetweenAccounts(TestContext context) {
        Account sourceAccount = buildTestAccount(a -> a.id(UUID.fromString("1111c9b1-ba65-44cf-96c4-00d30b20e111")));
        Account destinationAccount = buildTestAccount(a -> a.id(UUID.fromString("2221c9b1-ba65-44cf-96c4-00d30b20e222")));
        Deposit deposit = buildTestDeposit(d -> d
                .destinationAccount(destinationAccount)
                .amount(BigDecimal.valueOf(1000.00)));

        String requestBody =
                "{\n" +
                "  \"data\": {\n" +
                "    \"id\": \"eeecb95d-7c33-4897-ae61-569d28ef9eee\",\n" +
                "    \"source_account_id\": \"1111c9b1-ba65-44cf-96c4-00d30b20e111\",\n" +
                "    \"destination_account_id\": \"2221c9b1-ba65-44cf-96c4-00d30b20e222\",\n" +
                "    \"amount\": \"200.00\",\n" +
                "    \"title\": \"Super transfer\"\n" +
                "  }\n" +
                "}";
        String expectedResponse =
                "{\n" +
                "  \"data\": {\n" +
                "    \"id\": \"eeecb95d-7c33-4897-ae61-569d28ef9eee\",\n" +
                "    \"amount\": \"200.00\",\n" +
                "    \"title\": \"Super transfer\",\n" +
                //"  \"instant\": \"2018-06-29T06:02:50.108Z\",\n" +
                "    \"source_account_id\": \"1111c9b1-ba65-44cf-96c4-00d30b20e111\",\n" +
                "    \"destination_account_id\": \"2221c9b1-ba65-44cf-96c4-00d30b20e222\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Transfer\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/1111c9b1-ba65-44cf-96c4-00d30b20e111/transfers/eeecb95d-7c33-4897-ae61-569d28ef9eee\",\n" +
                "      \"source_account\": \"/v1/accounts/1111c9b1-ba65-44cf-96c4-00d30b20e111\",\n" +
                "      \"destination_account\": \"/v1/accounts/2221c9b1-ba65-44cf-96c4-00d30b20e222\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        Async async = context.async();
        postAccount(accountOpenParams(destinationAccount))
                .flatMap(x -> postAccount(accountOpenParams(sourceAccount)))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "deposits"), depositParams(deposit)))
                .flatMap(response -> get(getLink(response.bodyAsJsonObject(), "account")))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "transfers"), new JsonObject(requestBody)))
                .doOnSuccess(response -> verifyTransactionResponse(context, expectedResponse, response))
                .flatMap(x -> getAccount(sourceAccount.getId().toString()))
                .doOnSuccess(response -> context.assertEquals(
                        "800.0000",
                        response.bodyAsJsonObject().getJsonObject("data").getString("balance"))
                )
                .flatMap(x -> getAccount(destinationAccount.getId().toString()))
                .doOnSuccess(response -> context.assertEquals(
                        "200.0000",
                        response.bodyAsJsonObject().getJsonObject("data").getString("balance"))
                )
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void forbidsTransferringMoreMoneyThanItIsStoredOnSourceAccount(TestContext context) {
        Account sourceAccount = buildTestAccount(a -> a.id(UUID.fromString("1111a9b1-ba65-44cf-96c4-00d30b20e111")));
        Account destinationAccount = buildTestAccount(a -> a.id(UUID.fromString("2221a9b1-ba65-44cf-96c4-00d30b20e222")));
        Deposit deposit = buildTestDeposit(d -> d
                .destinationAccount(destinationAccount)
                .amount(BigDecimal.valueOf(10.00)));
        Transfer transfer = buildTestTransfer(t -> t
                .sourceAccount(sourceAccount)
                .destinationAccount(destinationAccount)
                .amount(BigDecimal.valueOf(15.00)));

        String expectedResponse =
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"illegal_state\",\n" +
                "    \"details\": \"Account has too low balance (ID: 1111a9b1-ba65-44cf-96c4-00d30b20e111)\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Transfer\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/1111a9b1-ba65-44cf-96c4-00d30b20e111/transfers\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        Async async = context.async();
        postAccount(accountOpenParams(destinationAccount))
                .flatMap(x -> postAccount(accountOpenParams(sourceAccount)))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "deposits"), depositParams(deposit)))
                .flatMap(response -> get(getLink(response.bodyAsJsonObject(), "account")))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "transfers"), transferParams(transfer)))
                .doOnSuccess(response -> {
                    context.assertEquals(422, response.statusCode());
                    context.assertEquals(new JsonObject(expectedResponse), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void forbidsTransferringMoneyToClosedAccount(TestContext context) {
        Account sourceAccount = buildTestAccount(a -> a.id(UUID.fromString("1111b9b1-ba65-44cf-96c4-00d30b20e111")));
        Account destinationAccount = buildTestAccount(a -> a.id(UUID.fromString("2221b9b1-ba65-44cf-96c4-00d30b20e222")));
        Deposit deposit = buildTestDeposit(d -> d
                .destinationAccount(destinationAccount)
                .amount(BigDecimal.valueOf(10.00)));
        Transfer transfer = buildTestTransfer(t -> t
                .sourceAccount(sourceAccount)
                .destinationAccount(destinationAccount)
                .amount(BigDecimal.valueOf(5.00)));

        String expectedResponse =
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"illegal_state\",\n" +
                "    \"details\": \"Account is closed (ID: 2221b9b1-ba65-44cf-96c4-00d30b20e222)\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Transfer\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/1111b9b1-ba65-44cf-96c4-00d30b20e111/transfers\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        Async async = context.async();
        postAccount(accountOpenParams(sourceAccount))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "deposits"), depositParams(deposit)))
                .flatMap(x -> postAccount(accountOpenParams(destinationAccount)))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "close")))
                .flatMap(response -> postTransfer(sourceAccount.getId(), transferParams(transfer)))
                .doOnSuccess(response -> {
                    context.assertEquals(422, response.statusCode());
                    context.assertEquals(new JsonObject(expectedResponse), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void allowsToGetTransferById(TestContext context) {
        Account sourceAccount = buildTestAccount();
        Account destinationAccount = buildTestAccount();
        Deposit deposit = buildTestDeposit(d -> d
                .destinationAccount(destinationAccount)
                .amount(BigDecimal.valueOf(10.00)));
        Transfer transfer = buildTestTransfer(t -> t
                .sourceAccount(sourceAccount)
                .destinationAccount(destinationAccount)
                .amount(BigDecimal.valueOf(5.00)));

        Async async = context.async();
        postAccount(accountOpenParams(destinationAccount))
                .flatMap(x -> postAccount(accountOpenParams(sourceAccount)))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "deposits"), depositParams(deposit)))
                .flatMap(response -> get(getLink(response.bodyAsJsonObject(), "account")))
                .flatMap(response -> post(getLink(response.bodyAsJsonObject(), "transfers"), transferParams(transfer)))
                .flatMap(response -> get(getLink(response.bodyAsJsonObject(), "self")))
                .doOnSuccess(response -> {
                    context.assertEquals(200, response.statusCode());
                    verifyRepresentation(context, transfer, response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void allowsToFetchAllDeposits(TestContext context) {
        testFetchingTransactionsOfType(context, "deposits", 5, 6);
    }

    @Test
    public void allowsToFetchAllWithdrawals(TestContext context) {
        testFetchingTransactionsOfType(context, "withdrawals", 5, 5);
    }

    @Test
    public void allowsToFetchAllTransfers(TestContext context) {
        testFetchingTransactionsOfType(context, "transfers", 5, 5);
    }

    @Test
    public void allowsToFetchAllTransactions(TestContext context) {
        testFetchingTransactionsOfType(context, "transactions", 5, 16);
    }

    private void testFetchingTransactionsOfType(
            TestContext context,
            String transactionsLink,
            int transactionsOfEachTypeToCreate,
            int expectedTransactionsCount) {
        Account account = buildTestAccount();

        Async async = context.async();
        postAccount(accountOpenParams(account))
                .flatMap(x -> createManyTransactions(transactionsOfEachTypeToCreate, account))
                .flatMap(transactions -> getAccount(account.getId().toString())
                        .flatMap(response -> get(getLink(response.bodyAsJsonObject(), transactionsLink)))
                        .doOnSuccess(response -> {
                            context.assertEquals(200, response.statusCode());
                            JsonObject body = response.bodyAsJsonObject();

                            JsonArray items = body.getJsonArray("items");
                            int size = body.getJsonObject("meta").getInteger("size");
                            context.assertEquals(size, items.size());
                            context.assertEquals(expectedTransactionsCount, items.size());

                            context.assertEquals("Collection", body.getJsonObject("meta").getString("type"));

                            for (int i = 0; i < items.size(); i++) {
                                JsonObject item = items.getJsonObject(i);
                                UUID id = UUID.fromString(item.getJsonObject("data").getString("id"));
                                verifyRepresentation(context, transactions.get(id), items.getJsonObject(i));
                            }
                        })
                )
                .subscribe(new TestSubscriber<>(context, async));
    }

    private String getLink(JsonObject response, String link) {
        String result = tryGetLink(response, link);
        if (result == null) {
            throw new IllegalStateException(String.format("No link to %s", link));
        }
        return result;
    }


    private String tryGetLink(JsonObject response, String link) {
        return response.getJsonObject("meta").getJsonObject("links").getString(link);
    }

    private void verifyTransactionResponse(TestContext context, String expectedResponse, HttpResponse<Buffer> response) {
        context.assertEquals(201, response.statusCode());

        JsonObject responseJson = response.bodyAsJsonObject();
        context.assertFalse(responseJson.getJsonObject("data").getInstant("instant").isAfter(Instant.now()));

        responseJson.getJsonObject("data").remove("instant");
        JsonObject expectedResponseJson = new JsonObject(expectedResponse);
        expectedResponseJson.getJsonObject("data").remove("instant");
        context.assertEquals(expectedResponseJson, responseJson);
    }

    private Single<Map<UUID, Transaction>> createManyTransactions(int numberOfTransactionsOfEachType, Account account) {
        List<Transaction> transactions = buildTestTransactions(numberOfTransactionsOfEachType, account);
        return getAccount(account.getId().toString())
                .flatMapCompletable(response -> {
                    JsonObject links = response.bodyAsJsonObject().getJsonObject("meta").getJsonObject("links");

                    List<Completable> requests = new ArrayList<>();
                    for (Transaction transaction : transactions) {
                        requests.add(post(transaction, links));
                    }

                    return Completable.concat(requests);
                })
                .andThen(Single.just(transactions
                        .stream()
                        .collect(toMap(Transaction::getId, Function.identity()))));
    }

    private List<Transaction> buildTestTransactions(int numberOfTransactionsOfEachType, Account account) {
        List<Transaction> transactions = new ArrayList<>();
        for (int i = 0; i < numberOfTransactionsOfEachType; i++) {
            final BigDecimal depositAmount  = BigDecimal.valueOf((i + 1) * 2.8);
            final BigDecimal withdrawalAmount = BigDecimal.valueOf((i + 1) * 1.5);
            final BigDecimal transferAmount = BigDecimal.valueOf((i + 1) * 0.8);

            transactions.add(buildTestDeposit(d -> d
                    .amount(depositAmount)
                    .destinationAccount(account)));

            transactions.add(buildTestWithdrawal(w -> w
                    .amount(withdrawalAmount)
                    .sourceAccount(account)));

            transactions.add(buildTestTransfer(t -> t
                    .amount(transferAmount)
                    .sourceAccount(account)
                    .destinationAccount(buildTestAccount())));
        }

        Collections.shuffle(transactions);

        // Starting deposit created so that when account is open, there is some money which can be used
        // in other transactions.
        Deposit startingDeposit = buildTestDeposit(d -> d
                .destinationAccount(account)
                .amount(BigDecimal.valueOf(1000000000.0)));
        transactions.add(0, startingDeposit);

        return transactions;
    }

    private Completable post(Transaction transaction, JsonObject links) {
        if (transaction instanceof Deposit) {
            String link = links.getString("deposits");
            return post(link, depositParams((Deposit) transaction)).toCompletable();
        } else if (transaction instanceof Withdrawal) {
            String link = links.getString("withdrawals");
            return post(link, withdrawalParams((Withdrawal) transaction)).toCompletable();
        } else if (transaction instanceof Transfer) {
            String link = links.getString("transfers");
            Transfer transfer = (Transfer) transaction;
            Account destinationAccount = transfer.getDestinationAccount()
                    .orElseThrow(() -> new IllegalStateException("Transfer must have a destination account"));
            return postAccount(accountOpenParams(destinationAccount)).toCompletable()
                    .andThen(post(link, transferParams(transfer)).toCompletable());

        }

        throw new IllegalArgumentException("Unknown type of transaction");
    }
}
