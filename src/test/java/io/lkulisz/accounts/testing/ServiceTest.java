package io.lkulisz.accounts.testing;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.lkulisz.accounts.persistence.DbInitializer;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.ext.jdbc.JDBCClient;
import lombok.AccessLevel;
import lombok.Getter;
import org.junit.Before;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public abstract class ServiceTest {
    @Getter(AccessLevel.PROTECTED)
    private Vertx vertx;
    @Getter(AccessLevel.PROTECTED)
    private Injector injector;

    @Before
    public void prepare(TestContext context) {
        TestModule testModule = new TestModule(Vertx.vertx(), binder -> {});
        injector = Guice.createInjector(testModule);
        injector.injectMembers(this);

        Async async = context.async();
        injector.getInstance(DbInitializer.class)
                .createTables()
                .flatMap(x -> injector.getInstance(JDBCClient.class).rxGetConnection())
                .flatMap(connection -> connection.rxExecute(
                        "TRUNCATE TABLE transactions; " +
                        "TRUNCATE TABLE accounts; "))
                .subscribe(new TestSubscriber<>(context, async));
    }
}
