package io.lkulisz.accounts.testing;

import io.lkulisz.accounts.domain.model.Deposit;
import io.lkulisz.accounts.domain.model.Transaction;
import io.lkulisz.accounts.domain.model.Transfer;
import io.lkulisz.accounts.domain.model.Withdrawal;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.TestContext;

import java.math.RoundingMode;
import java.time.Instant;

import static io.lkulisz.accounts.testing.Constants.DB_MONEY_SCALE;

public class RepresentationVerifiers {
    public static void verifyRepresentation(TestContext context, Deposit deposit, JsonObject body) {
        verifyCommonTransactionRepresentation(context, deposit, body);
        JsonObject data = body.getJsonObject("data");
        context.assertEquals(deposit.getDestinationAccountId().toString(), data.getString("destination_account_id"));

        JsonObject meta = body.getJsonObject("meta");
        context.assertEquals("Deposit", meta.getString("type"));

        JsonObject links = meta.getJsonObject("links");
        String account = String.format("/v1/accounts/%s", deposit.getDestinationAccountId());
        context.assertEquals(account, links.getString("account"));
        String self = account + String.format("/deposits/%s", deposit.getId());
        context.assertEquals(self, links.getString("self"));
    }

    public static void verifyRepresentation(TestContext context, Withdrawal withdrawal, JsonObject body) {
        verifyCommonTransactionRepresentation(context, withdrawal, body);

        JsonObject data = body.getJsonObject("data");
        context.assertEquals(withdrawal.getSourceAccountId().toString(), data.getString("source_account_id"));

        JsonObject meta = body.getJsonObject("meta");
        context.assertEquals("Withdrawal", meta.getString("type"));

        JsonObject links = meta.getJsonObject("links");
        String account = String.format("/v1/accounts/%s", withdrawal.getSourceAccountId());
        context.assertEquals(account, links.getString("account"));
        String self = account + String.format("/withdrawals/%s", withdrawal.getId());
        context.assertEquals(self, links.getString("self"));
    }

    public static void verifyRepresentation(TestContext context, Transfer transfer, JsonObject body) {
        verifyCommonTransactionRepresentation(context, transfer, body);

        JsonObject data = body.getJsonObject("data");
        context.assertEquals(transfer.getSourceAccountId().toString(), data.getString("source_account_id"));
        context.assertEquals(transfer.getDestinationAccountId().toString(), data.getString("destination_account_id"));

        JsonObject meta = body.getJsonObject("meta");
        context.assertEquals("Transfer", meta.getString("type"));

        String sourceAccount = String.format("/v1/accounts/%s", transfer.getSourceAccountId());
        String destinationAccount = String.format("/v1/accounts/%s", transfer.getDestinationAccountId());

        JsonObject links = meta.getJsonObject("links");
        context.assertEquals(sourceAccount, links.getString("source_account"));
        context.assertEquals(destinationAccount, links.getString("destination_account"));

        String self = sourceAccount + String.format("/transfers/%s", transfer.getId());
        context.assertEquals(self, links.getString("self"));
    }

    public static void verifyRepresentation(TestContext context, Transaction transaction, JsonObject body) {
        if (transaction instanceof Deposit) {
            verifyRepresentation(context, (Deposit) transaction, body);
        } else if (transaction instanceof Withdrawal) {
            verifyRepresentation(context, (Withdrawal) transaction, body);
        } else if (transaction instanceof Transfer) {
            verifyRepresentation(context, (Transfer) transaction, body);
        } else {
            throw new IllegalArgumentException("Unknown type of transaction");
        }
    }

    private static void verifyCommonTransactionRepresentation(TestContext context, Transaction transaction, JsonObject body) {
        JsonObject data = body.getJsonObject("data");
        context.assertEquals(transaction.getId().toString(), data.getString("id"));
        context.assertEquals(
                transaction.getAmount().setScale(DB_MONEY_SCALE, RoundingMode.HALF_UP).toString(),
                data.getString("amount"));
        context.assertEquals(transaction.getTitle(), data.getString("title"));
        context.assertFalse(transaction.getInstant().isAfter(Instant.now()));
    }
}
