package io.lkulisz.accounts.testing;

import com.google.inject.Binder;
import io.lkulisz.accounts.Verticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.rxjava.core.RxHelper;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.ext.web.client.HttpResponse;
import io.vertx.rxjava.ext.web.client.WebClient;
import lombok.AccessLevel;
import lombok.Getter;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import rx.Single;

import static io.lkulisz.accounts.http.util.HttpConstants.APPLICATION_JSON;
import static io.vertx.core.http.HttpHeaders.ACCEPT;

@RunWith(VertxUnitRunner.class)
public abstract class ApiTest {
    private static final String V1_ACCOUNTS = "/v1/accounts/";

    @Getter(AccessLevel.PROTECTED)
    private Vertx vertx;
    @Getter(AccessLevel.PROTECTED)
    private WebClient webClient;

    @Before
    public void prepare(TestContext context) {
        vertx = Vertx.vertx();
        Async async = context.async();

        RxHelper.deployVerticle(vertx, new Verticle(v -> new TestModule(v, this::configureBindings)))
                .subscribe(new TestSubscriber<>(context, async));
        webClient = WebClient.create(vertx, new WebClientOptions()
                .setDefaultHost("localhost")
                .setDefaultPort(8080));
    }

    @After
    public void finish(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    protected void configureBindings(Binder binder) {
    }

    protected Single<HttpResponse<Buffer>> postAccount(JsonObject requestBody) {
        return getWebClient()
                .post(V1_ACCOUNTS)
                .putHeader(ACCEPT.toString(), APPLICATION_JSON)
                .rxSendJsonObject(requestBody);
    }

    protected Single<HttpResponse<Buffer>> get(String link) {
        return getWebClient()
                .get(link)
                .putHeader(ACCEPT.toString(), APPLICATION_JSON)
                .rxSend();
    }

    protected Single<HttpResponse<Buffer>> getAccount(String id) {
        return get(V1_ACCOUNTS + id);
    }

    protected Single<HttpResponse<Buffer>> post(String link) {
        return getWebClient()
                .post(link)
                .putHeader(ACCEPT.toString(), APPLICATION_JSON)
                .rxSend();
    }

    protected Single<HttpResponse<Buffer>> post(String link, JsonObject body) {
        return getWebClient()
                .post(link)
                .putHeader(ACCEPT.toString(), APPLICATION_JSON)
                .rxSendJsonObject(body);
    }

    protected Single<HttpResponse<Buffer>> postAccountClose(String id) {
        return post(V1_ACCOUNTS + id + "/close");
    }

    protected Single<HttpResponse<Buffer>> postDeposit(Object accountId, JsonObject body) {
        return post(V1_ACCOUNTS + accountId.toString() + "/deposits", body);
    }

    protected Single<HttpResponse<Buffer>> postWithdrawal(Object accountId, JsonObject body) {
        return post(V1_ACCOUNTS + accountId.toString() + "/withdrawals", body);
    }

    protected Single<HttpResponse<Buffer>> postTransfer(Object accountId, JsonObject body) {
        return post(V1_ACCOUNTS + accountId.toString() + "/transfers", body);
    }

    protected Single<HttpResponse<Buffer>> getDeposit(Object accountId, Object transactionId) {
        return get(V1_ACCOUNTS + accountId.toString() + "/deposits/" + transactionId);
    }

    protected Single<HttpResponse<Buffer>> getWithdrawal(Object accountId, Object transactionId) {
        return get(V1_ACCOUNTS + accountId.toString() + "/withdrawals/" + transactionId);
    }

    protected Single<HttpResponse<Buffer>> getTransactions(Object accountId) {
        return get(V1_ACCOUNTS + accountId.toString() + "/transactions");
    }
}
