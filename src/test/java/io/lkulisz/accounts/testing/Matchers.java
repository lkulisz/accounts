package io.lkulisz.accounts.testing;

import io.lkulisz.accounts.domain.model.Account;
import io.lkulisz.accounts.domain.model.Deposit;
import io.lkulisz.accounts.domain.model.Transfer;
import io.lkulisz.accounts.domain.model.Withdrawal;
import io.lkulisz.accounts.domain.params.*;
import org.mockito.ArgumentMatcher;

public class Matchers {

    public static ArgumentMatcher<AccountOpenParams> eq(AccountOpenParams expected) {
        return actual ->
                actual.getId().equals(expected.getId()) &&
                actual.getName().equals(expected.getName()) &&
                actual.getOwnerId().equals(expected.getOwnerId());
    }

    public static ArgumentMatcher<Account> eq(Account expected) {
        return actual ->
                actual.getId().equals(expected.getId()) &&
                actual.getName().equals(expected.getName()) &&
                actual.getOwnerId().equals(expected.getOwnerId()) &&
                actual.getBalance().equals(expected.getBalance()) &&
                (actual.isClosed() == (expected.isClosed()));
    }

    public static ArgumentMatcher<DepositParams> eq(DepositParams expected) {
        return actual ->
                actual.getId().equals(expected.getId()) &&
                actual.getDestinationAccountId().equals(expected.getDestinationAccountId()) &&
                actual.getAmount().equals(expected.getAmount()) &&
                actual.getTitle().equals(expected.getTitle());
    }

    public static ArgumentMatcher<DepositParams> eq(Deposit expected) {
        return actual ->
                actual.getId().equals(expected.getId()) &&
                actual.getDestinationAccountId().equals(expected.getDestinationAccountId()) &&
                actual.getAmount().equals(expected.getAmount()) &&
                actual.getTitle().equals(expected.getTitle());
    }

    public static ArgumentMatcher<WithdrawalParams> eq(WithdrawalParams expected) {
        return actual ->
                actual.getId().equals(expected.getId()) &&
                actual.getSourceAccountId().equals(expected.getSourceAccountId()) &&
                actual.getAmount().equals(expected.getAmount()) &&
                actual.getTitle().equals(expected.getTitle());
    }

    public static ArgumentMatcher<WithdrawalParams> eq(Withdrawal expected) {
        return actual ->
                actual.getId().equals(expected.getId()) &&
                        actual.getSourceAccountId().equals(expected.getSourceAccountId()) &&
                        actual.getAmount().equals(expected.getAmount()) &&
                        actual.getTitle().equals(expected.getTitle());
    }

    public static ArgumentMatcher<TransferParams> eq(TransferParams expected) {
        return actual ->
                actual.getId().equals(expected.getId()) &&
                actual.getDestinationAccountId().equals(expected.getDestinationAccountId()) &&
                actual.getSourceAccountId().equals(expected.getSourceAccountId()) &&
                actual.getAmount().equals(expected.getAmount()) &&
                actual.getTitle().equals(expected.getTitle());
    }

    public static ArgumentMatcher<TransferParams> eq(Transfer expected) {
        return actual ->
                actual.getId().equals(expected.getId()) &&
                actual.getDestinationAccountId().equals(expected.getDestinationAccountId()) &&
                actual.getSourceAccountId().equals(expected.getSourceAccountId()) &&
                actual.getAmount().equals(expected.getAmount()) &&
                actual.getTitle().equals(expected.getTitle());
    }

    public static ArgumentMatcher<SingleTransactionQueryCriteria> eq(SingleTransactionQueryCriteria expected) {
        return actual ->
                actual.getAccountId().equals(expected.getAccountId()) &&
                actual.getTransactionType().equals(expected.getTransactionType()) &&
                actual.getTransactionId().equals(expected.getTransactionId());
    }
}
