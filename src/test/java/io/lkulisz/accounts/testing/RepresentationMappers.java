package io.lkulisz.accounts.testing;

import io.lkulisz.accounts.domain.model.Account;
import io.lkulisz.accounts.domain.model.Deposit;
import io.lkulisz.accounts.domain.model.Transfer;
import io.lkulisz.accounts.domain.model.Withdrawal;
import io.lkulisz.accounts.domain.params.AccountOpenParams;
import io.lkulisz.accounts.domain.params.DepositParams;
import io.lkulisz.accounts.domain.params.TransferParams;
import io.lkulisz.accounts.domain.params.WithdrawalParams;
import io.vertx.core.json.JsonObject;

import java.util.UUID;

public class RepresentationMappers {
    public static JsonObject accountOpenParams(Account account) {
        return new JsonObject()
                .put("data", new JsonObject()
                        .put("id", account.getId().toString())
                        .put("owner_id", account.getOwnerId().toString())
                        .put("name", account.getName()));
    }

    public static JsonObject accountOpenParams(AccountOpenParams params) {
        return new JsonObject()
                .put("data", new JsonObject()
                        .put("id", params.getId().toString())
                        .put("name", params.getName())
                        .put("owner_id", params.getOwnerId().toString()))
                .put("meta", new JsonObject()
                        .put("links", new JsonObject()
                            .put("some_link", "http://google.com"))
                        .put("some_additional_field", 12345));
    }

    public static JsonObject accountRepresentation(Account account) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"data\": {\n" +
                "    \"balance\": \"%s\",\n" +
                "    \"id\": \"%s\",\n" +
                "    \"owner_id\": \"%s\",\n" +
                "    \"name\": \"%s\",\n" +
                "    \"closed\": %b\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Account\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s\",\n" +
                "      \"deposits\": \"/v1/accounts/%s/deposits\",\n" +
                "      \"withdrawals\": \"/v1/accounts/%s/withdrawals\",\n" +
                "      \"transfers\": \"/v1/accounts/%s/transfers\",\n" +
                "      \"transactions\": \"/v1/accounts/%s/transactions\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                account.getBalance().toString(),
                account.getId().toString(),
                account.getOwnerId().toString(),
                account.getName(),
                account.isClosed(),
                account.getId().toString(),
                account.getId().toString(),
                account.getId().toString(),
                account.getId().toString(),
                account.getId().toString()
        ));
    }

    private static JsonObject accountErrorRepresentation(String id, String code, String details) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"%s\",\n" +
                "    \"details\": \"%s\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Account\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                code,
                details,
                id));
    }

    public static JsonObject accountNotFoundRepresentation(String id) {
        return accountErrorRepresentation(
                id,
                "not_found",
                String.format("Account with given ID not found (ID: %s)", id));
    }

    public static JsonObject accountCloseRepresentation(UUID id) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"AccountClose\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s/close\",\n" +
                "      \"account\": \"/v1/accounts/%s\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                id,
                id
        ));
    }

    public static JsonObject accountCloseErrorRepresentation(String id, String code, String details) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"%s\",\n" +
                "    \"details\": \"%s\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"AccountClose\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s/close\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                code,
                details,
                id));
    }

    public static JsonObject depositParams(Deposit deposit) {
        return new JsonObject()
                .put("data", new JsonObject()
                        .put("id", deposit.getId().toString())
                        .put("destination_account_id", deposit.getDestinationAccountId().toString())
                        .put("amount", deposit.getAmount().toString())
                        .put("title", deposit.getTitle()));
    }

    public static JsonObject depositParams(DepositParams params) {
        return new JsonObject()
                .put("data", new JsonObject()
                        .put("id", params.getId().toString())
                        .put("title", params.getTitle())
                        .put("amount", params.getAmount().toString())
                        .put("destination_account_id", params.getDestinationAccountId().toString()))
                .put("meta", new JsonObject()
                        .put("links", new JsonObject()
                                .put("some_link", "http://google.com"))
                        .put("some_additional_field", 12345));
    }

    public static JsonObject depositRepresentation(Deposit deposit) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"data\": {\n" +
                "    \"id\": \"%s\",\n" +
                "    \"amount\": \"%s\",\n" +
                "    \"title\": \"%s\",\n" +
                "    \"instant\": \"%s\",\n" +
                "    \"destination_account_id\": \"%s\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Deposit\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s/deposits/%s\",\n" +
                "      \"account\": \"/v1/accounts/%s\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                deposit.getId(),
                deposit.getAmount().toString(),
                deposit.getTitle(),
                deposit.getInstant().toString(),
                deposit.getDestinationAccountId().toString(),
                deposit.getDestinationAccountId().toString(),
                deposit.getId(),
                deposit.getDestinationAccountId().toString()
        ));
    }

    public static JsonObject depositsErrorRepresentation(Object accountId, String code, String details) {
        return transactionsErrorRepresentation("Deposit", "deposits", accountId, code, details);
    }

    public static JsonObject withdrawalParams(Withdrawal withdrawal) {
        return new JsonObject()
                .put("data", new JsonObject()
                        .put("id", withdrawal.getId().toString())
                        .put("source_account_id", withdrawal.getSourceAccountId().toString())
                        .put("amount", withdrawal.getAmount().toString())
                        .put("title", withdrawal.getTitle()));
    }

    public static JsonObject withdrawalParams(WithdrawalParams params) {
        return new JsonObject()
                .put("data", new JsonObject()
                        .put("id", params.getId().toString())
                        .put("title", params.getTitle())
                        .put("amount", params.getAmount().toString())
                        .put("source_account_id", params.getSourceAccountId().toString()))
                .put("meta", new JsonObject()
                        .put("links", new JsonObject()
                                .put("some_link", "http://google.com"))
                        .put("some_additional_field", 12345));
    }

    public static JsonObject withdrawalRepresentation(Withdrawal withdrawal) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"data\": {\n" +
                "    \"id\": \"%s\",\n" +
                "    \"amount\": \"%s\",\n" +
                "    \"title\": \"%s\",\n" +
                "    \"instant\": \"%s\",\n" +
                "    \"source_account_id\": \"%s\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Withdrawal\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s/withdrawals/%s\",\n" +
                "      \"account\": \"/v1/accounts/%s\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                withdrawal.getId(),
                withdrawal.getAmount().toString(),
                withdrawal.getTitle(),
                withdrawal.getInstant().toString(),
                withdrawal.getSourceAccountId().toString(),
                withdrawal.getSourceAccountId().toString(),
                withdrawal.getId(),
                withdrawal.getSourceAccountId().toString()
        ));
    }

    public static JsonObject withdrawalsErrorRepresentation(Object accountId, String code, String details) {
        return transactionsErrorRepresentation("Withdrawal", "withdrawals", accountId, code, details);
    }

    public static JsonObject withdrawalsAccountNotFoundRepresentation(String accountId) {
        return withdrawalsErrorRepresentation(
                accountId,
                "not_found",
                String.format("Account with given ID not found (ID: %s)", accountId));
    }

    public static JsonObject transferParams(Transfer transfer) {
        return new JsonObject()
                .put("data", new JsonObject()
                        .put("id", transfer.getId().toString())
                        .put("source_account_id", transfer.getSourceAccountId().toString())
                        .put("destination_account_id", transfer.getDestinationAccountId().toString())
                        .put("amount", transfer.getAmount().toString())
                        .put("title", transfer.getTitle()));
    }

    public static JsonObject transferParams(TransferParams transfer) {
        return new JsonObject()
                .put("data", new JsonObject()
                        .put("id", transfer.getId().toString())
                        .put("source_account_id", transfer.getSourceAccountId().toString())
                        .put("destination_account_id", transfer.getDestinationAccountId().toString())
                        .put("amount", transfer.getAmount().toString())
                        .put("title", transfer.getTitle()));
    }

    public static JsonObject transferRepresentation(Transfer transfer) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"data\": {\n" +
                "    \"id\": \"%s\",\n" +
                "    \"amount\": \"%s\",\n" +
                "    \"title\": \"%s\",\n" +
                "    \"instant\": \"%s\",\n" +
                "    \"source_account_id\": \"%s\",\n" +
                "    \"destination_account_id\": \"%s\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Transfer\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s/transfers/%s\",\n" +
                "      \"source_account\": \"/v1/accounts/%s\",\n" +
                "      \"destination_account\": \"/v1/accounts/%s\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                transfer.getId(),
                transfer.getAmount().toString(),
                transfer.getTitle(),
                transfer.getInstant().toString(),
                transfer.getSourceAccountId().toString(),
                transfer.getDestinationAccountId().toString(),
                transfer.getSourceAccountId().toString(),
                transfer.getId(),
                transfer.getSourceAccountId().toString(),
                transfer.getDestinationAccountId().toString()
        ));
    }

    public static JsonObject transfersErrorRepresentation(Object accountId, String code, String details) {
        return transactionsErrorRepresentation("Transfer", "transfers", accountId, code, details);
    }

    private static JsonObject depositErrorRepresentation(Object accountId, Object id, String code, String details) {
        return transactionErrorRepresentation("Deposit", "deposits", accountId, id, code, details);
    }

    public static JsonObject depositAccountNotFoundRepresentation(Object accountId, Object id) {
        return depositErrorRepresentation(
                accountId,
                id,
                "not_found",
                String.format("Account with given ID not found (ID: %s)", accountId));
    }

    private static JsonObject withdrawalErrorRepresentation(Object accountId, Object id, String code, String details) {
        return transactionErrorRepresentation("Withdrawal", "withdrawals", accountId, id, code, details);
    }

    public static JsonObject withdrawalNotFoundByIdRepresentation(Object accountId, Object id) {
        return withdrawalErrorRepresentation(
                accountId,
                id,
                "not_found",
                String.format("Transaction with given ID not found (ID: %s)", id));
    }

    private static JsonObject transactionsErrorRepresentation(
            String type,
            String link,
            Object accountId,
            String code,
            String details) {
        return new JsonObject(String.format(
                "{\n" +
                        "  \"error\": {\n" +
                        "    \"code\": \"%s\",\n" +
                        "    \"details\": \"%s\"\n" +
                        "  },\n" +
                        "  \"meta\": {\n" +
                        "    \"type\": \"%s\",\n" +
                        "    \"links\": {\n" +
                        "      \"self\": \"/v1/accounts/%s/%s\"\n" +
                        "    }\n" +
                        "  }\n" +
                        "}",
                code,
                details,
                type,
                accountId,
                link));
    }

    private static JsonObject transactionErrorRepresentation(
            String type,
            String link,
            Object accountId,
            Object id,
            String code,
            String details) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"%s\",\n" +
                "    \"details\": \"%s\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"%s\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s/%s/%s\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                code,
                details,
                type,
                accountId,
                link,
                id));
    }

    public static JsonObject transactionsAccountNotFoundRepresentation(Object accountId) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"not_found\",\n" +
                "    \"details\": \"Account with given ID not found (ID: %s)\"\n" +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"Collection\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"/v1/accounts/%s/transactions\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                accountId,
                accountId));
    }
}
