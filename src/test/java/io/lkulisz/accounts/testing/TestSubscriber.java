package io.lkulisz.accounts.testing;

import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import lombok.extern.slf4j.Slf4j;
import rx.Subscriber;

@Slf4j
public class TestSubscriber<T> extends Subscriber<T> {
    private final TestContext testContext;
    private final Async async;

    public TestSubscriber(TestContext testContext, Async async) {
        this.testContext = testContext;
        this.async = async;
    }

    @Override
    public void onCompleted() {
        async.complete();
    }

    @Override
    public void onError(Throwable e) {
        log.error("Unhandled Exception", e);
        testContext.fail(e);
    }

    @Override
    public void onNext(T item) {
    }
}
