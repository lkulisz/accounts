package io.lkulisz.accounts.testing;

import com.google.inject.Binder;
import io.vertx.rxjava.core.Vertx;

import java.util.function.Consumer;

class TestModule extends io.lkulisz.accounts.Module {
    private final Consumer<Binder> bindingsConfigurator;

    TestModule(Vertx vertx, Consumer<Binder> bindingsConfigurator) {
        super(vertx);
        this.bindingsConfigurator = bindingsConfigurator;
    }

    @Override
    protected void configure() {
        super.configure();
        bindingsConfigurator.accept(binder());
    }
}
