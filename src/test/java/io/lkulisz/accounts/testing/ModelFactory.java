package io.lkulisz.accounts.testing;

import com.google.common.collect.ImmutableSet;
import io.lkulisz.accounts.domain.model.*;
import io.lkulisz.accounts.domain.params.*;
import io.lkulisz.accounts.persistence.GenericTransaction;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import org.hibernate.validator.internal.engine.path.PathImpl;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.metadata.ConstraintDescriptor;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.function.Consumer;

import static io.lkulisz.accounts.testing.Constants.DB_MONEY_SCALE;

@SuppressWarnings("WeakerAccess")
public class ModelFactory {
    private static final Random RANDOM = new Random();

    public static Account buildTestAccount() {
        return buildTestAccount(null);
    }

    public static Account buildTestAccount(Consumer<Account.AccountBuilder> builderConsumer) {
        Account.AccountBuilder builder = Account.builder()
                .balance(randomAmount())
                .name("test-account " + UUID.randomUUID())
                .ownerId(UUID.randomUUID())
                .id(UUID.randomUUID())
                .closed(false);

        if (builderConsumer != null) {
            builderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static AccountOpenParams buildTestAccountOpenParams() {
        return buildTestAccountOpenParams(null);
    }

    public static AccountOpenParams buildTestAccountOpenParams(
            Consumer<TestAccountOpenParams.TestAccountOpenParamsBuilder> builderConsumer) {
        TestAccountOpenParams.TestAccountOpenParamsBuilder builder = TestAccountOpenParams.builder()
                .id(UUID.randomUUID())
                .ownerId(UUID.randomUUID())
                .name("test-account");

        if (builderConsumer != null) {
            builderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static Deposit buildTestDeposit(Consumer<Deposit.DepositBuilder> depositBuilderConsumer) {
        Deposit.DepositBuilder builder = Deposit.builder()
                .id(UUID.randomUUID())
                .destinationAccount(buildTestAccount())
                .amount(randomAmount())
                .title("title - " + UUID.randomUUID().toString())
                .instant(Instant.now());

        if (depositBuilderConsumer != null) {
            depositBuilderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static Deposit buildTestDeposit() {
        return buildTestDeposit(UUID.randomUUID());
    }

    public static Deposit buildTestDeposit(UUID accountId) {
        return buildTestDeposit(d -> d.destinationAccount(buildTestAccount(a -> a.id(accountId))));
    }

    public static Withdrawal buildTestWithdrawal(Consumer<Withdrawal.WithdrawalBuilder> builderConsumer) {
        Withdrawal.WithdrawalBuilder builder = Withdrawal.builder()
                .id(UUID.randomUUID())
                .sourceAccount(buildTestAccount())
                .amount(randomAmount())
                .title("title - " + UUID.randomUUID().toString())
                .instant(Instant.now());

        if (builderConsumer != null) {
            builderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static Withdrawal buildTestWithdrawal() {
        return buildTestWithdrawal(UUID.randomUUID());
    }

    public static Withdrawal buildTestWithdrawal(UUID accountId) {
        return buildTestWithdrawal(d -> d.sourceAccount(buildTestAccount(a -> a.id(accountId))));
    }

    public static DepositParams buildTestDepositParams() {
        return buildTestDepositParams(UUID.randomUUID());
    }

    public static DepositParams buildTestDepositParams(UUID accountId) {
        return buildTestDepositParams(deposit -> deposit.destinationAccountId(accountId));
    }

    public static DepositParams buildTestDepositParams(
            Consumer<TestDepositTransactionParams.TestDepositTransactionParamsBuilder> builderConsumer) {
        TestDepositTransactionParams.TestDepositTransactionParamsBuilder builder = TestDepositTransactionParams.builder()
                .id(UUID.randomUUID())
                .destinationAccountId(UUID.randomUUID())
                .amount(randomAmount())
                .title("some deposit - " + UUID.randomUUID().toString());

        if (builderConsumer != null) {
            builderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static WithdrawalParams buildTestWithdrawalParams() {
        return buildTestWithdrawalParams(UUID.randomUUID());
    }

    public static WithdrawalParams buildTestWithdrawalParams(UUID accountId) {
        return buildTestWithdrawalParams(withdrawal -> withdrawal.sourceAccountId(accountId));
    }

    public static WithdrawalParams buildTestWithdrawalParams(
            Consumer<TestWithdrawalTransactionParams.TestWithdrawalTransactionParamsBuilder> builderConsumer) {
        TestWithdrawalTransactionParams.TestWithdrawalTransactionParamsBuilder builder = TestWithdrawalTransactionParams.builder()
                .id(UUID.randomUUID())
                .sourceAccountId(UUID.randomUUID())
                .amount(randomAmount())
                .title("some withdrawal - " + UUID.randomUUID().toString());

        if (builderConsumer != null) {
            builderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static Transfer buildTestTransfer() {
        return buildTestTransfer(null);
    }

    public static Transfer buildTestTransfer(Consumer<Transfer.TransferBuilder> builderConsumer) {
        Transfer.TransferBuilder builder = Transfer.builder()
                .id(UUID.randomUUID())
                .sourceAccount(buildTestAccount())
                .destinationAccount(buildTestAccount())
                .amount(randomAmount())
                .title("title - " + UUID.randomUUID().toString())
                .instant(Instant.now());

        if (builderConsumer != null) {
            builderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static TransferParams buildTestTransferParams() {
        return buildTestTransferParams(null);
    }

    public static TransferParams buildTestTransferParams(
            Consumer<TestTransferTransactionParams.TestTransferTransactionParamsBuilder> builderConsumer) {
        TestTransferTransactionParams.TestTransferTransactionParamsBuilder builder = TestTransferTransactionParams.builder()
                .id(UUID.randomUUID())
                .sourceAccountId(UUID.randomUUID())
                .destinationAccountId(UUID.randomUUID())
                .amount(randomAmount())
                .title("some withdrawal - " + UUID.randomUUID().toString());

        if (builderConsumer != null) {
            builderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static Transaction buildTestTransaction() {
        return buildTestTransaction(null);
    }

    public static Transaction buildTestTransaction(Consumer<GenericTransaction.GenericTransactionBuilder> builderConsumer) {
        GenericTransaction.GenericTransactionBuilder builder = GenericTransaction.builder()
                .id(UUID.randomUUID())
                .amount(randomAmount())
                .sourceAccount(Optional.of(buildTestAccount()))
                .destinationAccount(Optional.of(buildTestAccount()))
                .instant(Instant.now())
                .title("Transaction title " + UUID.randomUUID().toString());

        if (builderConsumer != null) {
            builderConsumer.accept(builder);
        }

        return builder.build();
    }

    public static SingleTransactionQueryCriteria buildQuerySingleCriteria(UUID accountId, UUID transactionId) {
        return new TestSingleTransactionQueryCriteria(accountId, null, transactionId);
    }

    public static SingleTransactionQueryCriteria buildQuerySingleCriteria(Transaction transaction) {
        return buildQuerySingleCriteria(transaction, null);
    }

    public static SingleTransactionQueryCriteria buildQuerySingleCriteria(DepositParams params) {
        return new TestSingleTransactionQueryCriteria(
                params.getDestinationAccountId(),
                TransactionType.DEPOSIT,
                params.getId());
    }

    public static SingleTransactionQueryCriteria buildQuerySingleCriteria(WithdrawalParams params) {
        return new TestSingleTransactionQueryCriteria(
                params.getSourceAccountId(),
                TransactionType.WITHDRAWAL,
                params.getId());
    }

    public static SingleTransactionQueryCriteria buildQuerySingleCriteria(TransferParams params) {
        return new TestSingleTransactionQueryCriteria(
                params.getSourceAccountId(),
                TransactionType.TRANSFER,
                params.getId());
    }

    public static SingleTransactionQueryCriteria buildQuerySingleCriteria(
            Transaction transaction,
            TransactionType type) {
        UUID accountId = transaction.getSourceAccount()
                .map(Account::getId)
                .orElseGet(() -> transaction.getDestinationAccount()
                        .map(Account::getId)
                        .orElseThrow(() -> new IllegalStateException("Transaction does not have any account"))
                );

        type = type != null ? type : transaction.getType();

        return new TestSingleTransactionQueryCriteria(accountId, type, transaction.getId());
    }

    public static SingleTransactionQueryCriteria buildQuerySingleCriteria(
            UUID accountId,
            TransactionType transactionType,
            UUID transactionId) {
        return new TestSingleTransactionQueryCriteria(accountId, transactionType, transactionId);
    }

    public static TransactionsQueryCriteria buildQueryCriteria(
            UUID accountId,
            TransactionType transactionType) {
        return new TestTransactionsQueryCriteria(accountId, transactionType);
    }

    public static TransactionsQueryCriteria buildQueryCriteria(UUID accountId) {
        return new TestTransactionsQueryCriteria(accountId, null);
    }

    public static <T> ConstraintViolation<T> buildConstraintViolation(String path, String message) {
        return new TestConstraintViolation<>(PathImpl.createPathFromString(path), message);
    }

    public static <T> ConstraintViolationException buildConstraintViolationException(String path, String message) {
        ConstraintViolation<T> violation = buildConstraintViolation(path, message);
        return new ConstraintViolationException(ImmutableSet.of(violation));
    }

    private static BigDecimal randomAmount() {
        return BigDecimal.valueOf(RANDOM.nextDouble()).setScale(DB_MONEY_SCALE, BigDecimal.ROUND_HALF_UP);
    }

    @Data
    @Builder
    public static class TestAccountOpenParams implements AccountOpenParams {
        private UUID id;
        private UUID ownerId;
        private String name;
    }

    @Data
    @Builder
    public static class TestDepositTransactionParams implements DepositParams {
        private UUID id;
        private UUID destinationAccountId;
        private BigDecimal amount;
        private String title;
    }

    @Data
    @Builder
    public static class TestWithdrawalTransactionParams implements WithdrawalParams {
        private UUID id;
        private UUID sourceAccountId;
        private BigDecimal amount;
        private String title;
    }

    @Data
    @Builder
    public static class TestTransferTransactionParams implements TransferParams {
        private UUID id;
        private UUID sourceAccountId;
        private UUID destinationAccountId;
        private BigDecimal amount;
        private String title;
    }

    @Getter
    private static class TestConstraintViolation<T> implements ConstraintViolation<T> {
        private String messageTemplate;
        private T rootBean;
        private Class<T> rootBeanClass;
        private Object leafBean;
        private Object[] executableParameters;
        private Object executableReturnValue;
        private ConstraintDescriptor<?> constraintDescriptor;
        private Object invalidValue;

        private Path propertyPath;
        private String message;

        TestConstraintViolation(Path path, String message) {
            this.propertyPath = path;
            this.message = message;
        }

        @Override
        public <U> U unwrap(Class<U> type) {
            throw new IllegalStateException("Not implemented");
        }
    }

    @Data
    @AllArgsConstructor
    private static class TestSingleTransactionQueryCriteria implements SingleTransactionQueryCriteria {
        private UUID accountId;
        private TransactionType transactionType;
        private UUID transactionId;
    }

    @AllArgsConstructor
    private static class TestTransactionsQueryCriteria implements TransactionsQueryCriteria {
        @Getter
        private UUID accountId;
        private TransactionType transactionType;


        @Override
        public Optional<TransactionType> getTransactionType() {
            return Optional.ofNullable(transactionType);
        }
    }
}
