package io.lkulisz.accounts.http.v1.accounts;

import com.google.inject.Binder;
import io.lkulisz.accounts.domain.exception.IllegalAccountStateException;
import io.lkulisz.accounts.domain.model.Account;
import io.lkulisz.accounts.domain.model.Deposit;
import io.lkulisz.accounts.domain.model.Transfer;
import io.lkulisz.accounts.domain.model.Withdrawal;
import io.lkulisz.accounts.domain.params.AccountOpenParams;
import io.lkulisz.accounts.domain.params.DepositParams;
import io.lkulisz.accounts.domain.params.TransferParams;
import io.lkulisz.accounts.domain.params.WithdrawalParams;
import io.lkulisz.accounts.domain.service.AccountsService;
import io.lkulisz.accounts.persistence.DbInitializer;
import io.lkulisz.accounts.testing.ApiTest;
import io.lkulisz.accounts.testing.ModelFactory;
import io.lkulisz.accounts.testing.RepresentationMappers;
import io.lkulisz.accounts.testing.TestSubscriber;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.ext.web.client.HttpResponse;
import org.junit.Test;
import org.mockito.Mockito;
import rx.Single;

import java.util.UUID;
import java.util.function.BiFunction;
import java.util.function.Function;

import static io.lkulisz.accounts.http.util.HttpConstants.APPLICATION_JSON;
import static io.lkulisz.accounts.testing.Matchers.eq;
import static io.lkulisz.accounts.testing.ModelFactory.*;
import static io.lkulisz.accounts.testing.RepresentationMappers.*;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

public class AccountsControllerTest extends ApiTest {

    private AccountsService serviceMock = mock(AccountsService.class);
    private DbInitializer dbInitializerMock = mock(DbInitializer.class);

    @Override
    protected void configureBindings(Binder binder) {
        binder.bind(DbInitializer.class).toInstance(dbInitializerMock);
        binder.bind(AccountsService.class).toInstance(serviceMock);
    }

    @Override
    public void prepare(TestContext context) {
        when(dbInitializerMock.createTables()).thenReturn(Single.just(null));
        super.prepare(context);
    }

    @Test
    public void postAccountShouldReturnCorrectAccountRepresentation(TestContext context) {
        Account account = buildTestAccount();
        when(serviceMock.openAccount(any())).thenReturn(Single.just(account));

        JsonObject emptyRequestBody = new JsonObject().put("data", new JsonObject());

        Async async = context.async();
        postAccount(emptyRequestBody)
                .doOnSuccess(response -> {
                    context.assertEquals(201, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(accountRepresentation(account), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postAccountShouldParseParametersAndPassThemToService(TestContext context) {
        AccountOpenParams expectedParams = buildTestAccountOpenParams();
        when(serviceMock.openAccount(any())).thenReturn(Single.just(buildTestAccount()));

        JsonObject requestBody = new JsonObject()
                .put("data", new JsonObject()
                        .put("id", expectedParams.getId().toString())
                        .put("name", expectedParams.getName())
                        .put("owner_id", expectedParams.getOwnerId().toString()));

        Async async = context.async();
        postAccount(requestBody)
                .doOnSuccess(response -> verify(serviceMock).openAccount(argThat(eq(expectedParams))))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getAccountShouldReturnCorrectAccountRepresentation(TestContext context) {
        Account account = buildTestAccount();
        when(serviceMock.getAccount(any())).thenReturn(Single.just(account));

        Async async = context.async();
        getAccount(account.getId().toString())
                .doOnSuccess(response -> {
                    context.assertEquals(200, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(accountRepresentation(account), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getAccountShouldReturn404WhenIdCannotBeParsedToUuid(TestContext context) {
        String id = "some-random-string";
        Async async = context.async();
        getAccount(id)
                .doOnSuccess(response -> {
                    context.assertEquals(404, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(accountNotFoundRepresentation(id), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void pastAccountShouldAcceptAdditionalAttributesInMeta(TestContext context) {
        AccountOpenParams expectedParams = buildTestAccountOpenParams();
        when(serviceMock.openAccount(any())).thenReturn(Single.just(buildTestAccount()));

        Async async = context.async();
        postAccount(accountOpenParams(expectedParams))
                .doOnSuccess(response -> verify(serviceMock).openAccount(argThat(eq(expectedParams))))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postAccountCloseShouldReturnCorrectRepresentation(TestContext context) {
        UUID accountId = UUID.randomUUID();
        when(serviceMock.closeAccount(Mockito.eq(accountId))).thenReturn(Single.just(buildTestAccount(
                account -> account.id(accountId))));

        Async async = context.async();
        postAccountClose(accountId.toString())
                .doOnSuccess(response -> {
                    context.assertEquals(201, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(accountCloseRepresentation(accountId), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postAccountCloseShouldReturn422WhenAccountCannotBeClosed(TestContext context) {
        UUID id = UUID.randomUUID();
        when(serviceMock.closeAccount(Mockito.eq(id))).thenReturn(Single.error(new IllegalAccountStateException(id)));

        JsonObject expectedResponse = accountCloseErrorRepresentation(
                id.toString(),
                "illegal_state",
                String.format("Account state is illegal for this operation (ID: %s)", id));

        Async async = context.async();
        postAccountClose(id.toString())
                .doOnSuccess(response -> {
                    context.assertEquals(422, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(expectedResponse, response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postDepositShouldReturnCorrectRepresentation(TestContext context) {
        DepositParams params = buildTestDepositParams();
        Deposit deposit = buildTestDeposit();
        when(serviceMock.deposit(argThat(eq(params)))).thenReturn(Single.just(deposit));

        Async async = context.async();
        postDeposit(params.getDestinationAccountId().toString(), depositParams(params))
                .doOnSuccess(response -> {
                    context.assertEquals(201, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(depositRepresentation(deposit), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postDepositShouldReturn400WhenRequestPayloadIsNull(TestContext context) {
        UUID accountId = UUID.randomUUID();

        JsonObject expectedResponse = depositsErrorRepresentation(
                accountId.toString(),
                "invalid_payload",
                "`data`: must not be null");

        Async async = context.async();
        postDeposit(accountId.toString(), new JsonObject())
                .doOnSuccess(response -> {
                    context.assertEquals(400, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(expectedResponse, response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postWithdrawalShouldReturnCorrectRepresentation(TestContext context) {
        WithdrawalParams params = buildTestWithdrawalParams();
        Withdrawal withdrawal = buildTestWithdrawal();
        when(serviceMock.withdraw(argThat(eq(params)))).thenReturn(Single.just(withdrawal));

        Async async = context.async();
        postWithdrawal(params.getSourceAccountId().toString(), withdrawalParams(params))
                .doOnSuccess(response -> {
                    context.assertEquals(201, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(withdrawalRepresentation(withdrawal), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postWithdrawalShouldReturn400WhenRequestPayloadIsNull(TestContext context) {
        UUID accountId = UUID.randomUUID();

        JsonObject expectedResponse = withdrawalsErrorRepresentation(
                accountId.toString(),
                "invalid_payload",
                "`data`: must not be null");

        Async async = context.async();
        postWithdrawal(accountId.toString(), new JsonObject())
                .doOnSuccess(response -> {
                    context.assertEquals(400, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(expectedResponse, response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postWithdrawalShouldReturn404WhenAccountIdCannotBeParsed(TestContext context) {
        String id = "some-random-string";
        Async async = context.async();
        postWithdrawal(id, new JsonObject())
                .doOnSuccess(response -> {
                    context.assertEquals(404, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(withdrawalsAccountNotFoundRepresentation(id), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postTransferShouldReturnCorrectRepresentation(TestContext context) {
        TransferParams params = buildTestTransferParams();
        Transfer transfer = buildTestTransfer();
        when(serviceMock.transfer(argThat(eq(params)))).thenReturn(Single.just(transfer));

        Async async = context.async();
        postTransfer(params.getSourceAccountId().toString(), transferParams(params))
                .doOnSuccess(response -> {
                    context.assertEquals(201, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(transferRepresentation(transfer), response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postTransferShouldReturn400WhenRequestPayloadIsNull(TestContext context) {
        UUID accountId = UUID.randomUUID();

        JsonObject expectedResponse = transfersErrorRepresentation(
                accountId.toString(),
                "invalid_payload",
                "`data`: must not be null");

        Async async = context.async();
        postTransfer(accountId.toString(), new JsonObject())
                .doOnSuccess(response -> {
                    context.assertEquals(400, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(expectedResponse, response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void postTransferShouldReturn422OnConstraintViolationWithoutPath(TestContext context) {
        when(serviceMock.transfer(any())).thenReturn(Single.error(
                ModelFactory.<AccountOpenParams>buildConstraintViolationException("", "Source and Destination Account IDs must be different")));
        UUID accountId = UUID.randomUUID();

        JsonObject expectedResponse = transfersErrorRepresentation(
                accountId, "invalid_payload", "Source and Destination Account IDs must be different");

        Async async = context.async();
        postTransfer(accountId, transferParams(buildTestTransferParams()))
                .doOnSuccess(response -> {
                    context.assertEquals(422, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(expectedResponse, response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getDepositShouldReturn404WhenAccountIdCannotBeParsed(TestContext context) {
        testGetTransactionReturns404WhenResourceIdCannotBeParsed(
                context,
                "some-account-id",
                UUID.randomUUID(),
                this::getDeposit,
                RepresentationMappers::depositAccountNotFoundRepresentation);
    }

    @Test
    public void getWithdrawalShouldReturn404WhenTransactionIdCannotBeParsed(TestContext context) {
        testGetTransactionReturns404WhenResourceIdCannotBeParsed(
                context,
                UUID.randomUUID(),
                "some-transaction-id",
                this::getWithdrawal,
                RepresentationMappers::withdrawalNotFoundByIdRepresentation);
    }

    @Test
    public void getTransactionsShouldReturn404WhenAccountIdCannotBeParsed(TestContext context) {
        testGetTransactionsReturns404WhenResourceIdCannotBeParsed(
                context,
                "some-account-id",
                this::getTransactions,
                RepresentationMappers::transactionsAccountNotFoundRepresentation);
    }

    private void testGetTransactionReturns404WhenResourceIdCannotBeParsed(
            TestContext context,
            Object accountId,
            Object transactionId,
            BiFunction<Object, Object, Single<HttpResponse<Buffer>>> requestBuilder,
            BiFunction<Object, Object, JsonObject> expectedRepresentationBuilder) {
        Async async = context.async();
        requestBuilder.apply(accountId, transactionId)
                .doOnSuccess(response -> {
                    context.assertEquals(404, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(
                            expectedRepresentationBuilder.apply(accountId, transactionId),
                            response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    private void testGetTransactionsReturns404WhenResourceIdCannotBeParsed(
            TestContext context,
            Object accountId,
            Function<Object, Single<HttpResponse<Buffer>>> requestBuilder,
            Function<Object, JsonObject> expectedRepresentationBuilder) {
        Async async = context.async();
        requestBuilder.apply(accountId)
                .doOnSuccess(response -> {
                    context.assertEquals(404, response.statusCode());
                    context.assertEquals(APPLICATION_JSON, response.getHeader(CONTENT_TYPE.toString()));
                    context.assertEquals(
                            expectedRepresentationBuilder.apply(accountId),
                            response.bodyAsJsonObject());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }
}