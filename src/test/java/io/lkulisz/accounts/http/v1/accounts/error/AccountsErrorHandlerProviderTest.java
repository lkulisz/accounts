package io.lkulisz.accounts.http.v1.accounts.error;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.lkulisz.accounts.domain.exception.*;
import io.lkulisz.accounts.http.error.ErrorHandler;
import io.lkulisz.accounts.http.util.Envelope;
import io.lkulisz.accounts.testing.TestSubscriber;
import io.vertx.core.Future;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.rxjava.core.AbstractVerticle;
import io.vertx.rxjava.core.RxHelper;
import io.vertx.rxjava.core.Vertx;
import io.vertx.rxjava.core.buffer.Buffer;
import io.vertx.rxjava.ext.web.Router;
import io.vertx.rxjava.ext.web.RoutingContext;
import io.vertx.rxjava.ext.web.client.WebClient;
import io.vertx.rxjava.ext.web.handler.BodyHandler;
import io.vertx.rxjava.ext.web.handler.ResponseContentTypeHandler;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import rx.Single;

import javax.validation.ConstraintViolationException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.UUID;

import static io.lkulisz.accounts.http.error.NullRequestPayloadException.throwWhenNull;
import static io.lkulisz.accounts.http.util.HttpConstants.APPLICATION_JSON;
import static io.lkulisz.accounts.testing.ModelFactory.buildConstraintViolationException;
import static io.vertx.core.http.HttpHeaders.ACCEPT;
import static io.vertx.core.http.HttpHeaders.CONTENT_TYPE;

@Slf4j
@SuppressWarnings("ThrowableNotThrown")
@RunWith(VertxUnitRunner.class)
public class AccountsErrorHandlerProviderTest {
    private static final int PORT = 8081;
    private static final String RESOURCE_TYPE = "TestTransaction";

    private static final String CONSTRAINT_VIOLATION_WITH_ATTRIBUTE_PATH = "/constraint_violation_with_attribute_path";
    private static final String CONSTRAINT_VIOLATION_WITHOUT_ATTRIBUTE_PATH = "/constraint_violation_without_attribute_path";
    private static final String UNKNOWN = "/unknown";
    private static final String JSON = "/json";
    private static final String ACCOUNT_NOT_FOUND = "/account_not_found";
    private static final String TRANSACTION_NOT_FOUND = "/transaction_not_found";
    private static final String DUPLICATE_ACCOUNT = "/duplicate_account";
    private static final String DUPLICATE_TRANSACTION = "/duplicate_transaction";
    private static final String ILLEGAL_ACCOUNT_STATE = "/illegal_account_state";
    private static final String STALE_ACCOUNT_STATE = "/stale_account_state";

    private static final UUID RESOURCE_ID = UUID.randomUUID();
    private static final String ERROR_DETAILS = "Wow. Much error. Amaze";

    private static Vertx vertx;
    private static WebClient webClient;

    @BeforeClass
    public static void prepare(TestContext context) {
        vertx = Vertx.vertx();
        webClient = WebClient.create(vertx, new WebClientOptions()
                .setDefaultHost("localhost")
                .setDefaultPort(PORT));

        Async async = context.async();
        RxHelper.deployVerticle(vertx, new TestVerticle(new AccountsErrorHandlerProvider().get()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @AfterClass
    public static void finish(TestContext context) {
        vertx.close(context.asyncAssertSuccess());
    }

    @Test
    public void errorHandleShouldRepresentConstraintViolationWithAttributePath(TestContext context) {
        testGet(context, CONSTRAINT_VIOLATION_WITH_ATTRIBUTE_PATH,
                422, "invalid_payload", "`data.super.duper_attribute`: " + ERROR_DETAILS);
    }

    @Test
    public void errorHandleShouldRepresentConstraintViolationWithoutPath(TestContext context) {
        testGet(context, CONSTRAINT_VIOLATION_WITHOUT_ATTRIBUTE_PATH, 422, "invalid_payload", ERROR_DETAILS);
    }

    @Test
    public void errorHandlerShouldRepresentUnknownError(TestContext context) {
        testGet(context, UNKNOWN, 500, "unknown", null);
    }

    @Test
    public void errorHandlerShouldHandleInvalidJsonFormat(TestContext context) {
        testPost(
                context,
                "some random payload which definitely cannot be parsed to JSON",
                400,
                "invalid_payload",
                "Failed to decode:Unrecognized token 'some': was expecting ('true', 'false' or 'null')\\n " +
                        "at [Source: some random payload which definitely cannot be parsed to JSON; line: 1, column: 5]"
        );
    }

    @Test
    public void errorHandlerShouldHandleInvalidJsonAttributeTypes(TestContext context) {
        JsonObject body = new JsonObject()
                .put("data", new JsonObject()
                        .put("id", "some not UUID"));

        testPost(
                context,
                body.toString(),
                400,
                "invalid_payload",
                "UUID has to be represented by standard 36-char representation: input String 'some not UUID'\\n " +
                        "at [Source: N/A; line: -1, column: -1] " +
                        "(through reference chain: io.lkulisz.accounts.http.v1.accounts.error.TestRepresentation[\\\"data\\\"]" +
                        "->io.lkulisz.accounts.http.v1.accounts.error.Data[\\\"id\\\"])"
        );
    }

    @Test
    public void errorHandlerShouldHandleInvalidJsonObjectStructure(TestContext context) {
        JsonObject body = new JsonObject()
                .put("data", new JsonObject()
                        .put("id", new JsonObject()
                                .put("json_object", "instead_of_value")));

        testPost(
                context,
                body.toString(),
                400,
                "invalid_payload",
                "Can not deserialize instance of java.util.UUID out of START_OBJECT token\\n " +
                        "at [Source: N/A; line: -1, column: -1] " +
                        "(through reference chain: io.lkulisz.accounts.http.v1.accounts.error.TestRepresentation[\\\"data\\\"]" +
                        "->io.lkulisz.accounts.http.v1.accounts.error.Data[\\\"id\\\"])"
        );
    }

    @Test
    public void errorHandlerShouldHandleUnrecognizedJsonAttributes(TestContext context) {
        JsonObject body = new JsonObject()
                .put("data", new JsonObject()
                        .put("unrecognized", "property"));

        testPost(
                context,
                body.toString(),
                400,
                "invalid_payload",
                "Unrecognized field `data.unrecognized`"
        );
    }

    @Test
    public void errorHandlerShouldHandleNullData(TestContext context) {
        JsonObject body = new JsonObject();

        testPost(
                context,
                body.toString(),
                400,
                "invalid_payload",
                "`data`: must not be null"
        );
    }

    @Test
    public void errorHandlerShouldHandleAccountNotFound(TestContext context) {
        testGet(context, ACCOUNT_NOT_FOUND, 404, "not_found",
                "Account with given ID not found (ID: " + RESOURCE_ID.toString() + ")");
    }

    @Test
    public void errorHandlerShouldHandleDuplicateAccount(TestContext context) {
        testGet(context, DUPLICATE_ACCOUNT, 409, "duplicate",
                "Account already exists (ID: " + RESOURCE_ID.toString() + ")");
    }

    @Test
    public void errorHandlerShouldHandleIllegalAccountState(TestContext context) {
        testGet(context, ILLEGAL_ACCOUNT_STATE, 422, "illegal_state", ERROR_DETAILS + " (ID: " + RESOURCE_ID.toString() + ")");
    }

    @Test
    public void errorHandlerShouldHandleStaleAccountState(TestContext context) {
        testGet(context, STALE_ACCOUNT_STATE, 409, "conflict",
                "Account has been concurrently modified while executing request (ID: " + RESOURCE_ID.toString() + ")");
    }

    @Test
    public void errorHandlerShouldHandleTransactionNotFound(TestContext context) {
        testGet(context, TRANSACTION_NOT_FOUND, 404, "not_found",
                "Transaction with given ID not found (ID: " + RESOURCE_ID.toString() + ")");
    }

    @Test
    public void errorHandlerShouldHandleDuplicateTransaction(TestContext context) {
        testGet(context, DUPLICATE_TRANSACTION, 409, "duplicate",
                "Transaction already exists (ID: " + RESOURCE_ID.toString() + ")");
    }

    private void testGet(TestContext context, String path, int statusCode, String code, String details) {
        Async async = context.async();
        webClient.get(path)
                .rxSend()
                .doOnSuccess(response -> {
                    context.assertEquals(statusCode, response.statusCode());
                    context.assertEquals(
                            errorRepresentation(code, details, path),
                            response.bodyAsJsonObject()
                    );
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    private void testPost(
            TestContext context,
            String json,
            int statusCode,
            String code,
            String details) {
        Async async = context.async();
        webClient.post(JSON)
                .putHeader(ACCEPT.toString(), APPLICATION_JSON)
                .putHeader(CONTENT_TYPE.toString(), APPLICATION_JSON)
                .rxSendBuffer(Buffer.buffer(json))
                .doOnSuccess(response -> {
                    context.assertEquals(statusCode, response.statusCode());
                    context.assertEquals(
                            errorRepresentation(code, details, JSON),
                            response.bodyAsJsonObject()
                    );
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    private static JsonObject errorRepresentation(
            String code,
            String details,
            String selfLink) {
        return new JsonObject(String.format(
                "{\n" +
                "  \"error\": {\n" +
                "    \"code\": \"%s\"\n" +
                        (details != null
                            ? String.format(",\"details\": \"%s\"\n", details)
                            : "") +
                "  },\n" +
                "  \"meta\": {\n" +
                "    \"type\": \"%s\",\n" +
                "    \"links\": {\n" +
                "      \"self\": \"%s\"\n" +
                "    }\n" +
                "  }\n" +
                "}",
                code,
                RESOURCE_TYPE,
                selfLink));
    }

    private static class TestVerticle extends AbstractVerticle {
        private final ErrorHandler errorHandler;

        TestVerticle(ErrorHandler errorHandler) {
            this.errorHandler = errorHandler;
        }

        @Override
        public void start(Future<Void> startFuture) {
            Json.mapper.setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
            Router router = Router.router(vertx);
            router.route().handler(BodyHandler.create());
            router.route().produces(APPLICATION_JSON).handler(ResponseContentTypeHandler.create());

            router.get(CONSTRAINT_VIOLATION_WITH_ATTRIBUTE_PATH).handler(this::constraintViolationWithAttributePath);
            router.get(CONSTRAINT_VIOLATION_WITHOUT_ATTRIBUTE_PATH).handler(this::constraintViolationWithoutAttributePath);
            router.get(UNKNOWN).handler(this::generic);
            router.post(JSON).consumes(APPLICATION_JSON).handler(this::json);
            router.get(ACCOUNT_NOT_FOUND).handler(this::accountNotFound);
            router.get(TRANSACTION_NOT_FOUND).handler(this::transactionNotFound);
            router.get(DUPLICATE_ACCOUNT).handler(this::duplicateAccount);
            router.get(DUPLICATE_TRANSACTION).handler(this::duplicateTransaction);
            router.get(ILLEGAL_ACCOUNT_STATE).handler(this::illegalAccountState);
            router.get(STALE_ACCOUNT_STATE).handler(this::staleAccountState);

            vertx.createHttpServer()
                    .requestHandler(router::accept)
                    .rxListen(PORT)
                    .doOnSuccess(httpServer -> log.info("Test HTTP server running on port {}", PORT))
                    .doOnError(failure -> log.error("Could not start Test HTTP server", failure))
                    .subscribe(server -> startFuture.complete(), startFuture::fail);
        }

        private void constraintViolationWithAttributePath(RoutingContext rc) {
            ConstraintViolationException exception =
                    buildConstraintViolationException("super.duperAttribute", ERROR_DETAILS);
            errorHandler.handle(rc, exception, RESOURCE_TYPE);
        }

        private void constraintViolationWithoutAttributePath(RoutingContext rc) {
            ConstraintViolationException exception = buildConstraintViolationException("", ERROR_DETAILS);
            errorHandler.handle(rc, exception, RESOURCE_TYPE);
        }

        private void generic(RoutingContext rc) {
            errorHandler.handle(rc, new Exception(ERROR_DETAILS), RESOURCE_TYPE);
        }

        private void json(RoutingContext rc) {
            //noinspection ResultOfMethodCallIgnored
            Single.just(rc)
                    .map(context -> context.getBodyAsJson().mapTo(TestRepresentation.class))
                    .doOnSuccess(representation -> throwWhenNull(representation.getData()))
                    .subscribe(
                            representation -> rc.response().setStatusCode(201).end(representation.toJsonString()),
                            error -> errorHandler.handle(rc, error, RESOURCE_TYPE)
                    );
        }

        private void accountNotFound(RoutingContext rc) {
            errorHandler.handle(rc, new AccountNotFoundException(RESOURCE_ID.toString()), RESOURCE_TYPE);
        }

        private void transactionNotFound(RoutingContext rc) {
            errorHandler.handle(rc, new TransactionNotFoundException(RESOURCE_ID.toString()), RESOURCE_TYPE);
        }

        private void duplicateAccount(RoutingContext rc) {
            errorHandler.handle(rc, new DuplicateAccountException(RESOURCE_ID), RESOURCE_TYPE);
        }

        private void duplicateTransaction(RoutingContext rc) {
            errorHandler.handle(rc, new DuplicateTransactionException(RESOURCE_ID), RESOURCE_TYPE);
        }

        private void illegalAccountState(RoutingContext rc) {
            errorHandler.handle(rc, new IllegalAccountStateException(RESOURCE_ID, ERROR_DETAILS), RESOURCE_TYPE);
        }

        private void staleAccountState(RoutingContext rc) {
            errorHandler.handle(rc, new StaleAccountStateException(RESOURCE_ID), RESOURCE_TYPE);
        }
    }

    @NoArgsConstructor
    public static class TestRepresentation extends Envelope<TestRepresentation.Data, Envelope.Meta<Envelope.Links>> {
        @NoArgsConstructor
        @lombok.Data
        public static class Data {
            @JsonSerialize(using = ToStringSerializer.class)
            private UUID id;
            @JsonSerialize(using = ToStringSerializer.class)
            private BigDecimal amount;
            private String title;
            @JsonSerialize(using = ToStringSerializer.class)
            private Instant instant;
            private UUID destinationAccountId;
        }
    }
}