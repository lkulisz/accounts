package io.lkulisz.accounts.persistence;

import com.google.inject.Inject;
import io.lkulisz.accounts.domain.exception.AccountNotFoundException;
import io.lkulisz.accounts.domain.exception.DuplicateTransactionException;
import io.lkulisz.accounts.domain.exception.StaleAccountStateException;
import io.lkulisz.accounts.domain.exception.TransactionNotFoundException;
import io.lkulisz.accounts.domain.model.*;
import io.lkulisz.accounts.domain.params.SingleTransactionQueryCriteria;
import io.lkulisz.accounts.domain.params.TransactionsQueryCriteria;
import io.lkulisz.accounts.domain.repository.AccountsRepository;
import io.lkulisz.accounts.testing.ServiceTest;
import io.lkulisz.accounts.testing.TestSubscriber;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import io.vertx.rxjava.ext.jdbc.JDBCClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import rx.Single;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

import static io.lkulisz.accounts.testing.Matchers.eq;
import static io.lkulisz.accounts.testing.ModelFactory.*;
import static org.mockito.Mockito.when;

@SuppressWarnings("ConstantConditions")
@RunWith(VertxUnitRunner.class)
public class JdbcAccountsRepositoryTest extends ServiceTest {
    @Inject
    private JdbcRepositoryProvider repositoryProvider;
    @Inject
    private JDBCClient dbClient;

    @Test
    public void saveShouldInsertNewAccountWhenAccountDoesNotHaveDbId(TestContext context) {
        Account newAccount = buildTestAccount();

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(newAccount)
                .flatMap(savedAccount -> repository.getAccountById(newAccount.getId()))
                .doOnSuccess(retrievedAccount -> context.assertTrue(eq(newAccount).matches(retrievedAccount)))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void saveShouldUpdateExistingAccount(TestContext context) {
        Account startingAccount = buildTestAccount();

        Account mockAccount = Mockito.mock(Account.class);
        when(mockAccount.getId()).thenReturn(startingAccount.getId());
        when(mockAccount.getBalance()).thenReturn(startingAccount.getBalance());
        when(mockAccount.getOwnerId()).thenReturn(startingAccount.getOwnerId());
        when(mockAccount.getName()).thenReturn(startingAccount.getName());
        when(mockAccount.isClosed()).thenReturn(false);

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(mockAccount)
                .flatMap(x -> repository.getAccountById(startingAccount.getId()))
                .doOnSuccess(savedAccount -> context.assertTrue(eq(savedAccount).matches(startingAccount)))
                .flatMap(x -> {
                    when(mockAccount.getBalance()).thenReturn(new BigDecimal("15.0000"));
                    when(mockAccount.getOwnerId()).thenReturn(UUID.randomUUID());
                    when(mockAccount.getName()).thenReturn("Updated account name");
                    when(mockAccount.isClosed()).thenReturn(true);
                    return repository.save(mockAccount);
                })
                .flatMap(x -> getAccountVersion(startingAccount.getId()))
                .doOnSuccess(version -> context.assertEquals(2, version))
                .flatMap(x -> repository.getAccountById(startingAccount.getId()))
                .doOnSuccess(savedUpdatedAccount -> {
                    context.assertFalse(eq(startingAccount).matches(savedUpdatedAccount));
                    context.assertTrue(eq(mockAccount).matches(savedUpdatedAccount));
                })
                .flatMap(x -> countAccountRecords())
                .doOnSuccess(accountsCount -> context.assertEquals(1L, accountsCount))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void saveShouldThrowWhenStoredAccountVersionIsGreater(TestContext context) {
        Account account = buildTestAccount();

        AccountsRepository repository = repositoryProvider.get();
        AccountsRepository otherRepository = repositoryProvider.get();

        Async async = context.async();
        repository.save(account)
                .flatMap(savedAccount -> otherRepository.getAccountById(account.getId()))
                .flatMap(otherRepository::save)
                .flatMap(x -> repository.save(account))
                .doOnSuccess(x -> context.fail("Expected StaleAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(StaleAccountStateException.class, error.getClass());
                    context.assertEquals(account.getId(), ((StaleAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void saveShouldThrowWhenUpdatingAccountWhichNoLongerExists(TestContext context) {
        Account account = buildTestAccount();

        AccountsRepository repository = repositoryProvider.get();

        Async async = context.async();
        repository.save(account)
                .flatMap(savedAccount -> deleteAccount(account.getId()))
                .flatMap(x -> repository.save(account))
                .doOnSuccess(x -> context.fail("Expected StaleAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(StaleAccountStateException.class, error.getClass());
                    context.assertEquals(account.getId(), ((StaleAccountStateException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void saveShouldInsertNewTransactionRecord(TestContext context) {
        Transaction transaction = buildTestTransaction();

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(transaction.getSourceAccount().get())
                .flatMap(x -> repository.save(transaction.getDestinationAccount().get()))
                .flatMap(x -> repository.save(transaction))
                .flatMap(savedTransaction -> repository.getTransaction(buildQuerySingleCriteria(savedTransaction)))
                .doOnSuccess(storedTransaction -> {
                    context.assertEquals(transaction.getId(), storedTransaction.getId());
                    context.assertEquals(transaction.getTitle(), storedTransaction.getTitle());
                    context.assertEquals(transaction.getAmount(), storedTransaction.getAmount());
                    context.assertEquals(transaction.getInstant(), storedTransaction.getInstant());
                    context.assertTrue(eq(transaction.getSourceAccount().get()).matches(storedTransaction.getSourceAccount().get()));
                    context.assertTrue(eq(transaction.getDestinationAccount().get()).matches(storedTransaction.getDestinationAccount().get()));
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void saveShouldRollbackAndThrowWhenTransactionAlreadyThere(TestContext context) {
        Transaction transaction = buildTestTransaction();

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(transaction.getSourceAccount().get())
                .flatMap(x -> repository.save(transaction.getDestinationAccount().get()))
                .flatMap(x -> repository.save(transaction))
                .flatMap(x -> repository.save(transaction)) // should throw
                .doOnSuccess(x -> context.fail("Expected DuplicateTransactionException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(DuplicateTransactionException.class, error.getClass());
                    context.assertEquals(transaction.getId(), ((DuplicateTransactionException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void saveShouldUpdateSourceAccountIfPresent(TestContext context) {
        Account sourceAccount = buildTestAccount(a -> a.balance(BigDecimal.ZERO));
        Transaction transaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(sourceAccount))
                .destinationAccount(Optional.empty()));

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(sourceAccount)
                .flatMap(x -> repository.save(transaction))
                .flatMap(x -> getAccountVersion(sourceAccount.getId()))
                .doOnSuccess(version -> context.assertEquals(2, version))
                .flatMap(x -> repository.getAccountById(sourceAccount.getId()))
                .doOnSuccess(storedSourceAccount ->
                        context.assertEquals(storedSourceAccount.getName(), sourceAccount.getName()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void saveShouldUpdateDestinationAccountIfPresent(TestContext context) {
        Account destinationAccount = buildTestAccount();
        Transaction transaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.empty())
                .destinationAccount(Optional.of(destinationAccount)));

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(destinationAccount)
                .flatMap(x -> repository.save(transaction))
                .flatMap(x -> getAccountVersion(destinationAccount.getId()))
                .doOnSuccess(version -> context.assertEquals(2, version))
                .flatMap(x -> repository.getAccountById(destinationAccount.getId()))
                .doOnSuccess(storedSourceAccount ->
                        context.assertEquals(storedSourceAccount.getName(), destinationAccount.getName()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void saveShouldRollbackAndThrowWhenStoredSourceAccountIsGreater(TestContext context) {
        Account sourceAccount = buildTestAccount();
        Transaction transaction = buildTestTransaction(t -> t.sourceAccount(Optional.of(sourceAccount)));
        testRollbackAndThrowWhenTransactionAccountStateIsStale(context, sourceAccount, transaction);
    }

    @Test
    public void saveShouldRollbackAndThrowWhenStoredDestinationAccountIsGreater(TestContext context) {
        Account destinationAccount = buildTestAccount();
        Transaction transaction = buildTestTransaction(t -> t.destinationAccount(
                Optional.of(destinationAccount)));

        testRollbackAndThrowWhenTransactionAccountStateIsStale(context, destinationAccount, transaction);
    }

    @Test
    public void saveShouldRollbackAndThrowWhenSourceAccountDoesNotExist(TestContext context) {
        Account sourceAccount = buildTestAccount();
        Account destinationAccount = buildTestAccount();
        Transaction transaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(sourceAccount))
                .destinationAccount(Optional.of(destinationAccount)));

        testRollbackAndThrowWhenTransactionAccountDoesNotExist(context, destinationAccount, sourceAccount, transaction);
    }

    @Test
    public void saveShouldRollbackAndThrowWhenDestinationAccountDoesNotExist(TestContext context) {
        Account sourceAccount = buildTestAccount();
        Account destinationAccount = buildTestAccount();
        Transaction transaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(sourceAccount))
                .destinationAccount(Optional.of(destinationAccount)));

        testRollbackAndThrowWhenTransactionAccountDoesNotExist(context, sourceAccount, destinationAccount, transaction);
    }

    @Test
    public void getTransactionShouldReturnTransactionWithSourceAccount(TestContext context) {
        Transaction transaction = buildTestTransaction(t -> t.destinationAccount(Optional.empty()));
        testGetTransactionByQuery(context, transaction.getSourceAccount().get(), transaction);
    }

    @Test
    public void getTransactionShouldReturnTransactionWithDestinationAccount(TestContext context) {
        Transaction transaction = buildTestTransaction(t -> t.sourceAccount(Optional.empty()));
        testGetTransactionByQuery(context, transaction.getDestinationAccount().get(), transaction);
    }

    @Test
    public void getTransactionShouldThrowWhenTransactionDoesNotExist(TestContext context) {
        Transaction transaction = buildTestTransaction(t -> t.destinationAccount(Optional.empty()));

        AccountsRepository repository = repositoryProvider.get();
        Single<Transaction> action = repository.save(transaction.getSourceAccount().get())
                .flatMap(x -> repository.getTransaction(buildQuerySingleCriteria(transaction)));

        verifyTransactionNotFoundException(context, transaction.getId(), action);
    }

    @Test
    public void getTransactionShouldThrowWhenTransactionIsOfDifferentType(TestContext context) {
        Transaction transaction = buildTestTransaction(t -> t.destinationAccount(Optional.empty()));
        SingleTransactionQueryCriteria criteria = buildQuerySingleCriteria(transaction, TransactionType.DEPOSIT);

        AccountsRepository repository = repositoryProvider.get();
        Single<Transaction> action = repository.save(transaction.getSourceAccount().get())
                .flatMap(x -> repository.save(transaction))
                .flatMap(x -> repository.getTransaction(criteria));

        verifyTransactionNotFoundException(context, transaction.getId(), action);
    }

    @Test
    public void getTransactionShouldThrowWhenAccountDoesNotExist(TestContext context) {
        Transaction transaction = buildTestTransaction(t -> t.destinationAccount(Optional.empty()));
        SingleTransactionQueryCriteria criteria = buildQuerySingleCriteria(UUID.randomUUID(), transaction.getId());

        AccountsRepository repository = repositoryProvider.get();
        Single<Transaction> action = repository.save(transaction.getSourceAccount().get())
                .flatMap(x -> repository.save(transaction))
                .flatMap(x -> repository.getTransaction(criteria));

        verifyTransactionNotFoundException(context, transaction.getId(), action);
    }

    @Test
    public void getTransactionsShouldReturnTransactionWithSourceAndDestinationAccounts(TestContext context) {
        Account account = buildTestAccount();

        Transaction firstTransaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(account))
                .destinationAccount(Optional.empty()));
        Transaction secondTransaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.empty())
                .destinationAccount(Optional.of(account)));

        TransactionsQueryCriteria queryCriteria = buildQueryCriteria(account.getId());

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(account)
                .flatMap(x -> repository.save(firstTransaction))
                .flatMap(x -> repository.save(secondTransaction))
                .flatMapObservable(x -> repository.getTransactions(queryCriteria))
                .toList()
                .toSingle()
                .doOnSuccess(retrievedTransactions -> {
                    context.assertEquals(2, retrievedTransactions.size());
                    context.assertEquals(firstTransaction.getId(), retrievedTransactions.get(0).getId());
                    context.assertEquals(secondTransaction.getId(), retrievedTransactions.get(1).getId());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getTransactionsShouldFilterOutTransactionsFromOtherAccounts(TestContext context) {
        Account account = buildTestAccount();
        Account otherAccount = buildTestAccount();

        Transaction firstTransaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(account))
                .destinationAccount(Optional.empty()));
        Transaction secondTransaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(account))
                .destinationAccount(Optional.empty()));

        Transaction firstTransactionOnOtherAccount = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(otherAccount))
                .destinationAccount(Optional.empty()));
        Transaction secondTransactionOnOtherAccount = buildTestTransaction(t -> t
                .sourceAccount(Optional.empty())
                .destinationAccount(Optional.of(otherAccount)));

        TransactionsQueryCriteria queryCriteria = buildQueryCriteria(account.getId());

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(account)
                .flatMap(x -> repository.save(otherAccount))
                .flatMap(x -> repository.save(firstTransaction))
                .flatMap(x -> repository.save(firstTransactionOnOtherAccount))
                .flatMap(x -> repository.save(secondTransaction))
                .flatMap(x -> repository.save(secondTransactionOnOtherAccount))
                .flatMapObservable(x -> repository.getTransactions(queryCriteria))
                .toList()
                .toSingle()
                .doOnSuccess(retrievedTransactions -> {
                    context.assertEquals(2, retrievedTransactions.size());
                    context.assertEquals(firstTransaction.getId(), retrievedTransactions.get(0).getId());
                    context.assertEquals(secondTransaction.getId(), retrievedTransactions.get(1).getId());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getTransactionsShouldReturnEmptyObservableWhenAccountDoesNotExist(TestContext context) {
        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.getTransactions(buildQueryCriteria(UUID.randomUUID()))
                .toList()
                .toSingle()
                .doOnSuccess(transactions -> context.assertTrue(transactions.isEmpty()))
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getTransactionsShouldReturnEmptyCollectionsForAccountWithNoTransactionsOfGivenType(TestContext context) {
        Account account = buildTestAccount();

        Deposit deposit = buildTestDeposit(d -> d.destinationAccount(account));
        Withdrawal withdrawal = buildTestWithdrawal(w -> w.sourceAccount(account));

        TransactionsQueryCriteria queryCriteria = buildQueryCriteria(account.getId(), TransactionType.DEPOSIT);

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(account)
                .flatMap(x -> repository.save(deposit))
                .flatMap(x -> repository.save(withdrawal))
                .flatMapObservable(x -> repository.getTransactions(queryCriteria))
                .toList()
                .toSingle()
                .doOnSuccess(retrievedTransactions -> {
                    context.assertEquals(1, retrievedTransactions.size());
                    context.assertEquals(deposit.getId(), retrievedTransactions.get(0).getId());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    @Test
    public void getTransactionsShouldReturnTransactionsSortedByLogicalClock(TestContext context) {
        Account account = buildTestAccount();

        Transaction firstTransaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(account))
                .destinationAccount(Optional.empty())
                .instant(Instant.ofEpochSecond(1000)));
        Transaction secondTransaction = buildTestTransaction(t -> t
                .sourceAccount(Optional.of(account))
                .destinationAccount(Optional.empty())
                .instant(Instant.ofEpochSecond(1)));

        TransactionsQueryCriteria queryCriteria = buildQueryCriteria(account.getId());

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(account)
                .flatMap(x -> repository.save(firstTransaction))
                .flatMap(x -> repository.save(secondTransaction))
                .flatMapObservable(x -> repository.getTransactions(queryCriteria))
                .toList()
                .toSingle()
                .doOnSuccess(retrievedTransactions -> {
                    context.assertEquals(2, retrievedTransactions.size());
                    context.assertEquals(firstTransaction.getId(), retrievedTransactions.get(0).getId());
                    context.assertEquals(secondTransaction.getId(), retrievedTransactions.get(1).getId());
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    private void testGetTransactionByQuery(TestContext context, Account account, Transaction transaction) {
        SingleTransactionQueryCriteria queryCriteria = buildQuerySingleCriteria(transaction);

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        repository.save(account)
                .flatMap(x -> repository.save(transaction))
                .flatMap(x -> repository.getTransaction(queryCriteria))
                .subscribe(new TestSubscriber<>(context, async));
    }

    private void verifyTransactionNotFoundException(TestContext context, UUID id, Single<?> initialization) {
        Async async = context.async();
        initialization
                .doOnSuccess(x -> context.fail("Expected TransactionNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(TransactionNotFoundException.class, error.getClass());
                    context.assertEquals(id.toString(), ((TransactionNotFoundException) error).getId());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    private void testRollbackAndThrowWhenTransactionAccountStateIsStale(
            TestContext context,
            Account account,
            Transaction transaction) {

        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();
        AccountsRepository otherRepository = repositoryProvider.get();

        repository.save(transaction.getSourceAccount().get())
                .flatMap(x -> repository.save(transaction.getDestinationAccount().get()))
                .flatMap(x -> otherRepository.getAccountById(account.getId())).flatMap(otherRepository::save)
                .flatMap(x -> repository.save(transaction))
                .doOnSuccess(x -> context.fail("Expected StaleAccountStateException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(StaleAccountStateException.class, error.getClass());
                    context.assertEquals(account.getId(), ((StaleAccountStateException) error).getId());
                    return null;
                })
                .flatMap(x -> repository.getTransaction(buildQuerySingleCriteria(transaction)))
                .doOnSuccess(x -> context.fail("Expected TransactionNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(TransactionNotFoundException.class, error.getClass());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    private void testRollbackAndThrowWhenTransactionAccountDoesNotExist(
            TestContext context,
            Account accountToSave,
            Account account,
            Transaction transaction) {
        Async async = context.async();
        AccountsRepository repository = repositoryProvider.get();

        repository.save(accountToSave)
                .flatMap(x -> repository.save(transaction))
                .flatMap(savedTransaction -> repository.getTransaction(buildQuerySingleCriteria(savedTransaction)))
                .doOnSuccess(x -> context.fail("Expected AccountNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(AccountNotFoundException.class, error.getClass());
                    context.assertEquals(account.getId().toString(), ((AccountNotFoundException) error).getId());
                    return null;
                })
                .flatMap(x -> repository.getTransaction(buildQuerySingleCriteria(transaction)))
                .doOnSuccess(x -> context.fail("Expected TransactionNotFoundException, but got success"))
                .onErrorReturn(error -> {
                    context.assertEquals(TransactionNotFoundException.class, error.getClass());
                    return null;
                })
                .subscribe(new TestSubscriber<>(context, async));
    }

    private Single<Long> countAccountRecords() {
        return dbClient.rxGetConnection()
                .flatMap(connection -> connection
                    .rxQuery("SELECT COUNT(*) AS ACCOUNTS_COUNT FROM accounts")
                    .map(resultSet -> resultSet.getRows().get(0).getLong("ACCOUNTS_COUNT")));
    }

    private Single<?> deleteAccount(UUID id) {
        return dbClient.rxGetConnection()
                .flatMap(connection -> connection.rxUpdateWithParams(
                        "DELETE FROM accounts WHERE uuid = ?", new JsonArray()
                                .add(id.toString())));
    }

    private Single<Integer> getAccountVersion(UUID id) {
        return dbClient.rxGetConnection()
                .flatMap(connection -> connection.rxQueryWithParams(
                        "SELECT version FROM accounts WHERE uuid = ?", new JsonArray().add(id.toString()))
                )
                .map(resultSet -> resultSet.getRows().get(0).getInteger("VERSION"));
    }
}
