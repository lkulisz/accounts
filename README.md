# Accounts Service

## Building and running the service

Accounts service requires following software to be installed in the system

- Java 8 JDK (tested with Oracle JDK)
- Apache Maven (tested with Maven 3.5.2)

The service was built and tested on Linux (Ubuntu 18.04), however there it should work on Windows and Mac OS as well.

### Building

Project can be built using standard maven commands: `mvn compile` or `mnv clean compile`

Additionally, it is possible to build a *fat JAR*, which includes all the dependencies by using `mvn clean package'`. 
This command will clean the project, compile it, run all unit tests and build the final package which is going to be
placed in `target` folder.

### Running 

Whne the project as a *fat JAR*, and the package resides in it's default location, following command will start the service:
 `java -jar target/accounts-1.0-SNAPSHOT.jar`
 
The service will start accepting connections on `http://localhost:8080`

### Running tests

In order to run the tests use `mvn test` command. 

### Running in development mode

It is possible to run the service in development mode with `mvn vertx:run`. This commmand also starts the service on
`http://localhost:8080`, however it also monitors all the changes to the source code. When sources are changed, project
is automatically rebuilt, and service is restarted, which helps in rapid prototyping of changes. 

## Development

### IDE
The project can be imported to IntelliJ IDEA (tested with version 2018.1.5) using built-in Maven project import wizard.
Using other IDEs should be possible as well, however it was not tested.

#### Running the service in IDEA
In order to launch (and debug) service, it is necessary to create a new launch configuration with following settings:

- Main class: `io.vertx.core.Launcher`
- Program arguments: `run io.lkulisz.accounts.Verticle`

Defaults can be used for other parameters

### Dependencies

The project is build with following major dependencies:

- Eclipse Vert.x (https://vertx.io/) web framework. The project includes several vert.x libraries and a maven plugin.
- Google Guice (https://github.com/google/guice) dependency injection framework
- HSQLDB (http://hsqldb.org/) in-memory relational, SQL database
- Hibernate Validator (http://hibernate.org/validator/) validation library
- Project Lombok (https://projectlombok.org/)
- JUNit (https://junit.org/junit4/) unit testing framework
- Mockito (http://site.mockito.org/) test mocking framework

Please refer to `pom.xml` for details.

### Structure
The project tries to follow the the hexagonal architecture pattern, however the pattern is not implemented explicitly 
(there are no *Adapter and *Port classes in the project). Project packages are organized to help maintain this architecture:

- `domain.model` package implements Domain Model - a set of classes which encapsulate knowledge about Account management domain.
Classes in this package encode operations on accounts, like various transactions - transfer, deposit, etc.
- Other classes in `domain` package ar part of the Application Domain layer exposed as `AccountsService` facade, `AccountsRepository` interface
and various other interfaces and class that help to interact with these.
- `http` package includes all the code which allows to expose the (application) domain outside via REST HTTP API.
Code in this package can be thought as an adapter for between Application Domain and HTTP clients (via vert.x).
- `persistece` package contains code provides repositories for storage and retrieval of domain objects from HSQLDB. 

### Testing

Most of the code is covered with unit tests. However, unit tests in this project rarely focus on testing single classes
in separation. They test layers. For example, `AccountsControllerTest`  class includes tests for the whole HTTP layer,
`AccountsService` class tests whole application domain and domain model, `JdbcAccountsRepositoryTest` tests whole persistence layer.

Thanks to that it is easier to refactor code within one layer, while maintaining contract with other layers.

Additionally, there are a few high-level tests, which go through all the code of the service end-to-end. These tests are
implemented in `AcceptanceTest` class. Their purpose is to make sure that all layers work well with each other, and to 
document available operations. 
